#!/bin/bash

if [ -z "$1" ]
then
      echo "CI_COMMIT_TAG is empty"
else
      echo "CI_COMMIT_TAG is NOT empty"
      npm version $1
fi