#!/bin/bash

CWD=$1
BUILD_TYPE=$2
BUILD_TARGET=$3

if [ "$BUILD_TARGET" = 'linux' ] 
then
    FEATURE=window-glutin
    if [ "$BUILD_TYPE" = 'release' ] 
    then
        docker-compose run --rm rust cargo build --features=$FEATURE --release
    else
        docker-compose run --rm rust cargo build --features=$FEATURE
    fi
elif [ "$BUILD_TARGET" = 'web' ] 
then
    mkdir -p $CWD/pkg
    docker-compose run --rm rust wasm-pack build --target web
    cp index.html $CWD/pkg
fi
