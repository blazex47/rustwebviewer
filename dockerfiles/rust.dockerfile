FROM rust:latest

WORKDIR /var/www/app

RUN cargo install -f wasm-bindgen-cli

RUN cargo install -f wasm-pack

ENV CARGO_HOME=/var/www/cargo

ENV PATH=$CARGO_HOME/bin:$PATH