use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

pub fn window() -> web_sys::Window {
    web_sys::window().expect("No global `window` exists")
}

pub fn document() -> web_sys::Document 
{
    window().document().expect("Failed to load document!")
}

pub fn get_element_by_id(id: &str) -> web_sys::Element
{
    document().get_element_by_id(id).expect(&format!("Failed to load id {}!",id))
}

pub fn canvas() -> web_sys::HtmlCanvasElement
{
    let result = get_element_by_id("canvas");
    
    result.dyn_into::<web_sys::HtmlCanvasElement>().expect("Failed to dynamic case canvas to HtmlCanvasElement!")
}

pub fn request_animation_frame(f: &Closure<dyn FnMut(f32)>) {
    window()
        .request_animation_frame(f.as_ref().unchecked_ref())
        .expect("Should register `requestAnimationFrame` OK");
}