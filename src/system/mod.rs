pub mod web_api;

#[derive(Clone)]
pub struct Chrono
{
    m_prev: f32,
    m_time: f32,
}

impl Chrono
{
    pub fn new(time: f32) -> Self
    {
        Chrono{m_prev: time, m_time: time}
    }

    pub fn set_time(&mut self, time: f32)
    {
        self.m_time = time;
    }

    pub fn get_time(&mut self) -> f32
    {
        self.m_time
    }

    pub fn update_prev_to_curr_time(&mut self)
    {
        self.m_prev = self.m_time;
    }

    pub fn get_dt(&mut self) -> f32
    {
        (self.m_time - self.m_prev) * 0.001f32
    }
}