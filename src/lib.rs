extern crate console_error_panic_hook;
use wasm_bindgen::prelude::*;
use std::cell::RefCell;
use std::rc::Rc;
use system::{web_api, Chrono};
use engine::{Core};
use engine::render::{RenderManager};
use engine::world::{WorldManager};
use engine::input::{InputManager};
use engine::resource::{ResourceManager};
use engine::event::{EventMessager, EventListener, EventHandler};
use engine::resource::{Resource,ResourceRequestEvent,AssetRequestEvent,AssetRetrieveEvent};

mod system;
mod engine;

#[wasm_bindgen]
extern "C"{
    #[wasm_bindgen(js_namespace = ["document"])]
    fn asset_request(request_id: &JsValue,uri: &JsValue);
}

#[wasm_bindgen]
struct WasmApp
{
    m_engine: Rc<RefCell<Engine>>,
}

#[wasm_bindgen]
impl WasmApp
{
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self
    {
        WasmApp { m_engine : Rc::new(RefCell::new(Engine::new()))}
    }

    pub fn init(&mut self)
    {
        self.m_engine.borrow_mut().init().expect("Failed to start engine core!");
    }

    pub fn update(&mut self)
    {
        let mut engine = self.m_engine.clone();

        let mut timer = Chrono::new(0.0);

        let f = Rc::new(RefCell::new(None));
        let g = f.clone();

        *g.borrow_mut() = Some(Closure::wrap(Box::new(move |time| 
        {
            timer.set_time(time);
    
            engine.borrow_mut().update(&mut timer);
    
            web_api::request_animation_frame(f.borrow().as_ref().expect("Failed to borrow ref from funcion 'f'!"));
    
            timer.update_prev_to_curr_time();
    
        }) as Box<dyn FnMut(f32)>));

        web_api::request_animation_frame(g.borrow().as_ref().expect("Failed to borrow ref from funcion 'f'g"));
    }

    pub fn free(&mut self)
    {
        self.m_engine.borrow_mut().free();
    }

    pub fn resource_request(&mut self, name: JsValue, uri: JsValue)
    {
        let mut result_name = String::new();
        let mut result_uri = String::new();

        if let Some(name_str) = name.as_string()
        {
            result_name = name_str.clone();
            web_sys::console::log_2(&"resource_request name".into(),&name_str.into());
        }

        if let Some(uri_str) = uri.as_string()
        {
            result_uri = uri_str.clone();
            web_sys::console::log_2(&"resource_request uri".into(),&uri_str.into());
        }

        self.m_engine.borrow_mut().resource_request(result_name,result_uri);
    }

    pub fn asset_retrieve(&mut self, request_id: JsValue, buffer: JsValue, status_code: JsValue)
    {
        let mut result_request_id = String::new();

        if let Some(request_id_str) = request_id.as_string()
        {
            result_request_id = request_id_str.clone();
            web_sys::console::log_2(&"asset_retrieve request_id".into(),&request_id_str.into());
        }

        //let buffer: JsValue = blob_data.array_buffer().then([](){});
        let array = js_sys::Uint8Array::new(&buffer);
        let bytes: Vec<u8> = array.to_vec();

        web_sys::console::log_2(&"asset_retrieve bytes.len()".into(),&(bytes.len() as u32).into());

        let mut result_status = 200;

        if let Some(status_float) = status_code.as_f64()
        {
            result_status = status_float as u32;
            web_sys::console::log_2(&"asset_retrieve status_code".into(),&result_status.into());
        }

        self.m_engine.borrow_mut().asset_retrieve(result_request_id,bytes,result_status);
    }
}

pub struct Engine 
{
    m_core: Option<Core>,
    m_listener: EventListener,
    m_render_mgr: Rc<RefCell<RenderManager>>,
    m_world_mgr: Rc<RefCell<WorldManager>>,
    m_input_mgr: Rc<RefCell<InputManager>>,
    m_resource_mgr: Rc<RefCell<ResourceManager>>,
}

impl Engine 
{
    /// Create a new web client
    pub fn new() -> Engine 
    {
        //let app = Rc::new(App::new());

        let render_mgr = Rc::new(RefCell::new(RenderManager::new()));
        let world_mgr = Rc::new(RefCell::new(WorldManager::new()));
        let input_mgr = Rc::new(RefCell::new(InputManager::new()));
        let resource_mgr = Rc::new(RefCell::new(ResourceManager::new()));

        Engine { m_core: Some(Core::new()), m_listener: EventListener::new(), m_render_mgr: render_mgr, m_world_mgr : world_mgr, m_input_mgr: input_mgr, m_resource_mgr: resource_mgr}
    }

    pub fn init(&mut self) -> Result<(), JsValue> 
    {
        let render_mgr = &mut self.m_render_mgr;
        let world_mgr = &mut self.m_world_mgr;
        let input_mgr = &mut self.m_input_mgr;
        let resource_mgr = &mut self.m_resource_mgr;

        self.m_listener.init(&self.m_core);

        self.m_listener.register::<AssetRequestEvent>(&String::from("AssetRequest"),Box::new(move |handle:&mut EventHandler|->Vec<EventHandler>
        {
            let asset_req = handle.get_event::<AssetRequestEvent>();

            web_sys::console::log_2(&"resource_request request_id".into(),&asset_req.m_request_id.clone().into());
            web_sys::console::log_2(&"resource_request uri".into(),&asset_req.m_uri.clone().into());
            asset_request(&asset_req.m_request_id.clone().into(),&asset_req.m_uri.clone().into());
                
            Vec::new()
        }));

        input_mgr.borrow_mut().init(&self.m_core);
        render_mgr.borrow_mut().init(&self.m_core);
        world_mgr.borrow_mut().init(&self.m_core);
        resource_mgr.borrow_mut().init(&self.m_core);

        Ok(())
    }

    /// Update our simulation
    pub fn update(&mut self, timer: &mut Chrono) 
    {
        //self.app.store.borrow_mut().msg(&Msg::AdvanceClock(dt));
    
        let render_mgr = &self.m_render_mgr;
        let world_mgr = &mut self.m_world_mgr;
        let resource_mgr = &mut self.m_resource_mgr;

        world_mgr.borrow_mut().update(timer);
        render_mgr.borrow_mut().update(timer);
        resource_mgr.borrow_mut().update(timer);

        //web_sys::console::log_2(&"delta time".into(), &angle.into());
    }

    pub fn free(&mut self)
    {
        let render_mgr = &self.m_render_mgr;
        let world_mgr = &mut self.m_world_mgr;
        let resource_mgr = &mut self.m_resource_mgr;

        world_mgr.borrow_mut().free();
        render_mgr.borrow_mut().free();
        resource_mgr.borrow_mut().free();
    }

    pub fn resource_request(&mut self, name: String, uri: String)
    {
        self.m_core.as_ref().unwrap().m_messager.borrow_mut().invoke(& mut EventHandler::new(Vec::new(),ResourceRequestEvent::new(name, uri)));
    }

    pub fn asset_retrieve(&mut self, request_id: String, buffer: Vec<u8>, status_code: u32)
    {
        self.m_core.as_ref().unwrap().m_messager.borrow_mut().invoke(& mut EventHandler::new(Vec::new(),AssetRetrieveEvent::new(&request_id,&buffer,status_code)));
    }
}

#[wasm_bindgen]
pub fn main_js() -> Result<(), JsValue>
{
    console_error_panic_hook::set_once();

    //web_sys::console::log_2(&"input".into(),&input.into());

    let mut wasm_app = WasmApp::new();

    wasm_app.init();

    wasm_app.update();

    //wasm_app.free();

    Ok(())
}
