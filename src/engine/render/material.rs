// Standard Dependencies
use std::rc::Rc;
use std::any::Any;
use std::boxed::Box;
use std::io::{Cursor};
use std::cmp::Ordering;
use std::cell::RefCell;
use std::collections::HashMap;
// 3rd Party Dependencies
use uuid::Uuid;
use glam::{Mat4, Vec2, Vec3, Vec4, Quat};
// Engine Dependencies
use crate::engine::resource::{Model,PosNormTangTexCol,AlphaFlag};

#[derive(Clone)]
pub struct RenderMaterial
{
    pub m_alpha_factor: f32,
    pub m_albedo_factor: Vec4,
    pub m_metallic_factor: f32,
    pub m_roughness_factor: f32,
    pub m_emissive_factor: Vec3,

    pub m_albedo_uuid: Option<Uuid>,
    pub m_normal_uuid: Option<Uuid>,
    pub m_metallic_roughness_uuid: Option<Uuid>,
    pub m_ao_uuid: Option<Uuid>,
    pub m_emissive_uuid: Option<Uuid>,

    pub m_alpha_flag: AlphaFlag,
    pub m_double_sided: bool,
}

impl RenderMaterial
{
    pub fn new()-> Self
    {
        RenderMaterial
        {
            m_alpha_factor: 1.0,
            m_albedo_factor: Vec4::ONE,
            m_metallic_factor: 1.0,
            m_roughness_factor: 1.0,
            m_emissive_factor: Vec3::ONE,
        
            m_albedo_uuid: None,
            m_normal_uuid: None,
            m_metallic_roughness_uuid: None,
            m_ao_uuid: None,
            m_emissive_uuid: None,
        
            m_alpha_flag: AlphaFlag::Opaque,
            m_double_sided: false,
        }
    }
}