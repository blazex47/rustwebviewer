use std::boxed::Box;
use std::cell::RefCell;
use std::rc::Rc;
use std::any::Any;
use uuid::Uuid;
use std::io::{Cursor};
use std::cmp::Ordering;
use image::io::{Reader};
use image::{ImageFormat};
use glam::{Mat4, Vec2, Vec3, Vec4, Quat};
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use egui::{Context,RawInput,Color32};
use std::collections::HashMap;
use crate::engine::input::{MouseButton,MouseEvent};
use js_sys::{Float32Array, Uint32Array, Uint16Array, WebAssembly};
use web_sys::{WebGlProgram, WebGlRenderingContext, WebGlShader,WebGlTexture, HtmlCanvasElement, WebGlUniformLocation, WebGlBuffer, WebGlFramebuffer};
use crate::engine::event::{Event, EventListener, EventHandler};
use crate::engine::{Core, Chrono, UniqueIDRegistry};
use crate::engine::resource::{Model,PosNormTangTexCol,AlphaFlag};

static CUBE_VERTICES: [[f32;3]; 36] = 
[
    // Pos X
    [ 1.0,  1.0,  1.0],
    [ 1.0,  1.0, -1.0],
    [ 1.0, -1.0,  1.0],
    [ 1.0, -1.0,  1.0],
    [ 1.0,  1.0, -1.0],
    [ 1.0, -1.0, -1.0],
    // Neg X
    [-1.0,  1.0, -1.0],
    [-1.0,  1.0,  1.0],
    [-1.0, -1.0, -1.0],
    [-1.0, -1.0, -1.0],
    [-1.0,  1.0,  1.0],
    [-1.0, -1.0,  1.0],
    // Pos Y
    [-1.0,  1.0, -1.0],
    [ 1.0,  1.0, -1.0],
    [-1.0,  1.0,  1.0],
    [-1.0,  1.0,  1.0],
    [ 1.0,  1.0, -1.0],
    [ 1.0,  1.0,  1.0],
    // Neg Y
    [-1.0, -1.0,  1.0],
    [ 1.0, -1.0,  1.0],
    [-1.0, -1.0, -1.0],
    [-1.0, -1.0, -1.0],
    [ 1.0, -1.0,  1.0],
    [ 1.0, -1.0, -1.0],
    // Pos Z
    [-1.0,  1.0,  1.0],
    [ 1.0,  1.0,  1.0],
    [-1.0, -1.0,  1.0],
    [-1.0, -1.0,  1.0],
    [ 1.0,  1.0,  1.0],
    [ 1.0, -1.0,  1.0],
    // Neg Z
    [ 1.0,  1.0, -1.0],
    [-1.0,  1.0, -1.0],
    [ 1.0, -1.0, -1.0],
    [ 1.0, -1.0, -1.0],
    [-1.0,  1.0, -1.0],
    [-1.0, -1.0, -1.0]
];

static CUBE_VERTS: [[f32;2]; 36] = 
[
    // Bottom Left
    [-1.0, -1.0],
    [-1.0/3.0, -1.0],
    [-1.0, 0.0],
    [-1.0, 0.0],
    [-1.0/3.0, -1.0],
    [-1.0/3.0, 0.0],
    // Top Left
    [-1.0, 0.0],
    [-1.0/3.0, 0.0],
    [-1.0, 1.0],
    [-1.0, 1.0],
    [-1.0/3.0, 0.0],
    [-1.0/3.0, 1.0],
    // Bottom Middle
    [-1.0/3.0, -1.0],
    [1.0/3.0, -1.0],
    [-1.0/3.0, 0.0],
    [-1.0/3.0, 0.0],
    [1.0/3.0, -1.0],
    [1.0/3.0, 0.0],
    // Top Middle
    [-1.0/3.0, 0.0],
    [1.0/3.0, 0.0],
    [-1.0/3.0, 1.0],
    [-1.0/3.0, 1.0],
    [1.0/3.0, 0.0],
    [1.0/3.0, 1.0],
    // Bottom Right
    [1.0/3.0, -1.0],
    [1.0, -1.0],
    [1.0/3.0, 0.0],
    [1.0/3.0, 0.0],
    [1.0, -1.0],
    [1.0, 0.0],
    // Top Right
    [1.0/3.0, 0.0],
    [1.0, 0.0],
    [1.0/3.0, 1.0],
    [1.0/3.0, 1.0],
    [1.0, 0.0],
    [1.0, 1.0],
];

static CUBE_INDICES: [u32; 36] = 
[    
    0,  1,  2,  3,  4,  5,
    6,  7,  8,  9, 10, 11,
    12, 13, 14, 15, 16, 17,
    18, 19, 20, 21, 22, 23,
    24, 25, 26, 27, 28, 29,
    30, 31, 32, 33, 34, 35,
];

static FULLSCREEN_UV: [[f32;2]; 6] = 
[
    [ 0.0,  0.0],
    [ 1.0,  0.0],
    [ 0.0,  1.0],
    [ 0.0,  1.0],
    [ 1.0,  0.0],
    [ 1.0,  1.0],
];

static FULLSCREEN_INDICES: [u32; 6] = 
[   
    0,  1,  2,  3,  4,  5,
];

#[derive(Clone)]
pub struct PBRSetting
{
    pub m_vertex_bo: Option<WebGlBuffer>,
    pub m_index_bo: Option<WebGlBuffer>,
    pub m_index_len: u32,
    pub m_albedo_tex: Option<WebGlTexture>,
    pub m_normal_tex: Option<WebGlTexture>,
    pub m_metallic_roughness_tex: Option<WebGlTexture>,
    pub m_ao_tex: Option<WebGlTexture>,
    pub m_emissive_tex: Option<WebGlTexture>,
    pub m_albedo_factor: Vec4,
    pub m_metallic_factor: f32,
    pub m_roughness_factor: f32,
    pub m_emissive_factor: Vec3,
    pub m_double_sided: bool,
    pub m_alpha_factor: f32,
    pub m_alpha_flag: AlphaFlag,
}

#[derive(Clone)]
pub struct FlatSetting
{
    pub m_vertices_bo: Option<WebGlBuffer>,
    pub m_vertices: Vec<Vec3>,
    pub m_colors_bo: Option<WebGlBuffer>,
    pub m_colors: Vec<Vec3>,
    pub m_index_bo: Option<WebGlBuffer>,
    pub m_index: Vec<u32>,
}

#[derive(Clone)]
pub enum BufferObjInfo
{
    PBR(Model),
    Flat(String),
}

#[derive(Clone)]
pub struct BufferObjEvent
{
    pub m_bufferinfo: BufferObjInfo,
    pub m_renderinfo_map: HashMap<String,(Mat4,Vec<RenderInfo>)>,
}

impl Event for BufferObjEvent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }
}

impl BufferObjEvent
{
    pub fn new(bufferobj: &BufferObjInfo) -> Self
    {
        BufferObjEvent{ m_bufferinfo: bufferobj.clone()
                      , m_renderinfo_map : HashMap::new()}
    }
}

#[derive(Clone)]
pub enum RenderInfo
{
    PBR(PBRSetting),
    Flat(FlatSetting),
}

pub struct RenderEvent
{
    m_mov_matrix: Mat4,
    m_info: RenderInfo,
    m_opacity: f32,
}

impl Event for RenderEvent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }
}

impl RenderEvent
{
    pub fn new(mov_matrix: &Mat4,info: &RenderInfo, opacity: f32) -> Self
    {
        RenderEvent{m_mov_matrix: mov_matrix.clone() ,m_info : info.clone(),m_opacity: opacity}
    }
}

pub struct CameraEvent
{
    pub m_view: Mat4,
    pub m_proj: Mat4,
    pub m_cam_pos: Vec3,
}

impl Event for CameraEvent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }
}

impl CameraEvent
{
    pub fn new(view: &Mat4, proj: &Mat4, pos: &Vec3) -> Self
    {
        CameraEvent{m_view: view.clone(),m_proj: proj.clone(), m_cam_pos: pos.clone()}
    }
}

pub struct UIBuffer
{
    index_buffer: Option<WebGlBuffer>,
    pos_buffer: Option<WebGlBuffer>,
    tc_buffer: Option<WebGlBuffer>,
    color_buffer: Option<WebGlBuffer>
}

pub enum ShaderUniform
{
    UniformInt(i32),
    UniformFloat(f32),
    UniformVec2(Vec2),
    UniformVec3(Vec3),
    UniformVec4(Vec4),
    UniformMat4(Mat4),
    UniformTexture2D(Option<WebGlTexture>),
    UniformTextureCubeMap(Option<WebGlTexture>),
}

pub struct ShaderProgram
{
    m_context: Rc<RefCell<web_sys::WebGlRenderingContext>>,
    pub m_attributes: HashMap<String,u32>,
    pub m_uniforms: HashMap<String,(Option<WebGlUniformLocation>,ShaderUniform)>,
    pub m_program: web_sys::WebGlProgram,
}

impl ShaderProgram
{
    fn new(core: &Option<Core>,vertex_shader_source: &str, fragment_shader_source: &str) -> Self
    {
        let context = &core.as_ref().unwrap().m_context;
    
        let vert_shader = compile_shader(
            &context.borrow_mut(),
            WebGlRenderingContext::VERTEX_SHADER,
            vertex_shader_source,
        ).expect("Failed to compile vertex shader!");
        let frag_shader = compile_shader(
            &context.borrow_mut(),
            WebGlRenderingContext::FRAGMENT_SHADER,
            fragment_shader_source,
        ).expect("Failed to compile fragment shader!");
    
        let program = link_program(&context.borrow_mut(), &vert_shader, &frag_shader).expect("Failed to link shaders!");

        ShaderProgram{m_context: context.clone(), m_attributes: HashMap::new(), m_uniforms: HashMap::new(), m_program: program }
    }

    fn use_program(&mut self, use_prog: bool)
    {
        self.m_context.borrow_mut().use_program(match use_prog
            {
                true => Some(&self.m_program),
                false => None,
            });
    }

    fn set_attribute(&mut self, name: String)
    {
        if let None = self.m_attributes.get_mut(&name)
        {
            let attribute = self.m_context.borrow_mut().get_attrib_location(&self.m_program, name.clone().as_str()) as u32;

            self.m_attributes.insert(name,attribute);
        }
    }

    fn set_uniform(&mut self, name: String, shader_uniform: ShaderUniform)
    {
        if let Some((_,shader_uni)) = self.m_uniforms.get_mut(&name)
        {
            (*shader_uni) = shader_uniform;
        }
        else
        {
            let uniform = self.m_context.borrow_mut().get_uniform_location(&self.m_program, name.clone().as_str()).expect(&format!("cannot get {}",  name.clone().as_str())[..]).into();

            self.m_uniforms.insert(name,(uniform,shader_uniform));
        }        
    }

    fn bind_uniforms(&mut self)
    {
        self.m_context.borrow_mut().use_program(Some(&self.m_program));

        let mut tex_count = 0;

        for (_, value) in &self.m_uniforms
        {
            match &value.1
            {
                ShaderUniform::UniformInt(int_val) =>
                {
                    self.m_context.borrow_mut().uniform1i(value.0.as_ref(), *int_val);
                },
                ShaderUniform::UniformFloat(float_val) =>
                {
                    self.m_context.borrow_mut().uniform1f(value.0.as_ref(), *float_val);
                },
                ShaderUniform::UniformVec2(vec2_val) =>
                {
                    self.m_context.borrow_mut().uniform2f(value.0.as_ref(),vec2_val.x,vec2_val.y);
                },
                ShaderUniform::UniformVec3(vec3_val) =>
                {
                    self.m_context.borrow_mut().uniform3f(value.0.as_ref(),vec3_val.x,vec3_val.y,vec3_val.z);
                },
                ShaderUniform::UniformVec4(vec4_val) =>
                {
                    self.m_context.borrow_mut().uniform4f(value.0.as_ref(),vec4_val.x,vec4_val.y,vec4_val.z,vec4_val.w);
                },
                ShaderUniform::UniformMat4(mat4_val) =>
                {        
                    self.m_context.borrow_mut().uniform_matrix4fv_with_f32_array(value.0.as_ref(), false, mat4_val.as_ref());
                },
                ShaderUniform::UniformTexture2D(_tex2d_val) =>
                {
                    self.m_context.borrow_mut().uniform1i(value.0.as_ref(), tex_count as i32);
                    tex_count += 1;
                },
                ShaderUniform::UniformTextureCubeMap(_texcube_val) =>
                {
                    self.m_context.borrow_mut().uniform1i(value.0.as_ref(), tex_count as i32);
                    tex_count += 1;
                },                
            }
        }
    }

    fn set_texture_active(&mut self)
    {
        let mut tex_count = 0;

        for (_, value) in &self.m_uniforms
        {
            match &value.1
            {
                ShaderUniform::UniformTexture2D(tex2d_val) =>
                {
                    self.m_context.borrow_mut().active_texture(WebGlRenderingContext::TEXTURE0 + tex_count);
                    self.m_context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_2D, tex2d_val.as_ref());
                    tex_count += 1;
                },
                ShaderUniform::UniformTextureCubeMap(texcube_val) =>
                {

                    self.m_context.borrow_mut().active_texture(WebGlRenderingContext::TEXTURE0 + tex_count);
                    self.m_context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_CUBE_MAP, texcube_val.as_ref());
                    tex_count += 1;                
                }, 
                _=>
                {

                }                
            }
        }
    }
}

pub struct RenderManager 
{
    m_core: Option<Core>,
    m_listener: EventListener,
    m_pbr_queue: Rc<RefCell<Vec<(Mat4,PBRSetting,f32)>>>,
    m_flat_queue: Rc<RefCell<Vec<(Mat4,FlatSetting)>>>,
    m_cube_vertices:Option<WebGlBuffer>,
    m_cube_verts:Option<WebGlBuffer>,
    m_cube_indices:Option<WebGlBuffer>,
    m_fullscreen_uv: Option<WebGlBuffer>,
    m_fullscreen_indices: Option<WebGlBuffer>,
    m_cube_tex: Option<WebGlTexture>,
    m_specular_tex: Option<WebGlTexture>,
    m_irradiance_tex: Option<WebGlTexture>,
    m_hdr_tex: Option<WebGlTexture>,
    m_white_tex: Option<WebGlTexture>,
    m_black_tex: Option<WebGlTexture>,
    m_spec_brdf_tex: Option<WebGlTexture>,
    m_tonemap_tex: Option<WebGlTexture>,
    m_shadow_depth_tex: Option<WebGlTexture>,
    m_shadow_fbo: Option<WebGlFramebuffer>,
    m_camera_pos: Rc<RefCell<Vec3>>,
    m_sampling_size: Vec2,
    m_shaders_map: HashMap<String, ShaderProgram>,
    m_egui_ctx: egui::CtxRef,
    m_raw_input: Rc<RefCell<egui::RawInput>>,
    m_ui_captured: Rc<RefCell<bool>>,
    m_view_mtx: Rc<RefCell<Mat4>>,
    m_proj_mtx: Rc<RefCell<Mat4>>,
    m_ui_program: Option<web_sys::WebGlProgram>,
    m_ui_buffer: Option<UIBuffer>,
    m_egui_texture: Option<WebGlTexture>,
    m_egui_texture_version: Option<u64>,
}

pub fn native_pixels_per_point() -> f32 {
    let pixels_per_point = web_sys::window().unwrap().device_pixel_ratio() as f32;
    if pixels_per_point > 0.0 && pixels_per_point.is_finite() {
        pixels_per_point
    } else {
        1.0
    }
}

pub fn canvas_size_in_points(canvas: &HtmlCanvasElement) -> egui::Vec2 
{
    let pixels_per_point = native_pixels_per_point();
    egui::vec2(
        canvas.width() as f32 / pixels_per_point,
        canvas.height() as f32 / pixels_per_point,
    )
}

pub fn screen_size_in_native_points() -> Option<egui::Vec2> {
    let window = web_sys::window()?;
    Some(egui::Vec2::new(
        window.inner_width().ok()?.as_f64()? as f32,
        window.inner_height().ok()?.as_f64()? as f32,
    ))
}

pub fn resize_canvas_to_screen_size(canvas: &HtmlCanvasElement, max_size_points: egui::Vec2) -> Option<()> 
{
    let screen_size_points = screen_size_in_native_points()?;
    let pixels_per_point = native_pixels_per_point();

    let max_size_pixels = pixels_per_point * max_size_points;

    let canvas_size_pixels = pixels_per_point * screen_size_points;
    let canvas_size_pixels = canvas_size_pixels.min(max_size_pixels);
    let canvas_size_points = canvas_size_pixels / pixels_per_point;

    // Make sure that the height and width are always even numbers.
    // otherwise, the page renders blurry on some platforms.
    // See https://github.com/emilk/egui/issues/103
    fn round_to_even(v: f32) -> f32 {
        (v / 2.0).round() * 2.0
    }

    /*canvas
        .style()
        .set_property(
            "width",
            &format!("{}px", round_to_even(canvas_size_points.x)),
        )
        .ok()?;
    canvas
        .style()
        .set_property(
            "height",
            &format!("{}px", round_to_even(canvas_size_points.y)),
        )
        .ok()?;*/
    canvas.set_width(round_to_even(canvas_size_pixels.x) as u32);
    canvas.set_height(round_to_even(canvas_size_pixels.y) as u32);

    Some(())
}

pub fn reset_egui_raw_input(canvas: &HtmlCanvasElement) -> egui::RawInput
{
    let mut raw_input = egui::RawInput::default();

    raw_input.pixels_per_point = Some(native_pixels_per_point());
    raw_input.predicted_dt = 0.016666;
    raw_input.screen_rect = Some(egui::Rect::from_min_size(Default::default(), canvas_size_in_points(&canvas)));

    return raw_input;
}

// so far only support jpeg and png, seems that hdr have issue and check Cargo.toml 
fn decode_buffer(buffer: &Vec<u8>) -> Option<(u32,u32,u32,Vec<u8>)>
{
    let reader = Reader::new(Cursor::new(buffer.clone())).with_guessed_format().expect("Cursor io never fails");

    match reader.decode().expect("Failed to decode!")
    {
        image::DynamicImage::ImageLuma8(gray_image) =>
        {
            Some((gray_image.width(),gray_image.height(),WebGlRenderingContext::LUMINANCE,gray_image.into_raw()))
        },
        image::DynamicImage::ImageLumaA8(gray_alpha_image) =>
        {
            Some((gray_alpha_image.width(),gray_alpha_image.height(),WebGlRenderingContext::LUMINANCE_ALPHA,gray_alpha_image.into_raw()))
        },
        image::DynamicImage::ImageRgb8(rgb_image) =>
        {
            Some((rgb_image.width(),rgb_image.height(),WebGlRenderingContext::RGB,rgb_image.into_raw()))
        },
        image::DynamicImage::ImageRgba8(rgba_image) =>
        {
            Some((rgba_image.width(),rgba_image.height(),WebGlRenderingContext::RGBA,rgba_image.into_raw()))        },
        image::DynamicImage::ImageBgr8(_) =>
        {
            panic!("Decode Buffer: ImageBgr8 is not supported");
            None
        },
        image::DynamicImage::ImageBgra8(_) =>
        {
            panic!("Decode Buffer: ImageBgra8 is not supported");
            None
        },
        image::DynamicImage::ImageLuma16(_) =>
        {
            panic!("Decode Buffer: ImageLuma16 is not supported");
            None
        },
        image::DynamicImage::ImageLumaA16(_) =>
        {
            panic!("Decode Buffer: ImageLumaA16 is not supported");
            None
        },
        image::DynamicImage::ImageRgb16(_) =>
        {
            panic!("Decode Buffer: ImageRgb16 is not supported");
            None
        },
        image::DynamicImage::ImageRgba16(_) =>
        {
            panic!("Decode Buffer: ImageRgba16 is not supported");
            None
        },
        _ =>
        {
            panic!("Decode Buffer: Format is not supported");
            None
        }
    }
}

fn generate_create_texture(context: &WebGlRenderingContext,width: u32, height: u32,pixels: &Vec<u8>) -> Option<WebGlTexture>
{
    let count = (pixels.len() as u32) / (width * height);

    web_sys::console::log_2(&"generate_create_texture: ".into(),&count.into());

    let format = match count
    {
        1 => WebGlRenderingContext::LUMINANCE,
        2 => WebGlRenderingContext::LUMINANCE_ALPHA,
        3 => WebGlRenderingContext::RGB,
        4 =>WebGlRenderingContext::RGBA,
        _=> WebGlRenderingContext::ALPHA
    };
    
    generate_create_texture_with_format(context, width,height, format, pixels)
}

fn generate_create_texture_with_format(context: &WebGlRenderingContext,width: u32, height: u32, format: u32, pixels: &Vec<u8>) -> Option<WebGlTexture>
{
    let tex = context.create_texture();

    context.active_texture(WebGlRenderingContext::TEXTURE0);
    context.bind_texture(WebGlRenderingContext::TEXTURE_2D, tex.as_ref());

    let level = 0;
    let internal_format = format;
    let border = 0;
    let src_format = format;
    let src_type = WebGlRenderingContext::UNSIGNED_BYTE;
    let alignment = 1;
    context.pixel_storei(WebGlRenderingContext::UNPACK_ALIGNMENT, alignment);
    context.tex_image_2d_with_i32_and_i32_and_i32_and_format_and_type_and_opt_u8_array(
        WebGlRenderingContext::TEXTURE_2D,
        level,
        internal_format as i32,
        width as i32,
        height as i32,
        border,
        src_format,
        src_type,
        Some(pixels),
    )
    .unwrap();
    context.tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_S, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
    context.tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_T, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
    context.tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MIN_FILTER, WebGlRenderingContext::LINEAR as i32);
    context.tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MAG_FILTER, WebGlRenderingContext::LINEAR as i32);

    context.bind_texture(WebGlRenderingContext::TEXTURE_2D, None);

    tex
}

impl RenderManager 
{
    /// Create a new web client
    pub fn new() -> RenderManager  
    {
        let pbr_queue = Rc::new(RefCell::new(Vec::new()));

        let flat_queue = Rc::new(RefCell::new(Vec::new()));

        let listener = EventListener::new();

        let egui_ctx = egui::CtxRef::default();

        let raw_input = Rc::new(RefCell::new(egui::RawInput::default()));

        let view_mat = Rc::new(RefCell::new(Mat4::IDENTITY));

        let proj_mat = Rc::new(RefCell::new(Mat4::IDENTITY));

        let cam_pos = Rc::new(RefCell::new(Vec3::new(0.0,0.0,-1.0)));

        RenderManager{
            m_core : None, 
            m_listener : listener, 
            m_pbr_queue: pbr_queue,
            m_flat_queue: flat_queue,
            m_cube_vertices: None,
            m_cube_verts: None,
            m_cube_indices: None,
            m_fullscreen_uv: None,
            m_fullscreen_indices: None,
            m_cube_tex: None,
            m_specular_tex: None,
            m_irradiance_tex: None,
            m_hdr_tex: None,
            m_white_tex: None,
            m_black_tex: None,
            m_spec_brdf_tex: None,
            m_tonemap_tex: None,
            m_shadow_depth_tex: None,
            m_shadow_fbo: None,
            m_camera_pos: cam_pos,
            m_sampling_size: Vec2::new(512.0,512.0),
            m_shaders_map: HashMap::new(),
            m_egui_ctx: egui_ctx,
            m_view_mtx: view_mat,
            m_proj_mtx: proj_mat,
            m_raw_input: raw_input, m_ui_captured: Rc::new(RefCell::new(false)), m_ui_program: None, m_ui_buffer: None, m_egui_texture: None, m_egui_texture_version: None}
    }

    pub fn init(&mut self,core: &Option<Core>)
    {
        self.m_core = core.clone();

        let canvas = &self.m_core.as_ref().unwrap().m_canvas;

        //resize_canvas_to_screen_size(&canvas,egui::Vec2::new(1024.0, 2048.0));

        (*self.m_raw_input.borrow_mut()) = reset_egui_raw_input(&canvas);

        let context = &self.m_core.as_ref().unwrap().m_context;
    
        let egui_texture = context.borrow_mut().create_texture().unwrap();

        context.borrow_mut().active_texture(WebGlRenderingContext::TEXTURE0);
        context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_2D, Some(&egui_texture));

        context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_S, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
        context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_T, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
        context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MIN_FILTER, WebGlRenderingContext::LINEAR as i32);
        context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MAG_FILTER, WebGlRenderingContext::LINEAR as i32);

        self.m_egui_texture = Some(egui_texture);

        let inner_pbr_queue = self.m_pbr_queue.clone();

        let inner_flat_queue = self.m_flat_queue.clone();

        self.m_listener.init(&self.m_core);

        self.m_listener.register::<RenderEvent>
        (&String::from("Render"),Box::new(move |handle:&mut EventHandler| ->Vec<EventHandler>
        {
            let render_msg = handle.get_event::<RenderEvent>();

            match &render_msg.m_info
            {
                RenderInfo::PBR(pbr_setting)=>
                {
                    inner_pbr_queue.borrow_mut().push((render_msg.m_mov_matrix, pbr_setting.clone(),render_msg.m_opacity));
                },
                RenderInfo::Flat(flat_setting)=>
                {
                    inner_flat_queue.borrow_mut().push((render_msg.m_mov_matrix, flat_setting.clone()));
                }
            } 

            Vec::new()
        }));

        let inner_view = self.m_view_mtx.clone();

        let inner_proj = self.m_proj_mtx.clone();

        let inner_cam_pos = self.m_camera_pos.clone();

        self.m_listener.register::<CameraEvent>
        (&String::from("Camera"),Box::new(move |handle:&mut EventHandler| ->Vec<EventHandler>
        {
            //web_sys::console::log_1(&"CameraEvent!".into());

            let camera_msg = handle.get_event::<CameraEvent>();

            *inner_view.borrow_mut() = camera_msg.m_view.clone();

            *inner_proj.borrow_mut() = camera_msg.m_proj.clone();

            *inner_cam_pos.borrow_mut() = camera_msg.m_cam_pos.clone();

            Vec::new()
        }));
        
        let inner_context = self.m_core.as_ref().unwrap().m_context.clone();

        self.m_listener.register::<BufferObjEvent>
        (&String::from("BufferObj0"),Box::new(move |handle:&mut EventHandler| ->Vec<EventHandler>
        {
            let mut buf_obj_msg = handle.get_event::<BufferObjEvent>();
 
            buf_obj_msg.m_renderinfo_map = match &buf_obj_msg.m_bufferinfo
            {
                BufferObjInfo::PBR(model) =>
                {
                    let mut renderinfo_map = HashMap::new();

                    let meshes = &model.m_meshes;

                    for i in 0..meshes.len()
                    {
                        let mesh = &meshes[i];

                        let mut bo_list = Vec::new();

                        for primitive in &mesh.m_primitives
                        {                    
                            //let (vertices_start, vertices_end) = calculate_loc_begin_end::<f32, PosNormTangTex>(&primitive.m_vertices);
                            let vertices_start: u32 = primitive.m_vertices.as_ptr() as u32 / 4;
                            let vertices_breakdown = std::mem::size_of::<PosNormTangTexCol>() / 4;
                            let vertices_end: u32 = vertices_start + (vertices_breakdown * primitive.m_vertices.len()) as u32;

                            web_sys::console::log_2(&"std::mem::size_of::<PosNormTangTexCol>()!".into(),&(std::mem::size_of::<PosNormTangTexCol>() as u32).into());

                            //let (indices_start, indices_end) = calculate_loc_begin_end::<u32, u32>(&primitive.m_indices);
                            let indices_start: u32 = primitive.m_indices.as_ptr() as u32 / 4;
                            let indices_length: u32 = primitive.m_indices.len() as u32;
                            let indices_end: u32 = indices_start + indices_length;
                            
                            let mut albedo_tex: Option<WebGlTexture> = None;
                            let mut normal_tex: Option<WebGlTexture> = None;
                            let mut metallic_roughness_tex: Option<WebGlTexture> = None;
                            let mut ao_tex: Option<WebGlTexture> = None;
                            let mut emissive_tex: Option<WebGlTexture> = None;
                            let mut albedo_factor = Vec4::ONE;
                            let mut metallic_factor =  1.0;
                            let mut roughness_factor = 1.0;
                            let mut emissive_factor = Vec3::ONE;
                            let mut double_sided = false;
                            let mut alpha_factor = 1.0;
                            let mut alpha_flag = AlphaFlag::Opaque;

                            if let Some(mat_id) = primitive.m_material_id.0
                            {
                                let material = &model.m_materials[mat_id as usize];

                                if let Some(img_id) = material.m_albedo_image_id.0
                                {
                                    let image = &model.m_images[img_id as usize];

                                    web_sys::console::log_1(&"before albedo tex".into());

                                    albedo_tex = generate_create_texture(&*inner_context.borrow_mut(),image.m_width,image.m_height,&image.m_pixels);
                                }

                                if let Some(img_id) = material.m_normal_image_id.0
                                {
                                    let image = &model.m_images[img_id as usize];

                                    web_sys::console::log_1(&"before normal tex".into());

                                    normal_tex = generate_create_texture(&*inner_context.borrow_mut(),image.m_width,image.m_height,&image.m_pixels);
                                }

                                if let Some(img_id) = material.m_metallic_roughness_image_id.0
                                {
                                    let image = &model.m_images[img_id as usize];

                                    web_sys::console::log_1(&"before metallic_roughness tex".into());

                                    metallic_roughness_tex = generate_create_texture(&*inner_context.borrow_mut(),image.m_width,image.m_height,&image.m_pixels);
                                }

                                if let Some(img_id) = material.m_ao_image_id.0
                                {
                                    let image = &model.m_images[img_id as usize];

                                    web_sys::console::log_1(&"before ao tex".into());

                                    ao_tex = generate_create_texture(&*inner_context.borrow_mut(),image.m_width,image.m_height,&image.m_pixels);
                                }

                                if let Some(img_id) = material.m_emissive_image_id.0
                                {
                                    let image = &model.m_images[img_id as usize];

                                    web_sys::console::log_1(&"before emissive tex".into());

                                    emissive_tex = generate_create_texture(&*inner_context.borrow_mut(),image.m_width,image.m_height,&image.m_pixels);
                                }

                                albedo_factor = material.m_albedo_factor.clone();

                                metallic_factor = material.m_metallic_factor.clone();

                                roughness_factor = material.m_roughness_factor.clone();

                                emissive_factor = material.m_emissive_factor.clone();

                                double_sided = material.m_double_sided;
                                
                                alpha_factor = material.m_alpha_factor;

                                alpha_flag = material.m_alpha_flag.clone();
                            }

                            let vbo = Some(create_vbo(&inner_context,vertices_start, vertices_end));
                            let ibo = Some(create_ibo(&inner_context,indices_start, indices_end));

                            bo_list.push(RenderInfo::PBR(PBRSetting
                            {
                                m_vertex_bo: vbo,
                                m_index_bo: ibo,
                                m_index_len: primitive.m_indices.len() as u32,
                                m_albedo_tex: albedo_tex,
                                m_normal_tex: normal_tex,
                                m_metallic_roughness_tex: metallic_roughness_tex,
                                m_ao_tex: ao_tex,
                                m_emissive_tex: emissive_tex,
                                m_albedo_factor: albedo_factor,
                                m_metallic_factor: metallic_factor,
                                m_roughness_factor: roughness_factor,
                                m_emissive_factor: emissive_factor,
                                m_double_sided: double_sided,
                                m_alpha_factor: alpha_factor,
                                m_alpha_flag: alpha_flag,
                            }));
                        }

                        if bo_list.is_empty() == false
                        {
                            renderinfo_map.insert(i.to_string(),(mesh.m_transform.clone(),bo_list));
                        }
                    }
                    renderinfo_map
                },
                BufferObjInfo::Flat(name) =>
                {   
                    let mut renderinfo_map = HashMap::new();

                    let vbo = Some(inner_context.borrow_mut().create_buffer().expect("failed to create buffer"));
                    let cbo = Some(inner_context.borrow_mut().create_buffer().expect("failed to create buffer"));
                    let ibo = Some(inner_context.borrow_mut().create_buffer().expect("failed to create buffer"));
                    
                    renderinfo_map.insert(name.clone(),(Mat4::IDENTITY,vec![RenderInfo::Flat(FlatSetting{   m_vertices_bo: vbo,
                                                                                                    m_vertices: Vec::new(), 
                                                                                                    m_colors_bo: cbo, 
                                                                                                    m_colors: Vec::new(), 
                                                                                                    m_index_bo: ibo,
                                                                                                    m_index: Vec::new(), })]));
                    renderinfo_map
                }
            };

            vec![EventHandler::new(vec![String::from("BufferObj1")],buf_obj_msg.clone())]
        }));

        let raw_input = self.m_raw_input.clone();

        let ui_captured = self.m_ui_captured.clone();

        let temp_canvas = self.m_core.as_ref().unwrap().m_canvas.clone();

        self.m_listener.register::<MouseEvent>
        (&String::from("Input"),Box::new(move |handle:&mut EventHandler|->Vec<EventHandler>
            {            
                let mouse = handle.get_event::<MouseEvent>();

                match &*mouse
                {
                    MouseEvent::MouseScroll(scroll) =>
                    {
                        raw_input.borrow_mut().scroll_delta = egui::Vec2::new(scroll.x,scroll.y);
                    },
                    MouseEvent::MouseMove(pos) =>
                    {
                        let x = pos.x * temp_canvas.width() as f32;
                        let y = pos.y * temp_canvas.height() as f32;

                        (*raw_input.borrow_mut()).events.push(egui::Event::PointerMoved(egui::Pos2::new(x,y)));
                    },
                    MouseEvent::MouseDown(btn,pos) =>
                    {
                        let x = pos.x * temp_canvas.width() as f32;
                        let y = pos.y * temp_canvas.height() as f32;

                        let event_ptrbtn = egui::Event::PointerButton
                        {
                            pos: egui::Pos2::new(x,y),
                            button: match btn
                            {
                                MouseButton::Primary => egui::PointerButton::Primary,
                                MouseButton::Auxillary => egui::PointerButton::Middle,
                                MouseButton::Secondary => egui::PointerButton::Secondary
                            },
                            pressed: true,
                            modifiers: Default::default()
                        };
                        (*raw_input.borrow_mut()).events.push(event_ptrbtn);
                    },
                    MouseEvent::MouseUp(btn,pos) =>
                    {
                        let x = pos.x * temp_canvas.width() as f32;
                        let y = pos.y * temp_canvas.height() as f32;

                        let event_ptrbtn = egui::Event::PointerButton
                        {
                            pos: egui::Pos2::new(x,y),
                            button: match btn
                            {
                                MouseButton::Primary => egui::PointerButton::Primary,
                                MouseButton::Auxillary => egui::PointerButton::Middle,
                                MouseButton::Secondary => egui::PointerButton::Secondary
                            },
                            pressed: false,
                            modifiers: Default::default()
                        };
                        (*raw_input.borrow_mut()).events.push(event_ptrbtn);
                    },
                    _ => { println!("Not supported MouseEvent!");}
                }
    
                /*if *ui_captured.borrow_mut()
                {
                    return None;
                }*/

                Vec::new()
            }));

           let ext = context.borrow_mut().get_extension("WEBGL_depth_texture");

             /*context.borrow_mut().tex_image_2d_with_i32_and_i32_and_i32_and_format_and_type_and_opt_u8_array
            (WebGlRenderingContext::TEXTURE_2D, 0, WebGlRenderingContext::DEPTH_COMPONENT as i32, 512, 512, 0, WebGlRenderingContext::DEPTH_COMPONENT, WebGlRenderingContext::UNSIGNED_SHORT, None);*/

        /*=========================Shaders========================*/
        context.borrow_mut().get_extension("OES_texture_float");
        context.borrow_mut().get_extension("OES_texture_float_linear");
        context.borrow_mut().get_extension("OES_element_index_uint");
        
        //let (cube_vertices_start,cube_vertices_end) = calculate_loc_begin_end::<f32,[f32;3]>(&CUBE_VERTICES.to_vec());
        //let (cube_verts_start,cube_verts_end) = calculate_loc_begin_end::<f32,[f32;2]>(&CUBE_VERTS.to_vec());
        //let (cube_indices_start,cube_indices_end) = calculate_loc_begin_end::<u32,u32>(&CUBE_INDICES.to_vec());

        let cube_vertices_start: u32 = CUBE_VERTICES.as_ptr() as u32 / 4;
        let cube_vertices_end: u32 = cube_vertices_start + (3 * CUBE_VERTICES.len()) as u32;
        
        let cube_verts_start: u32 = CUBE_VERTS.as_ptr() as u32 / 4;
        let cube_verts_end: u32 = cube_verts_start + (2 * CUBE_VERTS.len()) as u32;

        let cube_indices_start: u32 = CUBE_INDICES.as_ptr() as u32 / 4;
        let cube_indices_end: u32 = cube_indices_start + CUBE_INDICES.len() as u32;

        self.m_cube_vertices = Some(create_vbo(&context,cube_vertices_start, cube_vertices_end));
        self.m_cube_verts = Some(create_vbo(&context,cube_verts_start, cube_verts_end));
        self.m_cube_indices = Some(create_ibo(&context,cube_indices_start, cube_indices_end));

        //let (fullscreen_uv_start,fullscreen_uv_end) = calculate_loc_begin_end::<f32,[f32;2]>(&FULLSCREEN_UV.to_vec());
        //let (fullscreen_indices_start,fullscreen_indices_end) = calculate_loc_begin_end::<u32,u32>(&FULLSCREEN_INDICES.to_vec());

        let fullscreen_indices_start: u32 = FULLSCREEN_INDICES.as_ptr() as u32 / 4;
        let fullscreen_indices_end: u32 = fullscreen_indices_start + FULLSCREEN_INDICES.len() as u32;

        let fullscreen_uv_start: u32 = FULLSCREEN_UV.as_ptr() as u32 / 4;
        let fullscreen_uv_end: u32 = fullscreen_uv_start + (2 * FULLSCREEN_UV.len()) as u32;

        self.m_fullscreen_uv = Some(create_vbo(&context,fullscreen_uv_start, fullscreen_uv_end));
        self.m_fullscreen_indices = Some(create_ibo(&context,fullscreen_indices_start, fullscreen_indices_end));

        let hdr_buf = (include_bytes!("../../../assets/environment/venice_sunrise.jpg")).to_vec();

        if let Some((width,height,format,pixels)) = decode_buffer(&hdr_buf)
        {
            //web_sys::console::log_2(&"decode_buffer: ".into(),&(pixels.len() as u32).into());

            self.m_hdr_tex = generate_create_texture_with_format(&*context.borrow_mut(),width,height,format,&pixels);
        }

        self.m_white_tex = generate_create_texture_with_format(&*context.borrow_mut(),1,1,WebGlRenderingContext::RGB,&vec![255,255,255]);

        self.m_black_tex = generate_create_texture_with_format(&*context.borrow_mut(),1,1,WebGlRenderingContext::RGB,&vec![0,0,0]);

        context.borrow_mut().enable(WebGlRenderingContext::CULL_FACE);

        // Enable the depth test
        context.borrow_mut().enable(WebGlRenderingContext::DEPTH_TEST);
    
        // Clear the color buffer bit
        context.borrow_mut().clear(WebGlRenderingContext::COLOR_BUFFER_BIT | WebGlRenderingContext::DEPTH_BUFFER_BIT);
    
        // Set the view port
        context.borrow_mut().viewport(0, 0, (self.m_sampling_size.x * 1.5) as i32, self.m_sampling_size.y as i32);

        /*self.m_shadow_depth_tex = context.borrow_mut().create_texture();
        context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_2D, self.m_shadow_depth_tex.as_ref());
        let alignment = 1;
        context.borrow_mut().pixel_storei(WebGlRenderingContext::UNPACK_ALIGNMENT, alignment);
        context.borrow_mut().tex_image_2d_with_i32_and_i32_and_i32_and_format_and_type_and_opt_u8_array(WebGlRenderingContext::TEXTURE_2D, 0, WebGlRenderingContext::RGBA as i32, 512, 512, 0, WebGlRenderingContext::RGBA, WebGlRenderingContext::UNSIGNED_BYTE, None);
        context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MIN_FILTER, WebGlRenderingContext::NEAREST as i32);
        context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MAG_FILTER, WebGlRenderingContext::NEAREST as i32);
        context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_S, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
        context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_T, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
        //context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_2D, None);

        self.m_shadow_fbo = context.borrow_mut().create_framebuffer();
        context.borrow_mut().bind_framebuffer(WebGlRenderingContext::FRAMEBUFFER,self.m_shadow_fbo.as_ref());

        // attach it to currently bound framebuffer object
        context.borrow_mut().framebuffer_texture_2d(WebGlRenderingContext::FRAMEBUFFER, WebGlRenderingContext::COLOR_ATTACHMENT0, WebGlRenderingContext::TEXTURE_2D, self.m_shadow_depth_tex.as_ref(), 0);

        context.borrow_mut().bind_framebuffer(WebGlRenderingContext::FRAMEBUFFER,None);*/

        let half_sample : (i32,i32) = ((0.5 * self.m_sampling_size.x) as i32, (0.5 * self.m_sampling_size.y) as i32);

        /*=========================Equirectangular Shaders========================*/
        web_sys::console::log_1(&"Equirectangular Shaders!".into());
        {
            let vertex_shader_source = include_str!("./shader/unproject_cubemap_tex_vert.glsl");
            let fragment_shader_source = include_str!("./shader/equirectangular_to_cube_faces_frag.glsl");

            let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);
            
            program.use_program(true);

            program.set_attribute(String::from("cube_vert"));
            program.set_attribute(String::from("vert"));
        
            program.set_uniform(String::from("equirectangular_texture"),ShaderUniform::UniformTexture2D(self.m_hdr_tex.clone()));

            program.use_program(false);

            self.m_shaders_map.insert(String::from("equirectangular"),program);

            if let Some(shader) = self.m_shaders_map.get_mut(&String::from("equirectangular"))
            {
                shader.set_texture_active();

                shader.use_program(true);

                context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, self.m_cube_vertices.as_ref());
                context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["cube_vert"], 3, WebGlRenderingContext::FLOAT, false, 0, 0);
                context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["cube_vert"]);

                context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, self.m_cube_verts.as_ref());
                context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["vert"], 2, WebGlRenderingContext::FLOAT, false, 0, 0);
                context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["vert"]);

                shader.bind_uniforms();

                context.borrow_mut().bind_buffer(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,self.m_cube_indices.as_ref());
                context.borrow_mut().draw_elements_with_i32(WebGlRenderingContext::TRIANGLES,CUBE_INDICES.len() as i32,WebGlRenderingContext::UNSIGNED_INT,0,);

                shader.use_program(false);
            }
            
            /*if context.borrow_mut().check_framebuffer_status(WebGlRenderingContext::FRAMEBUFFER) != WebGlRenderingContext::FRAMEBUFFER_COMPLETE
            {
                web_sys::console::log_1(&"ERROR::FRAMEBUFFER:: Framebuffer is not complete!".into());
            }*/

            //context.borrow_mut().pixel_storei(WebGlRenderingContext::PACK_ALIGNMENT,1);

            self.m_cube_tex = context.borrow_mut().create_texture();

            context.borrow_mut().active_texture(WebGlRenderingContext::TEXTURE0);
            context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_CUBE_MAP,self.m_cube_tex.as_ref());

            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_WRAP_S, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_WRAP_T, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_MAG_FILTER, WebGlRenderingContext::LINEAR as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_MIN_FILTER, WebGlRenderingContext::LINEAR as i32);

            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_POSITIVE_X,0,WebGlRenderingContext::RGBA, 0,   0,     half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_NEGATIVE_X,0,WebGlRenderingContext::RGBA, 0,   half_sample.1,   half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_POSITIVE_Y,0,WebGlRenderingContext::RGBA, half_sample.0, 0,   half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_NEGATIVE_Y,0,WebGlRenderingContext::RGBA, half_sample.0, half_sample.1, half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_POSITIVE_Z,0,WebGlRenderingContext::RGBA, 2 * half_sample.0, 0,   half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_NEGATIVE_Z,0,WebGlRenderingContext::RGBA, 2 * half_sample.0, half_sample.1, half_sample.0, half_sample.1,0);

            context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_CUBE_MAP,None);
        }

        //context.borrow_mut().bind_framebuffer(WebGlRenderingContext::FRAMEBUFFER,None);
    
        // Clear the color buffer bit
        context.borrow_mut().clear(WebGlRenderingContext::COLOR_BUFFER_BIT | WebGlRenderingContext::DEPTH_BUFFER_BIT);
    
        /*=========================Specular Shaders========================*/        
        web_sys::console::log_1(&"Specular Shaders!".into());
        {
            let vertex_shader_source = include_str!("./shader/unproject_cubemap_tex_vert.glsl");
            let fragment_shader_source = include_str!("./shader/env_to_specular_frag.glsl");
        
            let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);

            program.use_program(true);

            program.set_attribute(String::from("cube_vert"));
            program.set_attribute(String::from("vert"));

            program.set_uniform(String::from("roughness"),ShaderUniform::UniformFloat(0.25));
            
            program.set_uniform(String::from("env_texture"),ShaderUniform::UniformTextureCubeMap(self.m_cube_tex.clone()));

            program.use_program(false);
            
            self.m_shaders_map.insert(String::from("specular"),program);

            if let Some(shader) = self.m_shaders_map.get_mut(&String::from("specular"))
            {
                shader.set_texture_active();

                shader.use_program(true);

                context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, self.m_cube_vertices.as_ref());
                context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["cube_vert"], 3, WebGlRenderingContext::FLOAT, false, 0, 0);
                context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["cube_vert"]);

                context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, self.m_cube_verts.as_ref());
                context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["vert"], 2, WebGlRenderingContext::FLOAT, false, 0, 0);
                context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["vert"]);

                shader.bind_uniforms();

                context.borrow_mut().bind_buffer(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,self.m_cube_indices.as_ref());
                context.borrow_mut().draw_elements_with_i32(WebGlRenderingContext::TRIANGLES,CUBE_INDICES.len() as i32,WebGlRenderingContext::UNSIGNED_INT,0,);

                shader.use_program(false);
            }

            self.m_specular_tex = context.borrow_mut().create_texture();

            context.borrow_mut().active_texture(WebGlRenderingContext::TEXTURE0);
            context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_CUBE_MAP,self.m_specular_tex.as_ref());

            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_WRAP_S, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_WRAP_T, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_MAG_FILTER, WebGlRenderingContext::LINEAR as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_MIN_FILTER, WebGlRenderingContext::LINEAR as i32);

            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_POSITIVE_X,0,WebGlRenderingContext::RGBA, 0,   0,     half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_NEGATIVE_X,0,WebGlRenderingContext::RGBA, 0,   half_sample.1,   half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_POSITIVE_Y,0,WebGlRenderingContext::RGBA, half_sample.0, 0,   half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_NEGATIVE_Y,0,WebGlRenderingContext::RGBA, half_sample.0, half_sample.1, half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_POSITIVE_Z,0,WebGlRenderingContext::RGBA, 2 * half_sample.0, 0,   half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_NEGATIVE_Z,0,WebGlRenderingContext::RGBA, 2 * half_sample.0, half_sample.1, half_sample.0, half_sample.1,0);

            context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_CUBE_MAP,None);
        }

        // Clear the color buffer bit
        context.borrow_mut().clear(WebGlRenderingContext::COLOR_BUFFER_BIT | WebGlRenderingContext::DEPTH_BUFFER_BIT);
    
        /*=========================Irradiance Shaders========================*/
        web_sys::console::log_1(&"Irradiance Shaders!".into());

        {
            let vertex_shader_source = include_str!("./shader/unproject_cubemap_tex_vert.glsl");
            let fragment_shader_source = include_str!("./shader/env_to_irradiance_frag.glsl");
        
            let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);

            program.use_program(true);

            program.set_attribute(String::from("cube_vert"));
            program.set_attribute(String::from("vert"));

            program.set_uniform(String::from("env_texture"),ShaderUniform::UniformTextureCubeMap(self.m_specular_tex.clone()));

            program.use_program(false);
            
            self.m_shaders_map.insert(String::from("irradiance"),program);

            if let Some(shader) = self.m_shaders_map.get_mut(&String::from("irradiance"))
            {
                shader.set_texture_active();

                shader.use_program(true);

                context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, self.m_cube_vertices.as_ref());
                context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["cube_vert"], 3, WebGlRenderingContext::FLOAT, false, 0, 0);
                context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["cube_vert"]);

                context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, self.m_cube_verts.as_ref());
                context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["vert"], 2, WebGlRenderingContext::FLOAT, false, 0, 0);
                context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["vert"]);

                shader.bind_uniforms();

                context.borrow_mut().bind_buffer(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,self.m_cube_indices.as_ref());
                context.borrow_mut().draw_elements_with_i32(WebGlRenderingContext::TRIANGLES,CUBE_INDICES.len() as i32,WebGlRenderingContext::UNSIGNED_INT,0,);

                shader.use_program(false);
            }

            self.m_irradiance_tex = context.borrow_mut().create_texture();

            context.borrow_mut().active_texture(WebGlRenderingContext::TEXTURE0);
            context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_CUBE_MAP,self.m_irradiance_tex.as_ref());

            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_WRAP_S, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_WRAP_T, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_MAG_FILTER, WebGlRenderingContext::LINEAR as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_MIN_FILTER, WebGlRenderingContext::LINEAR as i32);

            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_POSITIVE_X,0,WebGlRenderingContext::RGBA, 0,   0,     half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_NEGATIVE_X,0,WebGlRenderingContext::RGBA, 0,   half_sample.1,   half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_POSITIVE_Y,0,WebGlRenderingContext::RGBA, half_sample.0, 0,   half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_NEGATIVE_Y,0,WebGlRenderingContext::RGBA, half_sample.0, half_sample.1, half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_POSITIVE_Z,0,WebGlRenderingContext::RGBA, 2 * half_sample.0, 0,   half_sample.0, half_sample.1,0);
            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_NEGATIVE_Z,0,WebGlRenderingContext::RGBA, 2 * half_sample.0, half_sample.1, half_sample.0, half_sample.1,0);

            context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_CUBE_MAP,None);

            context.borrow_mut().clear(WebGlRenderingContext::COLOR_BUFFER_BIT | WebGlRenderingContext::DEPTH_BUFFER_BIT);
        }

        // Set the view port
        context.borrow_mut().viewport(0, 0, self.m_sampling_size.x as i32, self.m_sampling_size.y as i32);
        /*=========================BRDF Shaders========================*/
        web_sys::console::log_1(&"BRDF Shaders!".into());

        {
            let vertex_shader_source = include_str!("./shader/fullscreen_triangle_vert.glsl");
            let fragment_shader_source = include_str!("./shader/integrate_spec_brdf_frag.glsl");
        
            let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);

            program.use_program(true);

            program.set_attribute(String::from("a_uv"));

            program.use_program(false);

            self.m_shaders_map.insert(String::from("brdf"),program);

            if let Some(shader) = self.m_shaders_map.get_mut(&String::from("brdf"))
            {
                shader.use_program(true);

                context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, self.m_fullscreen_uv.as_ref());
                context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["a_uv"], 2, WebGlRenderingContext::FLOAT, false, 0, 0);
                context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["a_uv"]);

                shader.bind_uniforms();

                context.borrow_mut().bind_buffer(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,self.m_fullscreen_indices.as_ref());
                context.borrow_mut().draw_elements_with_i32(WebGlRenderingContext::TRIANGLES,FULLSCREEN_INDICES.len() as i32,WebGlRenderingContext::UNSIGNED_INT,0,);

                shader.use_program(false);
            }

            self.m_spec_brdf_tex = context.borrow_mut().create_texture();

            context.borrow_mut().active_texture(WebGlRenderingContext::TEXTURE0);
            context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_2D,self.m_spec_brdf_tex.as_ref());

            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_S, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_T, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MAG_FILTER, WebGlRenderingContext::LINEAR as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MIN_FILTER, WebGlRenderingContext::LINEAR as i32);

            context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_2D,0,WebGlRenderingContext::RGBA, 0, 0, self.m_sampling_size.x as i32, self.m_sampling_size.y as i32, 0);

            context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_2D,None);

            context.borrow_mut().clear(WebGlRenderingContext::COLOR_BUFFER_BIT | WebGlRenderingContext::DEPTH_BUFFER_BIT);
        }

        /*=========================Environment Shaders========================*/
        web_sys::console::log_1(&"Environment Shaders!".into());

        let fov_y: f32 = 60.0;

        *self.m_camera_pos.borrow_mut() = Vec3::new(0.0,0.0,-1.0);

        *self.m_proj_mtx.borrow_mut() = Mat4::perspective_rh(fov_y.to_radians(),(canvas.width() as f32)/ (canvas.height() as f32),0.1, 1000.0);

        *self.m_view_mtx.borrow_mut() = Mat4::look_at_rh(self.m_camera_pos.borrow_mut().clone(),Vec3::new(0.0,0.0,0.0),Vec3::new(0.0,1.0,0.0));

        let inv_view_mtx = self.m_view_mtx.borrow_mut().inverse();

        {
            let vertex_shader_source = include_str!("./shader/environment_map_vert.glsl");
            let fragment_shader_source = include_str!("./shader/environment_map_frag.glsl");
        
            let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);

            program.use_program(true);

            program.set_attribute(String::from("a_pos"));

            program.set_uniform(String::from("proj"),ShaderUniform::UniformMat4(self.m_proj_mtx.borrow_mut().clone()));
            program.set_uniform(String::from("view"),ShaderUniform::UniformMat4(self.m_view_mtx.borrow_mut().clone()));

            program.set_uniform(String::from("cube_map"),ShaderUniform::UniformTextureCubeMap(self.m_specular_tex.clone()));

            program.use_program(false);
            
            self.m_shaders_map.insert(String::from("environment"),program);
        }

        /*=========================PBR Shaders========================*/
        web_sys::console::log_1(&"PBR Shaders!".into());

        {
            let vertex_shader_source = include_str!("./shader/pbr_vert.glsl");
            let fragment_shader_source = include_str!("./shader/pbr_frag.glsl");
        
            let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);

            program.use_program(true);

            program.set_attribute(String::from("a_pos"));
            program.set_attribute(String::from("a_norm"));
            program.set_attribute(String::from("a_tang"));
            program.set_attribute(String::from("a_uv"));

            program.set_uniform(String::from("model_mat"),ShaderUniform::UniformMat4(Mat4::IDENTITY));

            program.set_uniform(String::from("depth_mat"),ShaderUniform::UniformMat4(Mat4::IDENTITY));

            program.set_uniform(String::from("proj"),ShaderUniform::UniformMat4(self.m_proj_mtx.borrow_mut().clone()));
            program.set_uniform(String::from("view"),ShaderUniform::UniformMat4(self.m_view_mtx.borrow_mut().clone()));

            program.set_uniform(String::from("texture_size"),ShaderUniform::UniformVec2(self.m_sampling_size.clone()));

            program.set_uniform(String::from("opacity"),ShaderUniform::UniformFloat(1.0));
            //program.set_uniform(String::from("alpha_val"),ShaderUniform::UniformFloat(1.0));

            program.set_uniform(String::from("light_pos"),ShaderUniform::UniformVec3(Vec3::new(1.0,1.0,-1.0)));
            program.set_uniform(String::from("light_intensity"),ShaderUniform::UniformFloat(1.5));
            program.set_uniform(String::from("light_color"),ShaderUniform::UniformVec3(Vec3::ONE));

            program.set_uniform(String::from("shadow_depth_map"),ShaderUniform::UniformTexture2D(None));

            program.set_uniform(String::from("spec_cube_map"),ShaderUniform::UniformTextureCubeMap(self.m_specular_tex.clone()));
            program.set_uniform(String::from("irradiance_cube_map"),ShaderUniform::UniformTextureCubeMap(self.m_irradiance_tex.clone()));
            program.set_uniform(String::from("spec_brdf_map"),ShaderUniform::UniformTexture2D(self.m_spec_brdf_tex.clone()));

            program.set_uniform(String::from("albedo_map"),ShaderUniform::UniformTexture2D(self.m_white_tex.clone()));
            program.set_uniform(String::from("normal_map"),ShaderUniform::UniformTexture2D(self.m_black_tex.clone()));
            program.set_uniform(String::from("metallic_roughness_map"),ShaderUniform::UniformTexture2D(self.m_white_tex.clone()));
            program.set_uniform(String::from("ao_map"),ShaderUniform::UniformTexture2D(self.m_white_tex.clone()));
            program.set_uniform(String::from("emissive_map"),ShaderUniform::UniformTexture2D(self.m_white_tex.clone()));

            program.set_uniform(String::from("albedo_factor"),ShaderUniform::UniformVec4(Vec4::ONE));
            program.set_uniform(String::from("metallic_factor"),ShaderUniform::UniformFloat(1.0));
            program.set_uniform(String::from("roughness_factor"),ShaderUniform::UniformFloat(1.0));
            program.set_uniform(String::from("emissive_factor"),ShaderUniform::UniformVec3(Vec3::ONE));

            program.use_program(false);
            
            self.m_shaders_map.insert(String::from("pbr"),program);
        }

        /*=========================Shadow Shaders========================*/
        web_sys::console::log_1(&"Shadow Shaders!".into());

        {
            let vertex_shader_source = include_str!("./shader/shadow_vert.glsl");
            let fragment_shader_source = include_str!("./shader/shadow_frag.glsl");
        
            let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);

            program.use_program(true);

            program.set_attribute(String::from("a_pos"));

            program.set_uniform(String::from("model_mat"),ShaderUniform::UniformMat4(Mat4::IDENTITY));

            program.set_uniform(String::from("depth_mat"),ShaderUniform::UniformMat4(Mat4::IDENTITY));

            program.use_program(false);
            
            self.m_shaders_map.insert(String::from("shadow"),program);

            self.m_shadow_depth_tex = context.borrow_mut().create_texture();

            context.borrow_mut().active_texture(WebGlRenderingContext::TEXTURE0);
            context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_2D,self.m_shadow_depth_tex.as_ref());

            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_S, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_T, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MAG_FILTER, WebGlRenderingContext::LINEAR as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MIN_FILTER, WebGlRenderingContext::LINEAR as i32);

            context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_2D,None);
        }

        /*=========================Flat Shaders========================*/
        web_sys::console::log_1(&"Flat Shaders!".into());

        {
            let vertex_shader_source = include_str!("./shader/flat_vert.glsl");
            let fragment_shader_source = include_str!("./shader/flat_frag.glsl");
        
            let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);

            program.use_program(true);

            program.set_attribute(String::from("a_pos"));
            program.set_attribute(String::from("a_col"));

            program.set_uniform(String::from("model_mat"),ShaderUniform::UniformMat4(Mat4::IDENTITY));

            program.set_uniform(String::from("proj"),ShaderUniform::UniformMat4(self.m_proj_mtx.borrow_mut().clone()));
            program.set_uniform(String::from("view"),ShaderUniform::UniformMat4(self.m_view_mtx.borrow_mut().clone()));

            program.use_program(false);
            
            self.m_shaders_map.insert(String::from("flat"),program);
        }

        /*=========================ToneMap Shaders========================*/
        web_sys::console::log_1(&"ToneMap Shaders!".into());

        {
            let vertex_shader_source = include_str!("./shader/fullscreen_triangle_vert.glsl");
            let fragment_shader_source = include_str!("./shader/tonemap_frag.glsl");
        
            let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);

            program.use_program(true);

            program.set_attribute(String::from("a_uv"));

            program.set_uniform(String::from("hdr_tex"),ShaderUniform::UniformTexture2D(None));

            program.set_uniform(String::from("exposure"),ShaderUniform::UniformFloat(1.0));

            program.use_program(false);
            
            self.m_shaders_map.insert(String::from("tonemap"),program);

            self.m_tonemap_tex = context.borrow_mut().create_texture();

            context.borrow_mut().active_texture(WebGlRenderingContext::TEXTURE0);
            context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_2D,self.m_tonemap_tex.as_ref());

            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_S, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_T, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MAG_FILTER, WebGlRenderingContext::LINEAR as i32);
            context.borrow_mut().tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MIN_FILTER, WebGlRenderingContext::LINEAR as i32);

            context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_2D,None);
        }

        /*=========================Depth Shaders========================*/
        web_sys::console::log_1(&"Depth Shaders!".into());

        {
            let vertex_shader_source = include_str!("./shader/fullscreen_triangle_vert.glsl");
            let fragment_shader_source = include_str!("./shader/depth_frag.glsl");
        
            let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);

            program.use_program(true);

            program.set_attribute(String::from("a_uv"));

            program.set_uniform(String::from("shadow_depth_map"),ShaderUniform::UniformTexture2D(None));

            program.use_program(false);
            
            self.m_shaders_map.insert(String::from("depth"),program);
        }

        /*=========================World Shaders========================*/
        web_sys::console::log_1(&"World Shaders!".into());
        
        {
            let vertex_shader_source = include_str!("./shader/vert.glsl");
            let fragment_shader_source = include_str!("./shader/frag.glsl");
        
            let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);

            program.use_program(true);

            program.set_attribute(String::from("a_pos"));
            program.set_attribute(String::from("a_norm"));
            program.set_attribute(String::from("a_uv"));

            program.set_uniform(String::from("proj"),ShaderUniform::UniformMat4(self.m_proj_mtx.borrow_mut().clone()));
            program.set_uniform(String::from("view"),ShaderUniform::UniformMat4(self.m_view_mtx.borrow_mut().clone()));

            program.set_uniform(String::from("model_mat"),ShaderUniform::UniformMat4(Mat4::IDENTITY));

            program.set_uniform(String::from("albedo_map"),ShaderUniform::UniformTexture2D(None));

            program.use_program(false);

            self.m_shaders_map.insert(String::from("world"),program);
        }

        /*=========================UI Shaders========================*/
    
        let vertex_shader_source = include_str!("./shader/vertex_100es.glsl");
        let fragment_shader_source = include_str!("./shader/fragment_100es.glsl");
    
        let vert_shader = compile_shader(
            &context.borrow_mut(),
            WebGlRenderingContext::VERTEX_SHADER,
            vertex_shader_source,
        ).expect("Failed to compile vertex shader!");
        let frag_shader = compile_shader(
            &context.borrow_mut(),
            WebGlRenderingContext::FRAGMENT_SHADER,
            fragment_shader_source,
        ).expect("Failed to compile fragment shader!");
    
        let program = link_program(&context.borrow_mut(), &vert_shader, &frag_shader).expect("Failed to link shaders!");
        
        context.borrow_mut().use_program(Some(&program));
        
        self.m_ui_program = Some(program);

        let context_mut = context.borrow_mut();

        self.m_ui_buffer = Some(UIBuffer{
            index_buffer : context_mut.create_buffer(),
            pos_buffer : context_mut.create_buffer(),
            tc_buffer : context_mut.create_buffer(),
            color_buffer : context_mut.create_buffer()
        });
    }

    pub fn free(&mut self)
    {
        self.m_listener.deregister::<CameraEvent>(&String::from("Camera"));

        self.m_listener.deregister::<RenderEvent>(&String::from("Render"));

        self.m_listener.deregister::<BufferObjEvent>(&String::from("BufferObj0"));
    }

    pub fn update(&mut self, timer: &mut Chrono)
    {
        let context = &self.m_core.as_ref().unwrap().m_context;
        let canvas = &self.m_core.as_ref().unwrap().m_canvas;

        self.m_raw_input.borrow_mut().time = Some(timer.get_time() as f64);

        context.borrow_mut().disable(WebGlRenderingContext::SCISSOR_TEST);

        context.borrow_mut().enable(WebGlRenderingContext::CULL_FACE);
        // Enable the depth test
        context.borrow_mut().enable(WebGlRenderingContext::DEPTH_TEST);
        
        context.borrow_mut().enable(WebGlRenderingContext::BLEND);

        context.borrow_mut().blend_func(WebGlRenderingContext::SRC_ALPHA, WebGlRenderingContext::ONE_MINUS_SRC_ALPHA);

        // Set the view port
        context.borrow_mut().viewport(0, 0, self.m_sampling_size.x as i32, self.m_sampling_size.y as i32);

        context.borrow_mut().clear_color(1.0, 1.0, 1.0, 1.0);

        context.borrow_mut().clear_depth(1.0);
    
        //context.borrow_mut().bind_framebuffer(WebGlRenderingContext::FRAMEBUFFER,self.m_shadow_fbo.as_ref());
        // Clear the color buffer bit
        context.borrow_mut().clear(WebGlRenderingContext::COLOR_BUFFER_BIT | WebGlRenderingContext::DEPTH_BUFFER_BIT);
        
        /*if let Ok(max_count) = context.borrow_mut().get_parameter(WebGlRenderingContext::MAX_COMBINED_TEXTURE_IMAGE_UNITS)
        {
            web_sys::console::log_2(&"MAX_COMBINED_TEXTURE_IMAGE_UNITS: ".into(),&max_count);
        }*/

        /*if let Some(shader) = self.m_shaders_map.get_mut(&String::from("environment"))
        {
            shader.set_texture_active();

            shader.use_program(true);

            context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, self.m_cube_vertices.as_ref());
            context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["a_pos"], 3, WebGlRenderingContext::FLOAT, false, 0, 0);
            context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["a_pos"]);

            shader.set_uniform(String::from("view"),ShaderUniform::UniformMat4(self.m_view_mtx.borrow_mut().clone()));

            shader.bind_uniforms();

            context.borrow_mut().bind_buffer(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,self.m_cube_indices.as_ref());
            context.borrow_mut().draw_elements_with_i32(WebGlRenderingContext::TRIANGLES,CUBE_INDICES.len() as i32,WebGlRenderingContext::UNSIGNED_INT,0,);

            shader.use_program(false);
        }*/

        let light_angle: f32 = 30.0;

        let light_pos = Quat::from_axis_angle(Vec3::new(0.0,1.0,0.0), light_angle.to_radians()) * self.m_camera_pos.borrow_mut().normalize();

        let light_fov_y: f32 = 60.0;

        let light_proj_mat = Mat4::perspective_rh(light_fov_y.to_radians(),1.0,0.5, 2.0);

        let light_view_mat = Mat4::look_at_rh(light_pos,Vec3::new(0.0,0.0,0.0),Vec3::new(0.0,1.0,0.0));

        let depth_mat = light_proj_mat * light_view_mat;

        let mut pbr_queue = self.m_pbr_queue.borrow_mut();

        pbr_queue.sort_by(|a, b| 
            {
                let mut result = Ordering::Equal;

                if a.1.m_alpha_flag == AlphaFlag::Mask
                && b.1.m_alpha_flag == AlphaFlag::Blend
                {
                    result = Ordering::Greater;
                }
                if a.1.m_alpha_flag == AlphaFlag::Mask
                && b.1.m_alpha_flag == AlphaFlag::Opaque
                {
                    result = Ordering::Greater;    
                }
                if a.1.m_alpha_flag == AlphaFlag::Blend
                && b.1.m_alpha_flag == AlphaFlag::Opaque
                {
                    result = Ordering::Greater;    
                }
                if a.1.m_alpha_flag == AlphaFlag::Blend
                && b.1.m_alpha_flag == AlphaFlag::Mask
                {
                    result = Ordering::Less;    
                }
                if a.1.m_alpha_flag == AlphaFlag::Opaque
                && b.1.m_alpha_flag == AlphaFlag::Mask
                {
                    result = Ordering::Less;    
                }
                if a.1.m_alpha_flag == AlphaFlag::Opaque
                && b.1.m_alpha_flag == AlphaFlag::Blend
                {
                    result = Ordering::Less;    
                }

                result
            });
        
        if let Some(shader) = self.m_shaders_map.get_mut(&String::from("shadow"))
        {
            let size_of_vert = std::mem::size_of::<PosNormTangTexCol>() as i32;

            for (mov_matrix, renderinfo, opacity) in pbr_queue.iter()
            {
                shader.use_program(true);
            
                // Bind vertex buffer object
                context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, renderinfo.m_vertex_bo.as_ref());

                // Point an attribute to the currently bound VBO
                context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["a_pos"], 3, WebGlRenderingContext::FLOAT, false, size_of_vert, 0);
            
                // Enable the attribute
                context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["a_pos"]);

                shader.set_uniform(String::from("depth_mat"),ShaderUniform::UniformMat4(depth_mat.clone()));

                shader.set_uniform(String::from("model_mat"),ShaderUniform::UniformMat4(mov_matrix.clone()));

                shader.bind_uniforms();

                // Bind appropriate array buffer to it
                context.borrow_mut().bind_buffer(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,renderinfo.m_index_bo.as_ref());
                // Draw the triangle
                context.borrow_mut().draw_elements_with_i32(WebGlRenderingContext::TRIANGLES,renderinfo.m_index_len.clone() as i32,WebGlRenderingContext::UNSIGNED_INT,0,);

                shader.use_program(false);
            }
        }

        context.borrow_mut().disable(WebGlRenderingContext::CULL_FACE);
        //context.borrow_mut().bind_framebuffer(WebGlRenderingContext::FRAMEBUFFER,None);

        context.borrow_mut().active_texture(WebGlRenderingContext::TEXTURE0);
        context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_2D,self.m_shadow_depth_tex.as_ref());

        context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_2D,0,WebGlRenderingContext::RGBA, 0, 0, self.m_sampling_size.x as i32, self.m_sampling_size.y as i32,0);

        context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_2D,None);

        // Set the view port
        context.borrow_mut().viewport(0, 0, canvas.width() as i32, canvas.height() as i32);

        context.borrow_mut().clear_color(0.2, 0.2, 0.2, 1.0);

        context.borrow_mut().clear(WebGlRenderingContext::COLOR_BUFFER_BIT | WebGlRenderingContext::DEPTH_BUFFER_BIT);

        if let Some(shader) = self.m_shaders_map.get_mut(&String::from("pbr"))
        {
            for (mov_matrix, pbr_setting, opacity) in pbr_queue.iter()
            {   
                shader.use_program(true);
                             
                shader.set_uniform(String::from("shadow_depth_map"),ShaderUniform::UniformTexture2D(self.m_shadow_depth_tex.clone()));

                shader.set_uniform(String::from("spec_cube_map"),ShaderUniform::UniformTextureCubeMap(self.m_specular_tex.clone()));
                shader.set_uniform(String::from("irradiance_cube_map"),ShaderUniform::UniformTextureCubeMap(self.m_irradiance_tex.clone()));
                shader.set_uniform(String::from("spec_brdf_map"),ShaderUniform::UniformTexture2D(self.m_spec_brdf_tex.clone()));

                if let Some(_) = pbr_setting.m_albedo_tex
                {
                    shader.set_uniform(String::from("albedo_map"),ShaderUniform::UniformTexture2D(pbr_setting.m_albedo_tex.clone()));
                }
                else
                {
                    shader.set_uniform(String::from("albedo_map"),ShaderUniform::UniformTexture2D(self.m_white_tex.clone()));
                }

                if let Some(_) = pbr_setting.m_normal_tex
                {
                    shader.set_uniform(String::from("normal_map"),ShaderUniform::UniformTexture2D(pbr_setting.m_normal_tex.clone()));
                }
                else
                {
                    shader.set_uniform(String::from("normal_map"),ShaderUniform::UniformTexture2D(self.m_black_tex.clone()));
                }

                if let Some(_) = pbr_setting.m_metallic_roughness_tex
                {
                    shader.set_uniform(String::from("metallic_roughness_map"),ShaderUniform::UniformTexture2D(pbr_setting.m_metallic_roughness_tex.clone()));
                }
                else
                {
                    shader.set_uniform(String::from("metallic_roughness_map"),ShaderUniform::UniformTexture2D(self.m_white_tex.clone()));
                }

                if let Some(_) = pbr_setting.m_ao_tex
                {
                    shader.set_uniform(String::from("ao_map"),ShaderUniform::UniformTexture2D(pbr_setting.m_ao_tex.clone()));
                }
                else
                {
                    shader.set_uniform(String::from("ao_map"),ShaderUniform::UniformTexture2D(self.m_white_tex.clone()));
                }

                if let Some(_) = pbr_setting.m_emissive_tex
                {
                    shader.set_uniform(String::from("emissive_map"),ShaderUniform::UniformTexture2D(pbr_setting.m_emissive_tex.clone()));
                }
                else
                {
                    shader.set_uniform(String::from("emissive_map"),ShaderUniform::UniformTexture2D(self.m_white_tex.clone()));
                }

                shader.use_program(false);

                shader.set_texture_active();

                shader.use_program(true);
            
                let size_of_vert = std::mem::size_of::<PosNormTangTexCol>() as i32;

                // Bind vertex buffer object
                context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, pbr_setting.m_vertex_bo.as_ref());

                // Point an attribute to the currently bound VBO
                context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["a_pos"], 3, WebGlRenderingContext::FLOAT, false, size_of_vert, 0);
            
                // Enable the attribute
                context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["a_pos"]);

                // Point an attribute to the currently bound VBO
                context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["a_norm"], 3, WebGlRenderingContext::FLOAT, false, size_of_vert, 12);
            
                // Enable the attribute
                context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["a_norm"]);

                // Point an attribute to the currently bound VBO
                context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["a_tang"], 4, WebGlRenderingContext::FLOAT, false, size_of_vert, 24);

                // Enable the attribute
                context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["a_tang"]);

                // Point an attribute to the currently bound VBO
                context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["a_uv"], 2, WebGlRenderingContext::FLOAT, false, size_of_vert, 40);
            
                // Enable the attribute
                context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["a_uv"]);

                shader.set_uniform(String::from("opacity"),ShaderUniform::UniformFloat(opacity.clone()));

                //shader.set_uniform(String::from("alpha_val"),ShaderUniform::UniformFloat(pbr_setting.m_alpha_factor));

                shader.set_uniform(String::from("depth_mat"),ShaderUniform::UniformMat4(depth_mat.clone()));
        
                shader.set_uniform(String::from("light_pos"),ShaderUniform::UniformVec3(light_pos.clone()));

                shader.set_uniform(String::from("view"),ShaderUniform::UniformMat4(self.m_view_mtx.borrow_mut().clone()));

                shader.set_uniform(String::from("proj"),ShaderUniform::UniformMat4(self.m_proj_mtx.borrow_mut().clone()));

                shader.set_uniform(String::from("model_mat"),ShaderUniform::UniformMat4(mov_matrix.clone()));

                shader.set_uniform(String::from("albedo_factor"),ShaderUniform::UniformVec4(pbr_setting.m_albedo_factor));

                shader.set_uniform(String::from("metallic_factor"),ShaderUniform::UniformFloat(pbr_setting.m_metallic_factor));

                shader.set_uniform(String::from("roughness_factor"),ShaderUniform::UniformFloat(pbr_setting.m_roughness_factor));

                shader.set_uniform(String::from("emissive_factor"),ShaderUniform::UniformVec3(pbr_setting.m_emissive_factor.clone()));

                shader.bind_uniforms();

                // Bind appropriate array buffer to it
                context.borrow_mut().bind_buffer(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,pbr_setting.m_index_bo.as_ref());
                // Draw the triangle
                context.borrow_mut().draw_elements_with_i32(WebGlRenderingContext::TRIANGLES,pbr_setting.m_index_len.clone() as i32,WebGlRenderingContext::UNSIGNED_INT,0,);

                shader.use_program(false);
            }
        }

        pbr_queue.clear();

        /*context.borrow_mut().active_texture(WebGlRenderingContext::TEXTURE0);
        context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_2D,self.m_tonemap_tex.as_ref());

        context.borrow_mut().copy_tex_image_2d(WebGlRenderingContext::TEXTURE_2D,0,WebGlRenderingContext::RGBA, 0, 0, canvas.width() as i32, canvas.height() as i32,0);

        context.borrow_mut().bind_texture(WebGlRenderingContext::TEXTURE_2D,None);    

        if let Some(shader) = self.m_shaders_map.get_mut(&String::from("tonemap"))
        {
            shader.set_uniform(String::from("hdr_tex"),ShaderUniform::UniformTexture2D(self.m_tonemap_tex.clone()));

            shader.set_texture_active();

            shader.use_program(true);

            context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, self.m_fullscreen_uv.as_ref());
            context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["a_uv"], 2, WebGlRenderingContext::FLOAT, false, 0, 0);
            context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["a_uv"]);

            shader.bind_uniforms();

            context.borrow_mut().bind_buffer(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,self.m_fullscreen_indices.as_ref());
            context.borrow_mut().draw_elements_with_i32(WebGlRenderingContext::TRIANGLES,FULLSCREEN_INDICES.len() as i32,WebGlRenderingContext::UNSIGNED_INT,0,);

            shader.use_program(false);
        }*/

        /*context.borrow_mut().viewport(0, 0, 512,512);    

        if let Some(shader) = self.m_shaders_map.get_mut(&String::from("depth"))
        {
            shader.set_uniform(String::from("shadow_depth_map"),ShaderUniform::UniformTexture2D(self.m_shadow_depth_tex.clone()));

            shader.set_texture_active();

            shader.use_program(true);

            context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, self.m_fullscreen_uv.as_ref());
            context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["a_uv"], 2, WebGlRenderingContext::FLOAT, false, 0, 0);
            context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["a_uv"]);

            shader.bind_uniforms();

            context.borrow_mut().bind_buffer(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,self.m_fullscreen_indices.as_ref());
            context.borrow_mut().draw_elements_with_i32(WebGlRenderingContext::TRIANGLES,FULLSCREEN_INDICES.len() as i32,WebGlRenderingContext::UNSIGNED_INT,0,);

            shader.use_program(false);
        }*/

        context.borrow_mut().clear(WebGlRenderingContext::DEPTH_BUFFER_BIT);

        context.borrow_mut().disable(WebGlRenderingContext::DEPTH_TEST);

        let mut flat_queue = self.m_flat_queue.borrow_mut();

        /*if let Some(shader) = self.m_shaders_map.get_mut(&String::from("flat"))
        {
            for (mov_matrix, flat_setting) in flat_queue.iter()
            {
                shader.use_program(true);
            
                context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, flat_setting.m_vertices_bo.as_ref());

                let (vertices_start,vertices_end) = calculate_loc_begin_end::<f32,Vec3>(&flat_setting.m_vertices);

                let vertices_array = {
                    let memory_buffer = wasm_bindgen::memory()
                        .dyn_into::<WebAssembly::Memory>().expect("Failed to dynamic cast WebAssembly::Memory for vertices array!")
                        .buffer();
                    Float32Array::new(&memory_buffer).subarray(vertices_start, vertices_end)
                }; 

                context.borrow_mut().buffer_data_with_array_buffer_view(WebGlRenderingContext::ARRAY_BUFFER,&vertices_array,WebGlRenderingContext::DYNAMIC_DRAW,);

                // Point an attribute to the currently bound VBO
                context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["a_pos"], 3, WebGlRenderingContext::FLOAT, false, 0, 0);
            
                // Enable the attribute
                context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["a_pos"]);

                context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, flat_setting.m_colors_bo.as_ref());

                let (colors_start,colors_end) = calculate_loc_begin_end::<f32,Vec3>(&flat_setting.m_colors);

                let colors_array = {
                    let memory_buffer = wasm_bindgen::memory()
                        .dyn_into::<WebAssembly::Memory>().expect("Failed to dynamic cast WebAssembly::Memory for vertices array!")
                        .buffer();
                    Float32Array::new(&memory_buffer).subarray(colors_start, colors_end)
                };

                context.borrow_mut().buffer_data_with_array_buffer_view(WebGlRenderingContext::ARRAY_BUFFER,&colors_array,WebGlRenderingContext::DYNAMIC_DRAW,);

                context.borrow_mut().vertex_attrib_pointer_with_i32(shader.m_attributes["a_col"], 3, WebGlRenderingContext::FLOAT, false, 0, 0);
            
                context.borrow_mut().enable_vertex_attrib_array(shader.m_attributes["a_col"]);

                shader.set_uniform(String::from("model_mat"),ShaderUniform::UniformMat4(mov_matrix.clone()));

                shader.set_uniform(String::from("view"),ShaderUniform::UniformMat4(self.m_view_mtx.borrow_mut().clone()));

                shader.set_uniform(String::from("proj"),ShaderUniform::UniformMat4(self.m_proj_mtx.borrow_mut().clone()));

                shader.bind_uniforms();

                // Bind appropriate array buffer to it
                context.borrow_mut().bind_buffer(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,flat_setting.m_index_bo.as_ref());

            
                let (index_start,index_end) = calculate_loc_begin_end::<u32,u32>(&flat_setting.m_index);

                let indices_array = {
                    let memory_buffer = wasm_bindgen::memory()
                        .dyn_into::<WebAssembly::Memory>().expect("Failed to dynamic cast WebAssembly::Memory for indices array!")
                        .buffer();
                    Uint32Array::new(&memory_buffer).subarray(index_start, index_end)
                };
                        
                // Pass the vertex data to the buffer
                context.borrow_mut().buffer_data_with_array_buffer_view(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,&indices_array,WebGlRenderingContext::DYNAMIC_DRAW,);

                context.borrow_mut().draw_elements_with_i32(WebGlRenderingContext::LINES,flat_setting.m_index.len() as i32,WebGlRenderingContext::UNSIGNED_INT,0,);

                shader.use_program(false);
            }
        }*/

        flat_queue.clear();

        context.borrow_mut().disable(WebGlRenderingContext::BLEND);

        context.borrow_mut().disable(WebGlRenderingContext::DEPTH_TEST);

        let ui_program = self.m_ui_program.as_ref().unwrap();

        //let raw_input: egui::RawInput = my_integration.gather_input();
        self.m_egui_ctx.begin_frame((*self.m_raw_input.borrow()).clone());
        my_app_ui(&mut self.m_egui_ctx); // add panels, windows and widgets to `egui_ctx` here
        let (output, shapes) = self.m_egui_ctx.end_frame();
        let clipped_meshes = self.m_egui_ctx.tessellate(shapes); // create triangles to paint

        *self.m_ui_captured.borrow_mut() = match output.cursor_icon
        {
            egui::CursorIcon::Default => false,
            _ => true,
        };
        //my_integration.paint(clipped_meshes);
        upload_egui_texture(&context.borrow_mut(),&self.m_egui_texture,&mut self.m_egui_texture_version,&self.m_egui_ctx.texture());
        egui_clear(&context.borrow_mut(),&canvas,clear_color());

        paint_meshes(&context.borrow_mut(),ui_program,canvas,&self.m_ui_buffer,&self.m_egui_texture,clipped_meshes,self.m_egui_ctx.pixels_per_point());

        //my_integration.set_cursor_icon(output.cursor_icon);
        // Also see `egui::Output` for more

        (*self.m_raw_input.borrow_mut()) = reset_egui_raw_input(&canvas); 
    } 
}

fn clear_color() -> egui::Rgba {
    // NOTE: a bright gray makes the shadows of the windows look weird.
    // We use a bit of transparency so that if the user switches on the
    // `transparent()` option they get immediate results.
    egui::Color32::from_rgba_unmultiplied(12, 12, 12, 180).into()
}

fn egui_clear(context: &WebGlRenderingContext, canvas: &HtmlCanvasElement,clear_color: egui::Rgba) 
{
    context.disable(WebGlRenderingContext::SCISSOR_TEST);

    let width = canvas.width() as i32;
    let height = canvas.height() as i32;
    context.viewport(0, 0, width, height);

    /*let clear_color: egui::Color32 = clear_color.into();
    context.clear_color(
        clear_color[0] as f32 / 255.0,
        clear_color[1] as f32 / 255.0,
        clear_color[2] as f32 / 255.0,
        clear_color[3] as f32 / 255.0,
    );
    context.clear(WebGlRenderingContext::COLOR_BUFFER_BIT);*/
}

fn upload_egui_texture(context: &WebGlRenderingContext, egui_texture: &Option<WebGlTexture>, egui_texture_version: &mut Option<u64>,texture: &egui::Texture) 
{
    if *egui_texture_version == Some(texture.version) {
        return; // No change
    }

    let mut pixels: Vec<u8> = Vec::with_capacity(texture.pixels.len() * 4);
    for srgba in texture.srgba_pixels() {
        pixels.push(srgba.r());
        pixels.push(srgba.g());
        pixels.push(srgba.b());
        pixels.push(srgba.a());
    }

    context.active_texture(WebGlRenderingContext::TEXTURE0);
    context.bind_texture(WebGlRenderingContext::TEXTURE_2D, egui_texture.as_ref());

    // TODO: https://developer.mozilla.org/en-US/docs/Web/API/EXT_sRGB
    // https://www.khronos.org/registry/webgl/extensions/EXT_sRGB/
    let level = 0;
    let internal_format = WebGlRenderingContext::RGBA;
    let border = 0;
    let src_format = WebGlRenderingContext::RGBA;
    let src_type = WebGlRenderingContext::UNSIGNED_BYTE;
    context.tex_image_2d_with_i32_and_i32_and_i32_and_format_and_type_and_opt_u8_array(
        WebGlRenderingContext::TEXTURE_2D,
        level,
        internal_format as i32,
        texture.width as i32,
        texture.height as i32,
        border,
        src_format,
        src_type,
        Some(&pixels),
    )
    .unwrap();

    *egui_texture_version = Some(texture.version);
}

pub fn get_texture(egui_texture: &Option<WebGlTexture>,texture_id: egui::TextureId) -> Option<&WebGlTexture> 
{
    if texture_id == egui::TextureId::Egui
    {
        return egui_texture.as_ref();
    }

    None
}

fn paint_meshes(context: &WebGlRenderingContext, program: &WebGlProgram, canvas: &HtmlCanvasElement, ui_buffer: &Option<UIBuffer>,egui_texture: &Option<WebGlTexture>,clipped_meshes: Vec<egui::ClippedMesh>,pixels_per_point: f32,) -> Result<(), JsValue> {
    //self.upload_user_textures();

    context.enable(WebGlRenderingContext::SCISSOR_TEST);
    context.disable(WebGlRenderingContext::CULL_FACE); // egui is not strict about winding order.
    context.enable(WebGlRenderingContext::BLEND);
    context.blend_func(WebGlRenderingContext::ONE, WebGlRenderingContext::ONE_MINUS_SRC_ALPHA); // premultiplied alpha
    context.use_program(Some(program));
    context.active_texture(WebGlRenderingContext::TEXTURE0);

    let u_screen_size_loc = context
        .get_uniform_location(program, "u_screen_size")
        .unwrap();
    let screen_size_pixels = egui::Vec2::new(canvas.width() as f32, canvas.height() as f32);
    let screen_size_points = screen_size_pixels / pixels_per_point;
    context.uniform2f(
        Some(&u_screen_size_loc),
        screen_size_points.x,
        screen_size_points.y,
    );

    let u_sampler_loc = context.get_uniform_location(program, "u_sampler").unwrap();
    context.uniform1i(Some(&u_sampler_loc), 0);

    for egui::ClippedMesh(clip_rect, mesh) in clipped_meshes {
        if let Some(gl_texture) = get_texture(egui_texture,mesh.texture_id) {

            context.active_texture(WebGlRenderingContext::TEXTURE0);
            context.bind_texture(WebGlRenderingContext::TEXTURE_2D, Some(gl_texture));

            let clip_min_x = pixels_per_point * clip_rect.min.x;
            let clip_min_y = pixels_per_point * clip_rect.min.y;
            let clip_max_x = pixels_per_point * clip_rect.max.x;
            let clip_max_y = pixels_per_point * clip_rect.max.y;
            let clip_min_x = clip_min_x.clamp(0.0, screen_size_pixels.x);
            let clip_min_y = clip_min_y.clamp(0.0, screen_size_pixels.y);
            let clip_max_x = clip_max_x.clamp(clip_min_x, screen_size_pixels.x);
            let clip_max_y = clip_max_y.clamp(clip_min_y, screen_size_pixels.y);
            let clip_min_x = clip_min_x.round() as i32;
            let clip_min_y = clip_min_y.round() as i32;
            let clip_max_x = clip_max_x.round() as i32;
            let clip_max_y = clip_max_y.round() as i32;

            // scissor Y coordinate is from the bottom
            context.scissor(
                clip_min_x,
                canvas.height() as i32 - clip_max_y,
                clip_max_x - clip_min_x,
                clip_max_y - clip_min_y,
            );

            for mesh in mesh.split_to_u16() {
                paint_mesh(context,program,ui_buffer,&mesh);
            }
        } else {
            web_sys::console::log_1(&format!(
                "WebGL: Failed to find texture {:?}",
                mesh.texture_id
            ).into());
        }
    }
    Ok(())
}

fn paint_mesh(context: &WebGlRenderingContext, program: &WebGlProgram, ui_buffer: &Option<UIBuffer>, mesh: &egui::epaint::Mesh16) -> Result<(), JsValue> 
{
    debug_assert!(mesh.is_valid());

    let ui = ui_buffer.as_ref().unwrap();

    let mut positions: Vec<f32> = Vec::with_capacity(2 * mesh.vertices.len());
    let mut tex_coords: Vec<f32> = Vec::with_capacity(2 * mesh.vertices.len());
    let mut colors: Vec<u8> = Vec::with_capacity(4 * mesh.vertices.len());
        
    for v in &mesh.vertices 
    {
        positions.push(v.pos.x);
        positions.push(v.pos.y);
        tex_coords.push(v.uv.x);
        tex_coords.push(v.uv.y);
        colors.push(v.color[0]);
        colors.push(v.color[1]);
        colors.push(v.color[2]);
        colors.push(v.color[3]);
    }

    // --------------------------------------------------------------------

    let indices_memory_buffer = wasm_bindgen::memory()
        .dyn_into::<WebAssembly::Memory>()?
        .buffer();
    let indices_ptr = mesh.indices.as_ptr() as u32 / 2;
    let indices_array = js_sys::Int16Array::new(&indices_memory_buffer)
        .subarray(indices_ptr, indices_ptr + mesh.indices.len() as u32);

        context.bind_buffer(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER, ui.index_buffer.as_ref());
        context.buffer_data_with_array_buffer_view(
        WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,
        &indices_array,
        WebGlRenderingContext::STREAM_DRAW,
    );

    // --------------------------------------------------------------------

    let pos_memory_buffer = wasm_bindgen::memory()
        .dyn_into::<WebAssembly::Memory>()?
        .buffer();
    let pos_ptr = positions.as_ptr() as u32 / 4;
    let pos_array = js_sys::Float32Array::new(&pos_memory_buffer)
        .subarray(pos_ptr, pos_ptr + positions.len() as u32);

        context.bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, ui.pos_buffer.as_ref());
        context.buffer_data_with_array_buffer_view(WebGlRenderingContext::ARRAY_BUFFER, &pos_array, WebGlRenderingContext::STREAM_DRAW);

    let a_pos_loc = context.get_attrib_location(program, "a_pos");
    assert!(a_pos_loc >= 0);
    let a_pos_loc = a_pos_loc as u32;

    let normalize = false;
    let stride = 0;
    let offset = 0;
    context.vertex_attrib_pointer_with_i32(a_pos_loc, 2, WebGlRenderingContext::FLOAT, normalize, stride, offset);
    context.enable_vertex_attrib_array(a_pos_loc);

    // --------------------------------------------------------------------

    let tc_memory_buffer = wasm_bindgen::memory()
        .dyn_into::<WebAssembly::Memory>()?
        .buffer();
    let tc_ptr = tex_coords.as_ptr() as u32 / 4;
    let tc_array = js_sys::Float32Array::new(&tc_memory_buffer)
        .subarray(tc_ptr, tc_ptr + tex_coords.len() as u32);

        context.bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, ui.tc_buffer.as_ref());
        context.buffer_data_with_array_buffer_view(WebGlRenderingContext::ARRAY_BUFFER, &tc_array, WebGlRenderingContext::STREAM_DRAW);

    let a_tc_loc = context.get_attrib_location(program, "a_tc");
    assert!(a_tc_loc >= 0);
    let a_tc_loc = a_tc_loc as u32;

    let normalize = false;
    let stride = 0;
    let offset = 0;
    context.vertex_attrib_pointer_with_i32(a_tc_loc, 2, WebGlRenderingContext::FLOAT, normalize, stride, offset);
    context.enable_vertex_attrib_array(a_tc_loc);

    // --------------------------------------------------------------------

    let colors_memory_buffer = wasm_bindgen::memory()
        .dyn_into::<WebAssembly::Memory>()?
        .buffer();
    let colors_ptr = colors.as_ptr() as u32;
    let colors_array = js_sys::Uint8Array::new(&colors_memory_buffer)
        .subarray(colors_ptr, colors_ptr + colors.len() as u32);

        context.bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, ui.color_buffer.as_ref());
        context.buffer_data_with_array_buffer_view(WebGlRenderingContext::ARRAY_BUFFER, &colors_array, WebGlRenderingContext::STREAM_DRAW);

    let a_srgba_loc = context.get_attrib_location(program, "a_srgba");
    assert!(a_srgba_loc >= 0);
    let a_srgba_loc = a_srgba_loc as u32;

    let normalize = false;
    let stride = 0;
    let offset = 0;
    context.vertex_attrib_pointer_with_i32(
        a_srgba_loc,
        4,
        WebGlRenderingContext::UNSIGNED_BYTE,
        normalize,
        stride,
        offset,
    );
    context.enable_vertex_attrib_array(a_srgba_loc);

    // --------------------------------------------------------------------

    context.draw_elements_with_i32(
        WebGlRenderingContext::TRIANGLES,
        mesh.indices.len() as i32,
        WebGlRenderingContext::UNSIGNED_SHORT,
        0,
    );

    Ok(())
}

/// Called each time the UI needs repainting, which may be many times per second.
/// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
fn my_app_ui(ctx: &egui::CtxRef) 
{

    // Examples of how to create different panels and windows.
    // Pick whichever suits you.
    // Tip: a good default choice is to just keep the `CentralPanel`.
    // For inspiration and more examples, go to https://emilk.github.io/egui

    egui::Area::new("my_area")
    .current_pos(egui::pos2(0.0, 0.0))
    .interactable(true)
    .movable(true)
    .show(ctx, |ui| {
        let mut name = String::new();
        let mut age = 0;
        ui.heading("My egui Application");
        ui.horizontal(|ui| {
            ui.label("Your name: ");
            ui.text_edit_singleline(&mut name);
        });
        ui.add(egui::Slider::new(&mut age, 0..=120).text("age"));
        if ui.button("Click each year").clicked() {
            age += 1;
        }
        ui.label(format!("Hello '{}', age {}", name, age));
        //ui.colored_label(egui::Color32::WHITE,"100cm");
    });

    /*egui::Window::new("window").show(ctx, |ui| 
        {
        let mut name = String::new();
        let mut age = 0;
        ui.heading("My egui Application");
        ui.horizontal(|ui| {
            ui.label("Your name: ");
            ui.text_edit_singleline(&mut name);
        });
        ui.add(egui::Slider::new(&mut age, 0..=120).text("age"));
        if ui.button("Click each year").clicked() {
            age += 1;
        }
        ui.label(format!("Hello '{}', age {}", name, age));
    });*/
}

pub fn calculate_loc_begin_end<T, U>(buffer : &Vec<U>) -> (u32, u32)
{
    let type_size = std::mem::size_of::<T>();
    let start = (buffer.as_ptr() as u32) / (type_size as u32);
    let breakdown = std::mem::size_of::<U>() / type_size;
    let end: u32 = start + (breakdown * buffer.len()) as u32;
    (start,end)
}

pub fn create_vbo(context:&Rc<RefCell<WebGlRenderingContext>>,loc_begin: u32, loc_end: u32) -> WebGlBuffer
{
    let context = &context.borrow_mut();

    let vertices_array = {
        let memory_buffer = wasm_bindgen::memory()
            .dyn_into::<WebAssembly::Memory>().expect("Failed to dynamic cast WebAssembly::Memory for vertices array!")
            .buffer();
        Float32Array::new(&memory_buffer).subarray(loc_begin, loc_end)
    };

    // Create an empty buffer object to store the vertex buffer
    let vertex_buffer = context.create_buffer().expect("failed to create buffer");

    //Bind appropriate array buffer to it
    context.bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, Some(&vertex_buffer));

    // Pass the vertex data to the buffer
    context.buffer_data_with_array_buffer_view(
        WebGlRenderingContext::ARRAY_BUFFER,
        &vertices_array,
        WebGlRenderingContext::STATIC_DRAW,
    );

    // Unbind the buffer
    context.bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, None);

    vertex_buffer
}

pub fn create_ibo(context:&Rc<RefCell<WebGlRenderingContext>>,loc_begin: u32, loc_end: u32) -> WebGlBuffer
{
    let context = &context.borrow_mut();

    let indices_array = {
        let memory_buffer = wasm_bindgen::memory()
            .dyn_into::<WebAssembly::Memory>().expect("Failed to dynamic cast WebAssembly::Memory for indices array!")
            .buffer();
        Uint32Array::new(&memory_buffer).subarray(loc_begin, loc_end)
    };

    // Create an empty buffer object to store Index buffer
    let index_buffer = context.create_buffer().expect("failed to create buffer");

    // Bind appropriate array buffer to it
    context.bind_buffer(
        WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,
        Some(&index_buffer),
    );

    // Pass the vertex data to the buffer
    context.buffer_data_with_array_buffer_view(
        WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,
        &indices_array,
        WebGlRenderingContext::STATIC_DRAW,
    );

    // Unbind the buffer
    context.bind_buffer(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER, None);

    index_buffer
}

pub fn compile_shader(context: &WebGlRenderingContext,shader_type: u32,source: &str,) -> Result<WebGlShader, String> 
{
    let shader = context.create_shader(shader_type).expect("Unable to create shader object");

    context.shader_source(&shader, source);

    context.compile_shader(&shader);

    if context.get_shader_parameter(&shader, WebGlRenderingContext::COMPILE_STATUS).as_bool().unwrap_or(false)
    {
        Ok(shader)
    } 
    else 
    {
        Err(context.get_shader_info_log(&shader) .expect("Unknown error creating shader"))
    }
}

pub fn link_program(context: &WebGlRenderingContext,vert_shader: &WebGlShader,frag_shader: &WebGlShader,) -> Result<WebGlProgram, String> 
{
    let program = context.create_program().expect("Unable to create shader object");

    context.attach_shader(&program, vert_shader);

    context.attach_shader(&program, frag_shader);

    context.link_program(&program);

    if context.get_program_parameter(&program, WebGlRenderingContext::LINK_STATUS).as_bool().unwrap_or(false)
    {
        Ok(program)
    }
    else 
    {
        Err(context.get_program_info_log(&program).expect("Unknown error creating program object"))
    }
}
