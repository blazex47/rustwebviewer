// Standard Dependencies
use std::rc::Rc;
use std::any::Any;
use std::boxed::Box;
use std::io::{Cursor};
use std::cmp::Ordering;
use std::cell::RefCell;
use std::collections::HashMap;
// 3rd Party Dependencies
use uuid::Uuid;
use image::io::{Reader};
use image::{ImageFormat};
use wasm_bindgen::JsCast;
use wasm_bindgen::prelude::*;
use egui::{Context,RawInput,Color32};
use glam::{Mat4, Vec2, Vec3, Vec4, Quat};
use js_sys::{Float32Array, Uint32Array, Uint16Array, WebAssembly};
use web_sys::{WebGlProgram, WebGlRenderingContext, WebGlShader,WebGlTexture, HtmlCanvasElement, WebGlUniformLocation, WebGlBuffer, WebGlFramebuffer};
// Engine Dependencies
use crate::engine::input::{MouseButton,MouseEvent};
use crate::engine::{Core, Chrono, UniqueIDRegistry};
use crate::engine::event::{Event, EventListener, EventHandler};
use crate::engine::resource::{Model,PosNormTangTexCol,AlphaFlag};
// Local Dependencies
use scene::{SceneEvent,Scene};
use material::{RenderMaterial};
use camera::{CameraEvent, Camera};
use light::{LightEvent, Light};
use shader::*;
use egui_helper::*;
use texture::*;

pub mod scene;
pub mod material;
pub mod camera;
pub mod light;
pub mod shader;
pub mod egui_helper;
pub mod texture;

macro_rules! hashmap {
    ($( $key: expr => $val: expr ),*) => {{
         let mut map = ::std::collections::HashMap::new();
         $( map.insert($key, $val); )*
         map
    }}
}

static CUBE_VERTICES: [[f32;3]; 36] = 
[
    // Pos X
    [ 1.0,  1.0,  1.0],
    [ 1.0,  1.0, -1.0],
    [ 1.0, -1.0,  1.0],
    [ 1.0, -1.0,  1.0],
    [ 1.0,  1.0, -1.0],
    [ 1.0, -1.0, -1.0],
    // Neg X
    [-1.0,  1.0, -1.0],
    [-1.0,  1.0,  1.0],
    [-1.0, -1.0, -1.0],
    [-1.0, -1.0, -1.0],
    [-1.0,  1.0,  1.0],
    [-1.0, -1.0,  1.0],
    // Pos Y
    [-1.0,  1.0, -1.0],
    [ 1.0,  1.0, -1.0],
    [-1.0,  1.0,  1.0],
    [-1.0,  1.0,  1.0],
    [ 1.0,  1.0, -1.0],
    [ 1.0,  1.0,  1.0],
    // Neg Y
    [-1.0, -1.0,  1.0],
    [ 1.0, -1.0,  1.0],
    [-1.0, -1.0, -1.0],
    [-1.0, -1.0, -1.0],
    [ 1.0, -1.0,  1.0],
    [ 1.0, -1.0, -1.0],
    // Pos Z
    [-1.0,  1.0,  1.0],
    [ 1.0,  1.0,  1.0],
    [-1.0, -1.0,  1.0],
    [-1.0, -1.0,  1.0],
    [ 1.0,  1.0,  1.0],
    [ 1.0, -1.0,  1.0],
    // Neg Z
    [ 1.0,  1.0, -1.0],
    [-1.0,  1.0, -1.0],
    [ 1.0, -1.0, -1.0],
    [ 1.0, -1.0, -1.0],
    [-1.0,  1.0, -1.0],
    [-1.0, -1.0, -1.0]
];

static CUBE_UVS: [[f32;2]; 36] = 
[
    // Bottom Left
    [-1.0, -1.0],
    [-1.0/3.0, -1.0],
    [-1.0, 0.0],
    [-1.0, 0.0],
    [-1.0/3.0, -1.0],
    [-1.0/3.0, 0.0],
    // Top Left
    [-1.0, 0.0],
    [-1.0/3.0, 0.0],
    [-1.0, 1.0],
    [-1.0, 1.0],
    [-1.0/3.0, 0.0],
    [-1.0/3.0, 1.0],
    // Bottom Middle
    [-1.0/3.0, -1.0],
    [1.0/3.0, -1.0],
    [-1.0/3.0, 0.0],
    [-1.0/3.0, 0.0],
    [1.0/3.0, -1.0],
    [1.0/3.0, 0.0],
    // Top Middle
    [-1.0/3.0, 0.0],
    [1.0/3.0, 0.0],
    [-1.0/3.0, 1.0],
    [-1.0/3.0, 1.0],
    [1.0/3.0, 0.0],
    [1.0/3.0, 1.0],
    // Bottom Right
    [1.0/3.0, -1.0],
    [1.0, -1.0],
    [1.0/3.0, 0.0],
    [1.0/3.0, 0.0],
    [1.0, -1.0],
    [1.0, 0.0],
    // Top Right
    [1.0/3.0, 0.0],
    [1.0, 0.0],
    [1.0/3.0, 1.0],
    [1.0/3.0, 1.0],
    [1.0, 0.0],
    [1.0, 1.0],
];

static CUBE_INDICES: [u32; 36] = 
[    
    0,  1,  2,  3,  4,  5,
    6,  7,  8,  9, 10, 11,
    12, 13, 14, 15, 16, 17,
    18, 19, 20, 21, 22, 23,
    24, 25, 26, 27, 28, 29,
    30, 31, 32, 33, 34, 35,
];

static FULLSCREEN_UVS: [[f32;2]; 6] = 
[
    [ 0.0,  0.0],
    [ 1.0,  0.0],
    [ 0.0,  1.0],
    [ 0.0,  1.0],
    [ 1.0,  0.0],
    [ 1.0,  1.0],
];

static FULLSCREEN_INDICES: [u32; 6] = 
[   
    0,  1,  2,  3,  4,  5,
];

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum RenderFlag
{
    Equirectangular = 1,
    Specular = 2,
    Irradiance = 4,
    BRDF = 8,
    Environment = 16,
    PreStencil = 32,
    Shadow = 64,
    PBR = 128,
    World = 256,
    Flat = 512,
    PBRAlpha = 1024,
    WorldAlpha = 2048,
    FlatAlpha = 4096,
    PostStencil = 8192,
    UI = 16384,
}

/*#[macro_use]
extern crate bitflags;

bitflags! {
    struct RenderFlag: u64 {
        const Equirectangular = 1;
        const Specular = 2;
        const Irradiance = 4;
        const BRDF = 8;
        const Environment = 16;
        const Shadow = 32;
        const PBR = 64;
        const World = 128;
        const Flat = 256;
        const PBRAlpha = 512;
        const WorldAlpha = 1024;
        const FlatAlpha = 2048;
        const UI = 4096;    
    }
}

impl RenderFlag 
{
    pub fn clear(&mut self) 
    {
        self.bits = 0;
    }
}*/

pub struct BufferObject
{
    pub m_vertex_bo: (Option<WebGlBuffer>,usize),
    pub m_index_bo: (Option<WebGlBuffer>,usize),
}

pub struct RenderEvent
{
    pub m_block_map: HashMap<String,Vec<RenderBlock>>,
}

impl Event for RenderEvent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }
}

#[derive(Clone)]
pub enum BufferObjInfo
{
    PBR(Model),
    Flat(String),
}

#[derive(Clone)]
pub struct RenderInfo
{
    pub m_material: RenderMaterial,
    pub m_buffer_obj: Option<Uuid>,
}

#[derive(Clone)]
pub struct BufferObjEvent
{
    pub m_bufferinfo: BufferObjInfo,
    pub m_renderinfo_map: HashMap<String,(Mat4,Vec<RenderInfo>)>,
}

impl Event for BufferObjEvent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }
}

impl BufferObjEvent
{
    pub fn new(bufferobj: &BufferObjInfo) -> Self
    {
        BufferObjEvent{ m_bufferinfo: bufferobj.clone()
                      , m_renderinfo_map : HashMap::new()}
    }
}

#[derive(Clone)]
pub struct RenderBlock
{
    pub m_material: RenderMaterial,
    pub m_buffer_obj: Option<Uuid>,
    pub m_model_mtx: Mat4,
    pub m_opacity: f32,
    pub m_render_flag: u64,
}

pub fn get_texture(texture_map: &Rc<RefCell<HashMap<Uuid,Texture>>>,uuid_opt: &Option<Uuid>) -> Option<WebGlTexture>
{
    if let Some(uuid) = uuid_opt
    {
        if let Some(texture) = texture_map.borrow().get(uuid)
        {
            return texture.m_texture.clone();
        }
    }

    None
}

impl RenderBlock
{
    pub fn get(&self, texture_map: &Rc<RefCell<HashMap<Uuid,Texture>>>, render_flag: &RenderFlag, uniform_name :&String) -> Option<ShaderUniform>
    {
        match render_flag
        {
            RenderFlag::Equirectangular =>
            {
                return match uniform_name.as_str()
                {
                    "equirectangular_texture" => Some(ShaderUniform::Texture2D(get_texture(texture_map,&self.m_material.m_albedo_uuid))),
                    _ => None,
                }
            },
            RenderFlag::Specular =>
            {
                return None;
            },
            RenderFlag::Irradiance =>
            {
                return None;
            },
            RenderFlag::BRDF =>
            {
                return None;
            },
            RenderFlag::Environment =>
            {
                return None;
            },
            RenderFlag::PreStencil =>
            {
                return match uniform_name.as_str()
                {
                    "model_mat" => Some(ShaderUniform::Mat4(self.m_model_mtx.clone())),
                    _ => None,
                }
            },
            RenderFlag::Shadow =>
            {
                return match uniform_name.as_str()
                {
                    "model_mat" => Some(ShaderUniform::Mat4(self.m_model_mtx.clone())),
                    _ => None,
                }
            },
            RenderFlag::PBR =>
            {
                return match uniform_name.as_str()
                {
                    "model_mat" => Some(ShaderUniform::Mat4(self.m_model_mtx.clone())),
                    "opacity" => Some(ShaderUniform::Float(self.m_opacity)),
                    "albedo_factor" => Some(ShaderUniform::Vec4(self.m_material.m_albedo_factor.clone())),
                    "metallic_factor" => Some(ShaderUniform::Float(self.m_material.m_metallic_factor.clone())),
                    "roughness_factor" => Some(ShaderUniform::Float(self.m_material.m_roughness_factor.clone())),
                    "emissive_factor" => Some(ShaderUniform::Vec3(self.m_material.m_emissive_factor.clone())),
                    "albedo_map" => Some(ShaderUniform::Texture2D(get_texture(texture_map,&self.m_material.m_albedo_uuid))),
                    "normal_map" => Some(ShaderUniform::Texture2D(get_texture(texture_map,&self.m_material.m_normal_uuid))),
                    "metallic_roughness_map" => Some(ShaderUniform::Texture2D(get_texture(texture_map,&self.m_material.m_metallic_roughness_uuid))),
                    "ao_map" => Some(ShaderUniform::Texture2D(get_texture(texture_map,&self.m_material.m_ao_uuid))),
                    "emissive_map" => Some(ShaderUniform::Texture2D(get_texture(texture_map,&self.m_material.m_emissive_uuid))),
                    _ => None,
                }
            },
            RenderFlag::PBRAlpha => 
            {
                return match uniform_name.as_str()
                {
                    "model_mat" => Some(ShaderUniform::Mat4(self.m_model_mtx.clone())),
                    "opacity" => Some(ShaderUniform::Float(self.m_opacity)),
                    "albedo_factor" => Some(ShaderUniform::Vec4(self.m_material.m_albedo_factor.clone())),
                    "metallic_factor" => Some(ShaderUniform::Float(self.m_material.m_metallic_factor.clone())),
                    "roughness_factor" => Some(ShaderUniform::Float(self.m_material.m_roughness_factor.clone())),
                    "emissive_factor" => Some(ShaderUniform::Vec3(self.m_material.m_emissive_factor.clone())),
                    "albedo_map" => Some(ShaderUniform::Texture2D(get_texture(texture_map,&self.m_material.m_albedo_uuid))),
                    "normal_map" => Some(ShaderUniform::Texture2D(get_texture(texture_map,&self.m_material.m_normal_uuid))),
                    "metallic_roughness_map" => Some(ShaderUniform::Texture2D(get_texture(texture_map,&self.m_material.m_metallic_roughness_uuid))),
                    "ao_map" => Some(ShaderUniform::Texture2D(get_texture(texture_map,&self.m_material.m_ao_uuid))),
                    "emissive_map" => Some(ShaderUniform::Texture2D(get_texture(texture_map,&self.m_material.m_emissive_uuid))),
                    _ => None,
                }
            },
            RenderFlag::World =>
            {
                return match uniform_name.as_str()
                {
                    "model_mat" => Some(ShaderUniform::Mat4(self.m_model_mtx.clone())),
                    "albedo_map" => Some(ShaderUniform::Texture2D(get_texture(texture_map,&self.m_material.m_albedo_uuid))),
                    _ => None,
                }
            },
            RenderFlag::WorldAlpha =>
            {
                return match uniform_name.as_str()
                {
                    "model_mat" => Some(ShaderUniform::Mat4(self.m_model_mtx.clone())),
                    "albedo_map" => Some(ShaderUniform::Texture2D(get_texture(texture_map,&self.m_material.m_albedo_uuid))),
                    _ => None,
                }
            },
            RenderFlag::Flat =>
            {
                return match uniform_name.as_str()
                {
                    "model_mat" => Some(ShaderUniform::Mat4(self.m_model_mtx.clone())),
                    _ => None,
                }
            },
            RenderFlag::FlatAlpha =>
            {
                return match uniform_name.as_str()
                {
                    "model_mat" => Some(ShaderUniform::Mat4(self.m_model_mtx.clone())),
                    _ => None,
                }
            },
            RenderFlag::PostStencil =>
            {
                return match uniform_name.as_str()
                {
                    "model_mat" => Some(ShaderUniform::Mat4(self.m_model_mtx.clone())),
                    _ => None,
                }
            },
            RenderFlag::UI =>
            {
                return None;
            }
        }
    }
}

#[derive(Clone)]
pub enum ColorBuffer
{
    None,
    Screen,
    RenderPassColor(Vec4),
}

#[derive(Clone)]
pub enum StencilBuffer
{
    None,
    Always,
    NotEqual,
}

pub fn generate_output_tex(context: &WebGlRenderingContext, textures: &Rc<RefCell<HashMap<Uuid,Texture>>>, shader_output: &ShaderOutput) -> Option<Uuid>
{
    match shader_output
    {
        ShaderOutput::Texture2D(_) => 
        {
            let output_tex = Texture::new_empty_2d(context,WebGlRenderingContext::RGBA);

            let uuid = Uuid::new_v4();

            textures.borrow_mut().insert(uuid.clone(),output_tex);

            Some(uuid.clone())
        },
        ShaderOutput::TextureCubeMap =>
        {
            let output_tex = Texture::new_empty_cubemap(context,WebGlRenderingContext::RGBA);

            let uuid = Uuid::new_v4();

            textures.borrow_mut().insert(uuid.clone(),output_tex);

            Some(uuid.clone())
        },
        ShaderOutput::Screen =>
        {
            None
        }
    }
}

pub fn recursive_setup( context: &WebGlRenderingContext, textures: &Rc<RefCell<HashMap<Uuid, Texture>>>,pass_flag: &RenderFlag
                        ,render_pass: &RenderPass, pass_map: &HashMap<RenderFlag,RenderPass>,pass_order: &mut Vec<RenderFlag>)
{
    // Find myself
    for elem in pass_order.clone()
    {
        if elem == *pass_flag
        {
            // if found, skip myself
            return;
        }
    }

    // Check if got dependencies
    if render_pass.m_dependencies.is_empty() == false
    {
        for (depend_flag,_) in &render_pass.m_dependencies
        {
            if let Some(depend_pass) = pass_map.get(depend_flag)
            {
                recursive_setup(context,textures,depend_flag,depend_pass,pass_map,pass_order);
            }
        }
    }

    pass_order.push((*pass_flag).clone());
}

pub fn setup( context: &WebGlRenderingContext, textures: &Rc<RefCell<HashMap<Uuid, Texture>>>,
                pass_map: &mut HashMap<RenderFlag,RenderPass>,pass_order: &mut Vec<RenderFlag>)
{
    for (pass_flag,render_pass) in &pass_map.clone()
    {
        recursive_setup(context,textures,pass_flag,render_pass,pass_map,pass_order);
    }

    for (_,render_pass) in pass_map
    {
        render_pass.m_output_tex = generate_output_tex(context,textures,&render_pass.m_shader_output);
    }

    for order in pass_order
    {
        web_sys::console::log_2(&"order: ".into(),&(order.clone() as u32).into());
    }
}

#[derive(Clone)]
pub struct RenderPass
{
    pub m_dependencies: Vec<(RenderFlag, Vec<ShaderInput>)>,

    pub m_attribute_map: HashMap<String, ShaderAttribute>,
    pub m_uniform_map: HashMap<String, ShaderUniform>,

    pub m_clear_colorbuffer: ColorBuffer,

    pub m_shader_program: String,

    pub m_blending: bool,
    pub m_cullface: bool,
    pub m_depthmask: bool,
    pub m_depth_test: bool,
    pub m_stencil_test: bool,
    pub m_scissors_test: bool,

    pub m_clear_stencilbuffer: StencilBuffer,

    pub m_shader_output: ShaderOutput,
    pub m_output_tex: Option<Uuid>,
}

impl RenderPass
{
    pub fn new(dependencies: &Vec<(RenderFlag, Vec<ShaderInput>)>,shader_program: &String) -> Self
    {
        RenderPass
        {
            m_dependencies: dependencies.clone(),

            m_attribute_map: HashMap::new(),
            m_uniform_map: HashMap::new(),
        
            m_clear_colorbuffer: ColorBuffer::None,
        
            m_shader_program: shader_program.clone(),
        
            m_blending: false,
            m_cullface: true,
            m_depthmask: true,
            m_depth_test: true,
            m_stencil_test: false,
            m_scissors_test: false,
        
            m_clear_stencilbuffer: StencilBuffer::None,

            m_shader_output: ShaderOutput::Screen,
            m_output_tex: None,
        }
    }
}

pub fn generate_renderpass_map() -> HashMap<RenderFlag,RenderPass>
{
    let mut pass_map = HashMap::new();

    let mut renderpass = RenderPass::new(&Vec::new(),&String::from("equirectangular"));
    renderpass.m_attribute_map = hashmap![  String::from("cube_vert") => ShaderAttribute::Position,
                                            String::from("vert") => ShaderAttribute::TexCoord];
    renderpass.m_shader_output = ShaderOutput::TextureCubeMap;
    
    pass_map.insert(RenderFlag::Equirectangular,renderpass);

    let mut renderpass = RenderPass::new(&vec![(RenderFlag::Equirectangular,vec![ShaderInput::TextureCubeMap(String::from("env_texture"))])]
                                        ,&String::from("specular"));
    renderpass.m_attribute_map = hashmap![  String::from("cube_vert") => ShaderAttribute::Position,
                                            String::from("vert") => ShaderAttribute::TexCoord];
    renderpass.m_uniform_map = hashmap![ String::from("roughness") => ShaderUniform::Float(0.25)];
    renderpass.m_shader_output = ShaderOutput::TextureCubeMap;

    renderpass.m_clear_colorbuffer = ColorBuffer::Screen;

    pass_map.insert(RenderFlag::Specular,renderpass);

    let mut renderpass = RenderPass::new(&vec![ (RenderFlag::Equirectangular, Vec::new()),
                                                (RenderFlag::Specular,vec![ShaderInput::TextureCubeMap(String::from("env_texture"))])]
                                        ,&String::from("irradiance"));
    renderpass.m_attribute_map = hashmap![String::from("cube_vert") => ShaderAttribute::Position,
                                          String::from("vert") => ShaderAttribute::TexCoord];
    renderpass.m_shader_output = ShaderOutput::TextureCubeMap;

    renderpass.m_clear_colorbuffer = ColorBuffer::Screen;

    pass_map.insert(RenderFlag::Irradiance,renderpass);

    let mut renderpass = RenderPass::new(&Vec::new(),&String::from("brdf"));
    
    renderpass.m_attribute_map = hashmap![String::from("a_uv") => ShaderAttribute::TexCoord];
    
    renderpass.m_shader_output = ShaderOutput::Texture2D(Vec2::new(3.0,2.0));

    renderpass.m_clear_colorbuffer = ColorBuffer::Screen;

    pass_map.insert(RenderFlag::BRDF,renderpass);

    let mut renderpass = RenderPass::new(&Vec::new(),&String::from("prestencil"));
    
    renderpass.m_attribute_map = hashmap![String::from("a_pos") => ShaderAttribute::Position];

    renderpass.m_uniform_map = hashmap![ String::from("color") => ShaderUniform::Vec4(Vec4::new(1.0,1.0,1.0,1.0))];

    renderpass.m_stencil_test = true;

    renderpass.m_shader_output = ShaderOutput::Screen;

    renderpass.m_clear_colorbuffer = ColorBuffer::Screen;

    renderpass.m_clear_stencilbuffer = StencilBuffer::Always;

    pass_map.insert(RenderFlag::PreStencil,renderpass);

    let mut renderpass = RenderPass::new(&Vec::new(),&String::from("shadow"));

    renderpass.m_attribute_map = hashmap![String::from("a_pos") => ShaderAttribute::Position];

    renderpass.m_clear_colorbuffer = ColorBuffer::RenderPassColor(Vec4::ONE);

    renderpass.m_shader_output = ShaderOutput::Texture2D(Vec2::new(1.0,1.0));

    pass_map.insert(RenderFlag::Shadow,renderpass);

    let mut renderpass = RenderPass::new(&vec![ (RenderFlag::Specular,vec![ShaderInput::TextureCubeMap(String::from("cube_map"))]),
                                                (RenderFlag::Equirectangular, Vec::new()),                                            
                                                (RenderFlag::Irradiance, Vec::new()),
                                                (RenderFlag::Shadow, Vec::new()),
                                                (RenderFlag::PreStencil, Vec::new()),
                                                (RenderFlag::BRDF, Vec::new())]
                                        ,&String::from("environment"));
    renderpass.m_attribute_map = hashmap![String::from("a_pos") => ShaderAttribute::Position];
    
    renderpass.m_clear_colorbuffer = ColorBuffer::Screen;

    pass_map.insert(RenderFlag::Environment,renderpass);

    let mut renderpass = RenderPass::new(&vec![ (RenderFlag::Environment, Vec::new()),
                                                (RenderFlag::PreStencil, Vec::new()),
                                                (RenderFlag::Specular,vec![ShaderInput::TextureCubeMap(String::from("spec_cube_map"))]), 
                                                (RenderFlag::Irradiance,vec![ShaderInput::TextureCubeMap(String::from("irradiance_cube_map"))]),
                                                (RenderFlag::BRDF,vec![     ShaderInput::Texture2D(String::from("spec_brdf_map"))]),
                                                (RenderFlag::Shadow,vec![   ShaderInput::Texture2D(String::from("shadow_depth_map")),
                                                                            ShaderInput::TexDimension(String::from("texture_size"))]) ],&String::from("pbr"));
    renderpass.m_attribute_map = hashmap![  String::from("a_pos") => ShaderAttribute::Position,
                                            String::from("a_norm") => ShaderAttribute::Normal,
                                            String::from("a_tang") => ShaderAttribute::Tangent,
                                            String::from("a_uv") => ShaderAttribute::TexCoord];
    renderpass.m_cullface = false;

    pass_map.insert(RenderFlag::PBR,renderpass);

    let mut renderpass = RenderPass::new(&vec![ (RenderFlag::PBR, Vec::new())],&String::from("world"));

    renderpass.m_attribute_map = hashmap![  String::from("a_pos") => ShaderAttribute::Position,
                                            String::from("a_norm") => ShaderAttribute::Normal,
                                            String::from("a_uv") => ShaderAttribute::TexCoord];
    renderpass.m_cullface = false;

    pass_map.insert(RenderFlag::World,renderpass);

    let mut renderpass = RenderPass::new(&vec![ (RenderFlag::World, Vec::new())],& String::from("flat"));

    renderpass.m_attribute_map = hashmap![  String::from("a_pos") => ShaderAttribute::Position,
                                            String::from("a_col") => ShaderAttribute::Color];
    renderpass.m_cullface = false;

    pass_map.insert(RenderFlag::Flat,renderpass);

    let mut renderpass = RenderPass::new(&vec![ (RenderFlag::Specular,vec![ShaderInput::TextureCubeMap(String::from("spec_cube_map"))]), 
                                                (RenderFlag::Irradiance,vec![ShaderInput::TextureCubeMap(String::from("irradiance_cube_map"))]),
                                                (RenderFlag::BRDF,vec![     ShaderInput::Texture2D(String::from("spec_brdf_map"))]),
                                                (RenderFlag::Shadow,vec![   ShaderInput::Texture2D(String::from("shadow_depth_map")),
                                                                            ShaderInput::TexDimension(String::from("texture_size"))]),
                                                (RenderFlag::Flat, Vec::new())],
                                                &String::from("pbr"));
    renderpass.m_attribute_map = hashmap![  String::from("a_pos") => ShaderAttribute::Position,
                                            String::from("a_norm") => ShaderAttribute::Normal,
                                            String::from("a_tang") => ShaderAttribute::Tangent,
                                            String::from("a_uv") => ShaderAttribute::TexCoord];
    renderpass.m_depthmask = false;
    renderpass.m_blending = true;

    pass_map.insert(RenderFlag::PBRAlpha,renderpass);

    let mut renderpass = RenderPass::new(&vec![ (RenderFlag::PBRAlpha, Vec::new())],&String::from("world"));

    renderpass.m_attribute_map = hashmap![  String::from("a_pos") => ShaderAttribute::Position,
                                            String::from("a_norm") => ShaderAttribute::Normal,
                                            String::from("a_uv") => ShaderAttribute::TexCoord];
    renderpass.m_depthmask = false;
    renderpass.m_blending = true;

    pass_map.insert(RenderFlag::WorldAlpha,renderpass);

    let mut renderpass = RenderPass::new(&vec![ (RenderFlag::WorldAlpha, Vec::new()) ],&String::from("flat"));

    renderpass.m_attribute_map = hashmap![  String::from("a_pos") => ShaderAttribute::Position,
                                            String::from("a_col") => ShaderAttribute::Color];
    renderpass.m_depthmask = false;
    renderpass.m_blending = true;

    pass_map.insert(RenderFlag::FlatAlpha,renderpass);

    let mut renderpass = RenderPass::new(&vec![ (RenderFlag::FlatAlpha, Vec::new())],&String::from("poststencil"));
    
    renderpass.m_attribute_map = hashmap![  String::from("a_pos") => ShaderAttribute::Position,
                                            String::from("a_norm") => ShaderAttribute::Normal];

    renderpass.m_uniform_map = hashmap![ String::from("color") => ShaderUniform::Vec4(Vec4::new(1.0,0.0,0.0,1.0))];

    renderpass.m_stencil_test = true;
    renderpass.m_depth_test = false;

    renderpass.m_shader_output = ShaderOutput::Screen;

    renderpass.m_clear_stencilbuffer = StencilBuffer::NotEqual;

    pass_map.insert(RenderFlag::PostStencil,renderpass);

    let mut renderpass = RenderPass::new(&vec![ (RenderFlag::PostStencil, Vec::new())],&String::from("ui"));

    pass_map.insert(RenderFlag::UI,renderpass);

    pass_map
}

pub fn renderpass_pre_setup(core: &Option<Core>, offset_ratio :&Vec2, dimension_ratio :&Vec2, bg_color :&Vec4, renderpass: &mut RenderPass) -> (i32,i32,i32,i32)
{
    let context = &core.as_ref().unwrap().m_context;

    let canvas = &core.as_ref().unwrap().m_canvas.clone();

    let x_offset = (offset_ratio.x * canvas.width() as f32) as i32;
    let y_offset = (offset_ratio.y * canvas.height() as f32) as i32;
    let x_width = dimension_ratio.x * canvas.width() as f32;
    let y_height = dimension_ratio.y * canvas.height() as f32;

    let mut mul_2_val = 1.0;

    if x_width / y_height >= 16.0 / 9.0
    {
        while mul_2_val * 2.0 < y_height
        {
            mul_2_val *= 2.0;
        }
    }
    else
    {
        panic!("Doesn't heigth greater than width");
    }

    if renderpass.m_blending == true
    {
        context.borrow_mut().enable(WebGlRenderingContext::BLEND);
    }
    else
    {
        context.borrow_mut().disable(WebGlRenderingContext::BLEND);
    }

    if renderpass.m_cullface == true
    {
        context.borrow_mut().enable(WebGlRenderingContext::CULL_FACE);
    }
    else
    {
        context.borrow_mut().disable(WebGlRenderingContext::CULL_FACE);
    }

    if renderpass.m_depth_test == true
    {
        context.borrow_mut().enable(WebGlRenderingContext::DEPTH_TEST);
    }
    else
    {
        context.borrow_mut().disable(WebGlRenderingContext::DEPTH_TEST);
    }

    if renderpass.m_stencil_test == true
    {
        context.borrow_mut().enable(WebGlRenderingContext::STENCIL_TEST);
    }
    else
    {
        context.borrow_mut().disable(WebGlRenderingContext::STENCIL_TEST);
    }

    if renderpass.m_scissors_test == true
    {
        context.borrow_mut().enable(WebGlRenderingContext::SCISSOR_TEST);
    }
    else
    {
        context.borrow_mut().disable(WebGlRenderingContext::SCISSOR_TEST);
    }

    context.borrow_mut().depth_mask(renderpass.m_depthmask);

    let result = match renderpass.m_shader_output
    {
        ShaderOutput::Screen =>
        {
            context.borrow_mut().viewport(x_offset, y_offset, x_width as i32, y_height as i32);
            (x_offset, y_offset, x_width as i32, y_height as i32)
        },
        ShaderOutput::Texture2D(aspect) =>
        {
            context.borrow_mut().viewport(x_offset, y_offset,(mul_2_val * aspect.x / aspect.y) as i32, mul_2_val as i32);
            (x_offset, y_offset,(mul_2_val * aspect.x / aspect.y) as i32, mul_2_val as i32)
        },
        ShaderOutput::TextureCubeMap =>
        {
            context.borrow_mut().viewport(x_offset, y_offset,(mul_2_val * 3.0 / 2.0) as i32, mul_2_val as i32);
            (x_offset, y_offset,(mul_2_val * 3.0 / 2.0) as i32, mul_2_val as i32)
        },    
    };

    match renderpass.m_clear_colorbuffer
    {
        ColorBuffer::None =>
        {

        },
        ColorBuffer::RenderPassColor(color) =>
        {
            context.borrow_mut().clear_color(color.x,color.y,color.z,color.w);
            context.borrow_mut().clear(WebGlRenderingContext::COLOR_BUFFER_BIT | WebGlRenderingContext::DEPTH_BUFFER_BIT );
        },
        ColorBuffer::Screen =>
        {
            context.borrow_mut().clear_color(bg_color.x,bg_color.y,bg_color.z,bg_color.w);
            context.borrow_mut().clear(WebGlRenderingContext::COLOR_BUFFER_BIT | WebGlRenderingContext::DEPTH_BUFFER_BIT );
        },
    }

    match renderpass.m_clear_stencilbuffer
    {
        StencilBuffer::None =>
        {
            context.borrow_mut().stencil_mask(0x00); 
        },
        StencilBuffer::Always =>
        {
            context.borrow_mut().stencil_func(WebGlRenderingContext::ALWAYS, 1, 0xFF); 
            context.borrow_mut().stencil_mask(0xFF); 
        },
        StencilBuffer::NotEqual =>
        {
            context.borrow_mut().stencil_func(WebGlRenderingContext::NOTEQUAL, 1, 0xFF);
            context.borrow_mut().stencil_mask(0x00); 
        }
    }

    result
}

pub fn attach_texture(context: &WebGlRenderingContext, tex_count:&mut u32,shader_uniform: &ShaderUniform)
{
    match shader_uniform
    {
        ShaderUniform::Texture2D(tex2d_val) =>
        {
            context.active_texture(WebGlRenderingContext::TEXTURE0 + *tex_count);
            context.bind_texture(WebGlRenderingContext::TEXTURE_2D, tex2d_val.as_ref());
            *tex_count += 1;
        },
        ShaderUniform::TextureCubeMap(texcube_val) =>
        {
            context.active_texture(WebGlRenderingContext::TEXTURE0 + *tex_count);
            context.bind_texture(WebGlRenderingContext::TEXTURE_CUBE_MAP, texcube_val.as_ref());
            *tex_count += 1;       
        }, 
        _=>
        {

        }                
    }
}

pub fn bind_uniform(context: &WebGlRenderingContext, loc: &Option<WebGlUniformLocation>,tex_count:&mut u32,shader_uniform: &ShaderUniform)
{
    match shader_uniform
    {
        ShaderUniform::Int(int_val) =>
        {
            context.uniform1i(loc.as_ref(), int_val.clone());
        },
        ShaderUniform::Float(float_val) =>
        {
            context.uniform1f(loc.as_ref(), float_val.clone());
        },
        ShaderUniform::Vec2(vec2_val) =>
        {
            context.uniform2f(loc.as_ref(),vec2_val.x,vec2_val.y);
        },
        ShaderUniform::Vec3(vec3_val) =>
        {
            context.uniform3f(loc.as_ref(),vec3_val.x,vec3_val.y,vec3_val.z);
        },
        ShaderUniform::Vec4(vec4_val) =>
        {
            context.uniform4f(loc.as_ref(),vec4_val.x,vec4_val.y,vec4_val.z,vec4_val.w);
        },
        ShaderUniform::Mat4(mat4_val) =>
        {
            context.uniform_matrix4fv_with_f32_array(loc.as_ref(), false, mat4_val.as_ref());
        },
        ShaderUniform::Texture2D(_tex2d_val) =>
        {
            context.uniform1i(loc.as_ref(), *tex_count as i32);
            *tex_count += 1;
        },
        ShaderUniform::TextureCubeMap(_texcube_val) =>
        {
            context.uniform1i(loc.as_ref(), *tex_count as i32);
            *tex_count += 1;
        },                
    }
}

pub struct RenderManager
{
    m_core: Option<Core>,
    m_listener: EventListener,
    pub m_hdr_uuid: Rc<RefCell<Option<Uuid>>>,
    pub m_white_uuid: Rc<RefCell<Option<Uuid>>>,
    pub m_black_uuid: Rc<RefCell<Option<Uuid>>>,
    pub m_cube_bo: Rc<RefCell<Option<Uuid>>>,
    pub m_fullscreen_bo: Rc<RefCell<Option<Uuid>>>,
    pub m_textures : Rc<RefCell<HashMap<Uuid,Texture>>>,
    pub m_bufferobjs: Rc<RefCell<HashMap<Uuid,BufferObject>>>,
    pub m_program_map: HashMap<String,ShaderProgram>,
    pub m_scene_list: Rc<RefCell<Vec<(String,Scene)>>>,
    pub m_raw_input: Rc<RefCell<egui::RawInput>>,
}

impl RenderManager
{
    pub fn new() -> Self
    {
        RenderManager
        {   
            m_core: None,
            m_listener: EventListener::new(),
            m_hdr_uuid: Rc::new(RefCell::new(None)),
            m_white_uuid: Rc::new(RefCell::new(None)),
            m_black_uuid: Rc::new(RefCell::new(None)),
            m_cube_bo: Rc::new(RefCell::new(None)),
            m_fullscreen_bo: Rc::new(RefCell::new(None)),
            m_textures: Rc::new(RefCell::new(HashMap::new())),
            m_bufferobjs: Rc::new(RefCell::new(HashMap::new())),
            m_program_map: HashMap::new(),
            m_scene_list: Rc::new(RefCell::new(Vec::new())),
            m_raw_input: Rc::new(RefCell::new(egui::RawInput::default())),
        }
    }

    pub fn init(&mut self,core: &Option<Core>)
    {
        self.m_core = core.clone();

        self.m_listener.init(&self.m_core);

        let canvas = &self.m_core.as_ref().unwrap().m_canvas;

        let context = &self.m_core.as_ref().unwrap().m_context;

        let ext = context.borrow_mut().get_extension("WEBGL_depth_texture");

        context.borrow_mut().get_extension("OES_texture_float");
        context.borrow_mut().get_extension("OES_texture_float_linear");
        context.borrow_mut().get_extension("OES_element_index_uint");

        //resize_canvas_to_screen_size(&canvas,egui::Vec2::new(1024.0, 2048.0));

        (*self.m_raw_input.borrow_mut()) = reset_egui_raw_input(&canvas);

        ///////////////////////////// TEMPORARY BEGIN /////////////////////////////////////
        let size_of_vert = std::mem::size_of::<PosNormTangTexCol>();

        let mut cube_vertices: Vec<PosNormTangTexCol> = Vec::new();
        
        for i in 0..CUBE_VERTICES.len()
        {
            cube_vertices.push(PosNormTangTexCol{
                m_position: if CUBE_VERTICES.is_empty(){ Vec3::ZERO } else { Vec3::new(CUBE_VERTICES[i][0],CUBE_VERTICES[i][1],CUBE_VERTICES[i][2]) },
                m_normal: Vec3::ONE ,
                m_tangent: Vec4::ONE,
                m_tex_coord: if CUBE_UVS.is_empty(){ Vec2::ZERO } else { Vec2::new(CUBE_UVS[i][0],CUBE_UVS[i][1]) },
                m_color: Vec4::ZERO,
            });
        }

        let cube_vertices_start: u32 = cube_vertices.as_ptr() as u32 / 4;
        let cube_vertices_end: u32 = cube_vertices_start + (size_of_vert * cube_vertices.len()) as u32;
        
        let cube_indices_start: u32 = CUBE_INDICES.as_ptr() as u32 / 4;
        let cube_indices_end: u32 = cube_indices_start + CUBE_INDICES.len() as u32;

        let vbo = Some(create_vbo(&context,cube_vertices_start, cube_vertices_end));
        let ibo = Some(create_ibo(&context,cube_indices_start, cube_indices_end));

        let bo_uuid = Uuid::new_v4();

        (*self.m_cube_bo.borrow_mut()) = Some(bo_uuid.clone());

        self.m_bufferobjs.borrow_mut().insert(bo_uuid.clone(),BufferObject
        {
            m_vertex_bo: (vbo,cube_vertices.len()),
            m_index_bo: (ibo,CUBE_INDICES.len()),
        });

        let mut fullscreen_vertices: Vec<PosNormTangTexCol> = Vec::new();
        
        for i in 0..FULLSCREEN_UVS.len()
        {
            fullscreen_vertices.push(PosNormTangTexCol{
                m_position: Vec3::ZERO,
                m_normal: Vec3::ONE ,
                m_tangent: Vec4::ONE,
                m_tex_coord: if FULLSCREEN_UVS.is_empty(){ Vec2::ZERO } else { Vec2::new(FULLSCREEN_UVS[i][0],FULLSCREEN_UVS[i][1]) },
                m_color: Vec4::ZERO,
            });
        }

        let fullscreen_vertices_start: u32 = fullscreen_vertices.as_ptr() as u32 / 4;
        let fullscreen_vertices_end: u32 = fullscreen_vertices_start + (size_of_vert * fullscreen_vertices.len()) as u32;

        let fullscreen_indices_start: u32 = FULLSCREEN_INDICES.as_ptr() as u32 / 4;
        let fullscreen_indices_end: u32 = fullscreen_indices_start + FULLSCREEN_INDICES.len() as u32;

        let vbo = Some(create_vbo(&context,fullscreen_vertices_start, fullscreen_vertices_end));
        let ibo = Some(create_ibo(&context,fullscreen_indices_start, fullscreen_indices_end));

        let bo_uuid = Uuid::new_v4();

        (*self.m_fullscreen_bo.borrow_mut()) = Some(bo_uuid.clone());

        self.m_bufferobjs.borrow_mut().insert(bo_uuid.clone(),BufferObject
        {
            m_vertex_bo: (vbo,fullscreen_vertices.len()),
            m_index_bo: (ibo,FULLSCREEN_INDICES.len()),
        });

        let hdr_buf = (include_bytes!("./venice_sunrise.jpg")).to_vec();

        if let Some((width,height,format,pixels)) = decode_buffer(&hdr_buf)
        {
            //web_sys::console::log_2(&"decode_buffer: ".into(),&(pixels.len() as u32).into());

            let tex = Texture::new_with_format_2d(&*context.borrow_mut(),width as i32,height as i32,format,&pixels);

            let uuid = Uuid::new_v4();

            (*self.m_hdr_uuid.borrow_mut()) = Some(uuid);

            self.m_textures.borrow_mut().insert(uuid, tex.clone());
        }

        let tex = Texture::new_with_format_2d(&*context.borrow_mut(),1,1,WebGlRenderingContext::RGB,&vec![255,255,255]);

        let uuid = Uuid::new_v4();

        (*self.m_white_uuid.borrow_mut()) = Some(uuid); 

        self.m_textures.borrow_mut().insert(uuid, tex.clone());

        let tex = Texture::new_with_format_2d(&*context.borrow_mut(),1,1,WebGlRenderingContext::RGB,&vec![0,0,0]);

        let uuid = Uuid::new_v4();

        (*self.m_black_uuid.borrow_mut()) = Some(uuid);

        self.m_textures.borrow_mut().insert(uuid, tex.clone());

        ///////////////////////////// TEMPORARY END /////////////////////////////////////

        setup_shaderprogram(core,&mut self.m_program_map);

        let scene_list = self.m_scene_list.clone();

        let mut inner_context = context.clone();

        let texture_list = self.m_textures.clone();

        let hdr_uuid = self.m_hdr_uuid.clone();
        let cube_bo = self.m_cube_bo.clone();
        let fullscreen_bo = self.m_fullscreen_bo.clone();

        self.m_listener.register::<SceneEvent>
        (&String::from("Scene"),Box::new(move |handle:&mut EventHandler| ->Vec<EventHandler>
        {
            //web_sys::console::log_1(&"SceneEvent!".into());

            let scene_msg = handle.get_event::<SceneEvent>();

            let mut temp_list = scene_msg.m_scene_list.clone();

            for (_,scene) in &mut temp_list
            {
                scene.m_pass_map = generate_renderpass_map();

                setup(&inner_context.borrow(),&texture_list,&mut scene.m_pass_map,&mut scene.m_pass_order);

                let mut render_mat = RenderMaterial::new();

                render_mat.m_albedo_uuid = hdr_uuid.borrow_mut().clone(); //Equirectangular
    
                scene.m_render_queue.push(RenderBlock{
                    m_material: render_mat,
                    m_buffer_obj: cube_bo.borrow_mut().clone(),
                    m_model_mtx: Mat4::IDENTITY,
                    m_opacity: 1.0,
                    m_render_flag:  RenderFlag::Equirectangular as u64
                                |   RenderFlag::Specular as u64
                                |   RenderFlag::Irradiance as u64,
                });
    
                scene.m_render_queue.push(RenderBlock{
                    m_material: RenderMaterial::new(),
                    m_buffer_obj: fullscreen_bo.borrow_mut().clone(),
                    m_model_mtx: Mat4::IDENTITY,
                    m_opacity: 1.0,
                    m_render_flag:  RenderFlag::BRDF as u64,
                });
            }
            
            (*scene_list.borrow_mut()) = temp_list;

            Vec::new()
        }));

        let mut scene_list = self.m_scene_list.clone();

        self.m_listener.register::<CameraEvent>
        (&String::from("Camera"),Box::new(move |handle:&mut EventHandler| ->Vec<EventHandler>
        {
            //web_sys::console::log_1(&"CameraEvent!".into());

            let camera_msg = handle.get_event::<CameraEvent>();

            for (scene_id, scene) in &mut *scene_list.borrow_mut()
            {
                if let Some(camera) = camera_msg.m_camera_map.get(scene_id)
                {
                    scene.m_view_matrix = camera.get_view_matrix();
                    scene.m_proj_matrix = camera.get_proj_matrix();
                }
            }

            Vec::new()
        }));

        let mut scene_list = self.m_scene_list.clone();

        self.m_listener.register::<LightEvent>
        (&String::from("Light"),Box::new(move |handle:&mut EventHandler| ->Vec<EventHandler>
        {
            //web_sys::console::log_1(&"CameraEvent!".into());

            let light_msg = handle.get_event::<LightEvent>();

            for (scene_id, scene) in &mut *scene_list.borrow_mut()
            {
                if let Some(light) = light_msg.m_light_map.get(scene_id)
                {
                    scene.m_light = light.clone();
                }
            }

            Vec::new()
        }));

        scene_list = self.m_scene_list.clone();

        self.m_listener.register::<RenderEvent>
        (&String::from("Render"),Box::new(move |handle:&mut EventHandler| ->Vec<EventHandler>
        {
            let mut render_msg = handle.get_event::<RenderEvent>();

            for (scene_id, scene) in &mut *scene_list.borrow_mut()
            {
                if let Some(block_list) = render_msg.m_block_map.get_mut(scene_id)
                {
                    scene.m_render_queue.append(block_list);
                }
            }

            Vec::new()
        }));

        let raw_input = self.m_raw_input.clone();

        let temp_canvas = self.m_core.as_ref().unwrap().m_canvas.clone();

        self.m_listener.register::<MouseEvent>
        (&String::from("Input"),Box::new(move |handle:&mut EventHandler|->Vec<EventHandler>
        {            
            let mouse = handle.get_event::<MouseEvent>();

            match &*mouse
            {
                MouseEvent::MouseScroll(scroll) =>
                {
                    raw_input.borrow_mut().scroll_delta = egui::Vec2::new(scroll.x,scroll.y);
                },
                MouseEvent::MouseMove(pos) =>
                {
                    let x = pos.x * temp_canvas.width() as f32;
                    let y = pos.y * temp_canvas.height() as f32;

                    (*raw_input.borrow_mut()).events.push(egui::Event::PointerMoved(egui::Pos2::new(x,y)));
                },
                MouseEvent::MouseDown(btn,pos) =>
                {
                    let x = pos.x * temp_canvas.width() as f32;
                    let y = pos.y * temp_canvas.height() as f32;

                    let event_ptrbtn = egui::Event::PointerButton
                    {
                        pos: egui::Pos2::new(x,y),
                        button: match btn
                        {
                            MouseButton::Primary => egui::PointerButton::Primary,
                            MouseButton::Auxillary => egui::PointerButton::Middle,
                            MouseButton::Secondary => egui::PointerButton::Secondary
                        },
                        pressed: true,
                        modifiers: Default::default()
                    };
                    (*raw_input.borrow_mut()).events.push(event_ptrbtn);
                },
                MouseEvent::MouseUp(btn,pos) =>
                {
                    let x = pos.x * temp_canvas.width() as f32;
                    let y = pos.y * temp_canvas.height() as f32;

                    let event_ptrbtn = egui::Event::PointerButton
                    {
                        pos: egui::Pos2::new(x,y),
                        button: match btn
                        {
                            MouseButton::Primary => egui::PointerButton::Primary,
                            MouseButton::Auxillary => egui::PointerButton::Middle,
                            MouseButton::Secondary => egui::PointerButton::Secondary
                        },
                        pressed: false,
                        modifiers: Default::default()
                    };
                    (*raw_input.borrow_mut()).events.push(event_ptrbtn);
                },
                _ => { println!("Not supported MouseEvent!");}
            }

            /*if *ui_captured.borrow_mut()
            {
                return None;
            }*/

            Vec::new()
        }));

        let textures = self.m_textures.clone();
        let bufferobjs = self.m_bufferobjs.clone();

        inner_context = context.clone();

        let white_uuid = self.m_white_uuid.clone();
        let black_uuid = self.m_black_uuid.clone();

        self.m_listener.register::<BufferObjEvent>
        (&String::from("BufferObj0"),Box::new(move |handle:&mut EventHandler| ->Vec<EventHandler>
        {
            let mut buf_obj_msg = handle.get_event::<BufferObjEvent>();
 
            buf_obj_msg.m_renderinfo_map = match &buf_obj_msg.m_bufferinfo
            {
                BufferObjInfo::PBR(model) =>
                {
                    let mut renderinfo_map = HashMap::new();

                    let meshes = &model.m_meshes;

                    for i in 0..meshes.len()
                    {
                        let mesh = &meshes[i];

                        let mut bo_list = Vec::new();

                        for primitive in &mesh.m_primitives
                        {                    
                            //let (vertices_start, vertices_end) = calculate_loc_begin_end::<f32, PosNormTangTex>(&primitive.m_vertices);
                            let vertices_start: u32 = primitive.m_vertices.as_ptr() as u32 / 4;
                            let vertices_length: usize = primitive.m_vertices.len();
                            let vertices_breakdown = std::mem::size_of::<PosNormTangTexCol>() / 4;
                            let vertices_end: u32 = vertices_start + (vertices_breakdown * primitive.m_vertices.len()) as u32;

                            web_sys::console::log_2(&"std::mem::size_of::<PosNormTangTexCol>()!".into(),&(std::mem::size_of::<PosNormTangTexCol>() as u32).into());

                            //let (indices_start, indices_end) = calculate_loc_begin_end::<u32, u32>(&primitive.m_indices);
                            let indices_start: u32 = primitive.m_indices.as_ptr() as u32 / 4;
                            let indices_length: usize = primitive.m_indices.len();
                            let indices_end: u32 = indices_start + (indices_length as u32);
                            
                            let mut render_mat = RenderMaterial::new();

                            if let Some(mat_id) = primitive.m_material_id.0
                            {
                                let material = &model.m_materials[mat_id as usize];

                                if let Some(img_id) = material.m_albedo_image_id.0
                                {
                                    let image = &model.m_images[img_id as usize];

                                    web_sys::console::log_1(&"before albedo tex".into());

                                    let albedo_tex = Texture::new_2d(&*inner_context.borrow_mut(),image.m_width as i32,image.m_height as i32,&image.m_pixels);
  
                                    let uuid = Uuid::new_v4();

                                    render_mat.m_albedo_uuid = Some(uuid.clone());

                                    textures.borrow_mut().insert(uuid.clone(),albedo_tex);
                                }
                                else
                                {
                                    render_mat.m_albedo_uuid = (*white_uuid.borrow_mut()).clone();
                                }

                                if let Some(img_id) = material.m_normal_image_id.0
                                {
                                    let image = &model.m_images[img_id as usize];

                                    web_sys::console::log_1(&"before normal tex".into());

                                    let normal_tex = Texture::new_2d(&*inner_context.borrow_mut(),image.m_width as i32,image.m_height as i32,&image.m_pixels);

                                    let uuid = Uuid::new_v4();

                                    render_mat.m_normal_uuid = Some(uuid.clone());

                                    textures.borrow_mut().insert(uuid.clone(),normal_tex);
                                }
                                else
                                {
                                    render_mat.m_normal_uuid = (*black_uuid.borrow_mut()).clone();
                                }

                                if let Some(img_id) = material.m_metallic_roughness_image_id.0
                                {
                                    let image = &model.m_images[img_id as usize];

                                    web_sys::console::log_1(&"before metallic_roughness tex".into());

                                    let metallic_roughness_tex = Texture::new_2d(&*inner_context.borrow_mut(),image.m_width as i32,image.m_height as i32,&image.m_pixels);

                                    let uuid = Uuid::new_v4();

                                    render_mat.m_metallic_roughness_uuid = Some(uuid.clone());

                                    textures.borrow_mut().insert(uuid.clone(),metallic_roughness_tex);
                                }
                                else
                                {
                                    render_mat.m_metallic_roughness_uuid = (*white_uuid.borrow_mut()).clone();
                                }

                                if let Some(img_id) = material.m_ao_image_id.0
                                {
                                    let image = &model.m_images[img_id as usize];

                                    web_sys::console::log_1(&"before ao tex".into());

                                    let ao_tex = Texture::new_2d(&*inner_context.borrow_mut(),image.m_width as i32,image.m_height as i32,&image.m_pixels);

                                    let uuid = Uuid::new_v4();

                                    render_mat.m_ao_uuid = Some(uuid.clone());

                                    textures.borrow_mut().insert(uuid.clone(),ao_tex);
                                }
                                else
                                {
                                    render_mat.m_ao_uuid = (*white_uuid.borrow_mut()).clone();
                                }

                                if let Some(img_id) = material.m_emissive_image_id.0
                                {
                                    let image = &model.m_images[img_id as usize];

                                    web_sys::console::log_1(&"before emissive tex".into());

                                    let emissive_tex = Texture::new_2d(&*inner_context.borrow_mut(),image.m_width as i32,image.m_height as i32,&image.m_pixels);

                                    let uuid = Uuid::new_v4();

                                    render_mat.m_emissive_uuid = Some(uuid.clone());

                                    textures.borrow_mut().insert(uuid.clone(),emissive_tex);
                                }
                                else
                                {
                                    render_mat.m_emissive_uuid = (*white_uuid.borrow_mut()).clone();
                                }

                                render_mat.m_albedo_factor = material.m_albedo_factor.clone();

                                render_mat.m_metallic_factor = material.m_metallic_factor.clone();

                                render_mat.m_roughness_factor = material.m_roughness_factor.clone();

                                render_mat.m_emissive_factor = material.m_emissive_factor.clone();

                                render_mat.m_double_sided = material.m_double_sided;
                                
                                render_mat.m_alpha_factor = material.m_alpha_factor;

                                render_mat.m_alpha_flag = material.m_alpha_flag.clone();
                            }

                            let vbo = Some(create_vbo(&inner_context,vertices_start, vertices_end));
                            let ibo = Some(create_ibo(&inner_context,indices_start, indices_end));

                            let bo_uuid = Uuid::new_v4();

                            bufferobjs.borrow_mut().insert(bo_uuid.clone(),BufferObject
                            {
                                m_vertex_bo: (vbo,vertices_length),
                                m_index_bo: (ibo,indices_length),
                            });

                            bo_list.push(RenderInfo 
                            {
                                m_material: render_mat,
                                m_buffer_obj: Some(bo_uuid.clone())
                            });
                        }

                        if bo_list.is_empty() == false
                        {
                            renderinfo_map.insert(i.to_string(),(mesh.m_transform.clone(),bo_list));
                        }
                    }
                    renderinfo_map
                },
                BufferObjInfo::Flat(name) =>
                {   
                    let mut renderinfo_map = HashMap::new();

                    renderinfo_map
                }
            };

            vec![EventHandler::new(vec![String::from("BufferObj1")],buf_obj_msg.clone())]
        }));
    }

    pub fn update(&mut self, timer: &mut Chrono)
    {
        let context = &self.m_core.as_ref().unwrap().m_context;

        let canvas = &self.m_core.as_ref().unwrap().m_canvas.clone();

        //web_sys::console::log_2(&"update self.m_scene_list: ".into(),&(self.m_scene_list.borrow().len() as u32).into());

        for (scene_name, scene) in &mut *self.m_scene_list.borrow_mut()
        {
            /*scene.m_render_queue.push(RenderBlock{
                m_material: RenderMaterial::new(),
                m_buffer_obj: (*self.m_cube_bo.borrow_mut()).clone(),
                m_model_mtx: Mat4::IDENTITY,
                m_opacity: 1.0,
                m_render_flag:  RenderFlag::Environment as u64,
            });*/

            /*let mut render_mat = RenderMaterial::new();

            render_mat.m_albedo_uuid = self.m_hdr_uuid.borrow_mut().clone(); //Equirectangular

            scene.m_render_queue.push(RenderBlock{
                m_material: render_mat,
                m_buffer_obj: self.m_cube_bo.borrow_mut().clone(),
                m_model_mtx: Mat4::IDENTITY,
                m_opacity: 1.0,
                m_render_flag:  RenderFlag::Equirectangular as u64
                            |   RenderFlag::Specular as u64
                            |   RenderFlag::Irradiance as u64
                            |   RenderFlag::Environment as u64,
            });

            scene.m_render_queue.push(RenderBlock{
                m_material: RenderMaterial::new(),
                m_buffer_obj: self.m_fullscreen_bo.borrow_mut().clone(),
                m_model_mtx: Mat4::IDENTITY,
                m_opacity: 1.0,
                m_render_flag:  RenderFlag::BRDF as u64,
            });*/

            context.borrow_mut().enable(WebGlRenderingContext::BLEND);
            context.borrow_mut().enable(WebGlRenderingContext::CULL_FACE);
            context.borrow_mut().enable(WebGlRenderingContext::DEPTH_TEST);
            context.borrow_mut().disable(WebGlRenderingContext::SCISSOR_TEST);
            context.borrow_mut().disable(WebGlRenderingContext::STENCIL_TEST);

            context.borrow_mut().blend_func(WebGlRenderingContext::SRC_ALPHA, WebGlRenderingContext::ONE_MINUS_SRC_ALPHA);

            context.borrow_mut().depth_mask(true);

            let x_offset = (scene.m_offset_ratio.x * canvas.width() as f32) as i32;
            let y_offset = (scene.m_offset_ratio.y * canvas.height() as f32) as i32;
            let x_width = (scene.m_dimension_ratio.x * canvas.width() as f32) as i32;
            let y_height = (scene.m_dimension_ratio.y * canvas.height() as f32) as i32;

            context.borrow_mut().viewport(x_offset, y_offset, x_width, y_height);
    
            context.borrow_mut().clear_color(0.0,0.0,0.0,1.0);

            context.borrow_mut().clear_depth(1.0);

            context.borrow_mut().stencil_op(WebGlRenderingContext::KEEP, WebGlRenderingContext::KEEP, WebGlRenderingContext::REPLACE);

            context.borrow_mut().clear(WebGlRenderingContext::COLOR_BUFFER_BIT | WebGlRenderingContext::DEPTH_BUFFER_BIT | WebGlRenderingContext::STENCIL_BUFFER_BIT );

            //web_sys::console::log_2(&"update scene.m_pass_order: ".into(),&(scene.m_pass_order.len() as u32).into());

            let scene_offset_ratio = scene.m_offset_ratio.clone();
            let scene_dimension_ratio = scene.m_dimension_ratio.clone();
            let scene_bg_color = scene.m_bg_color.clone();

            for renderpass_id in &scene.m_pass_order
            {
                let mut vp_val: (i32,i32,i32,i32) = (0,0,0,0);

                if let Some(renderpass) = scene.m_pass_map.get_mut(renderpass_id)
                {
                    vp_val = renderpass_pre_setup(&self.m_core,&scene_offset_ratio,&scene_dimension_ratio,&scene_bg_color,renderpass);
                }

                let mut renderpass_dependencies = Vec::new();

                if let Some(renderpass) = scene.m_pass_map.get(renderpass_id)
                {  
                    renderpass_dependencies = renderpass.m_dependencies.clone();
                }

                let mut pass_uniform_map = HashMap::new();

                // Add dependency related uniform into uniform map
                for (inner_pass_id,dependencies) in &renderpass_dependencies
                {
                    if let Some(inner_pass) = &scene.m_pass_map.get(inner_pass_id)
                    {
                        if let Some(uuid) = &inner_pass.m_output_tex
                        {
                            if let Some(texture) = self.m_textures.borrow().get(uuid)
                            {
                                for dependency in dependencies
                                {
                                    match dependency
                                    {
                                        ShaderInput::Texture2D(key) =>
                                        {
                                            pass_uniform_map.insert(key.clone(), ShaderUniform::Texture2D(texture.m_texture.clone()));
                                        },
                                        ShaderInput::TextureCubeMap(key) =>
                                        {
                                            pass_uniform_map.insert(key.clone(), ShaderUniform::TextureCubeMap(texture.m_texture.clone()));
                                        },
                                        ShaderInput::TexDimension(key) =>
                                        {
                                            let width = texture.m_width as f32;
                                            let height = texture.m_height as f32;
                                            pass_uniform_map.insert(key.clone(), ShaderUniform::Vec2(Vec2::new(width,height)));
                                        },
                                    }
                                }
                            }
                        }
                    }
                }
                
                if let Some(renderpass) = scene.m_pass_map.get_mut(renderpass_id)
                {
                    renderpass.m_uniform_map.extend(pass_uniform_map);
                }

                if let Some(renderpass) = scene.m_pass_map.get(renderpass_id)
                {        
                    if let Some(program) = self.m_program_map.get_mut(&renderpass.m_shader_program)
                    {
                        let mut renderpass_drawn = false;

                        for block in &scene.m_render_queue
                        {
                            if (block.m_render_flag as u64 & *renderpass_id as u64) > 0
                            {
                                renderpass_drawn = true;

                                let mut tex_count = 0;

                                // Attach tecture to active them
                                for (name, _) in &program.m_uniforms
                                {
                                    if let Some(shader_uniform) = scene.get(renderpass_id,name)
                                    {
                                        attach_texture(&*context.borrow_mut(),&mut tex_count, &shader_uniform);
                                    }

                                    if let Some(shader_uniform) = renderpass.m_uniform_map.get(name)
                                    {
                                        attach_texture(&*context.borrow_mut(),&mut tex_count, &shader_uniform);
                                    }

                                    if let Some(shader_uniform) = block.get(&self.m_textures,renderpass_id,name)
                                    {
                                        attach_texture(&*context.borrow_mut(),&mut tex_count, &shader_uniform);
                                    }
                                }

                                program.use_program(true);

                                tex_count = 0;

                                for (name, location) in &program.m_uniforms
                                {
                                    //web_sys::console::log_2(&"attrib name: ".into(),&name.into());

                                    if let Some(shader_uniform) = scene.get(renderpass_id,name)
                                    {
                                        bind_uniform(&*context.borrow_mut(),location,&mut tex_count, &shader_uniform);
                                    }

                                    if let Some(shader_uniform) = renderpass.m_uniform_map.get(name)
                                    {
                                        bind_uniform(&*context.borrow_mut(),location,&mut tex_count, &shader_uniform);
                                    }

                                    if let Some(shader_uniform) = block.get(&self.m_textures,renderpass_id,name)
                                    {
                                        bind_uniform(&*context.borrow_mut(),location,&mut tex_count, &shader_uniform);
                                    }
                                }

                                if let Some(uuid) = &block.m_buffer_obj
                                {
                                    if let Some(bufobj) = self.m_bufferobjs.borrow().get(uuid)
                                    {
                                        let size_of_vert = std::mem::size_of::<PosNormTangTexCol>() as i32;

                                        // Bind vertex buffer object
                                        context.borrow_mut().bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, bufobj.m_vertex_bo.0.as_ref());
        
                                        for (name, location) in &program.m_attributes
                                        {
                                            if let Some(attrib) = renderpass.m_attribute_map.get(name)
                                            {
                                                let (attrib_size, attrib_offset) = match attrib
                                                {
                                                    ShaderAttribute::Position => (3, 0),
                                                    ShaderAttribute::Normal => (3, 12),
                                                    ShaderAttribute::Tangent => (4, 24),
                                                    ShaderAttribute::TexCoord => (2, 40),
                                                    ShaderAttribute::Color => (4, 48),
                                                };
        
                                                // Point an attribute to the currently bound VBO
                                                context.borrow_mut().vertex_attrib_pointer_with_i32(*location, attrib_size, WebGlRenderingContext::FLOAT, false, size_of_vert, attrib_offset);
                                            
                                                // Enable the attribute
                                                context.borrow_mut().enable_vertex_attrib_array(*location);
                                            }
                                        }

                                        // Bind appropriate array buffer to it
                                        context.borrow_mut().bind_buffer(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,bufobj.m_index_bo.0.as_ref());

                                        // Draw the triangle
                                        context.borrow_mut().draw_elements_with_i32(WebGlRenderingContext::TRIANGLES,bufobj.m_index_bo.1.clone() as i32,WebGlRenderingContext::UNSIGNED_INT,0,);
                                    }
                                }

                                program.use_program(false);
                            }
                        }
                    
                        if renderpass_drawn == true
                        {
                            if let Some(uuid) = renderpass.m_output_tex
                            {
                                if let Some(tex) = self.m_textures.borrow_mut().get_mut(&uuid)
                                {
                                    match renderpass.m_shader_output
                                    {
                                        ShaderOutput::Texture2D(_) => 
                                        {
                                            tex.copy_frame_to_tex_2d(&*context.borrow_mut(), vp_val.0, vp_val.1, vp_val.2, vp_val.3);
                                        },
                                        ShaderOutput::TextureCubeMap =>
                                        {
                                            tex.copy_frame_to_tex_cubemap(&*context.borrow_mut(), 3, 2, vp_val.0, vp_val.1, vp_val.2, vp_val.3);
                                        },
                                        ShaderOutput::Screen =>
                                        {
        
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            scene.m_render_queue.clear();
        }
    }

    pub fn free(&mut self)
    {
        self.m_listener.deregister::<CameraEvent>(&String::from("Camera"));

        self.m_listener.deregister::<RenderEvent>(&String::from("Render"));

        self.m_listener.deregister::<RenderEvent>(&String::from("Input"));

        self.m_listener.deregister::<BufferObjEvent>(&String::from("BufferObj0"));
    }
}

pub fn create_vbo(context:&Rc<RefCell<WebGlRenderingContext>>,loc_begin: u32, loc_end: u32) -> WebGlBuffer
{
    let context = &context.borrow_mut();

    let vertices_array = {
        let memory_buffer = wasm_bindgen::memory()
            .dyn_into::<WebAssembly::Memory>().expect("Failed to dynamic cast WebAssembly::Memory for vertices array!")
            .buffer();
        Float32Array::new(&memory_buffer).subarray(loc_begin, loc_end)
    };

    // Create an empty buffer object to store the vertex buffer
    let vertex_buffer = context.create_buffer().expect("failed to create buffer");

    //Bind appropriate array buffer to it
    context.bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, Some(&vertex_buffer));

    // Pass the vertex data to the buffer
    context.buffer_data_with_array_buffer_view(
        WebGlRenderingContext::ARRAY_BUFFER,
        &vertices_array,
        WebGlRenderingContext::STATIC_DRAW,
    );

    // Unbind the buffer
    context.bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, None);

    vertex_buffer
}

pub fn create_ibo(context:&Rc<RefCell<WebGlRenderingContext>>,loc_begin: u32, loc_end: u32) -> WebGlBuffer
{
    let context = &context.borrow_mut();

    let indices_array = {
        let memory_buffer = wasm_bindgen::memory()
            .dyn_into::<WebAssembly::Memory>().expect("Failed to dynamic cast WebAssembly::Memory for indices array!")
            .buffer();
        Uint32Array::new(&memory_buffer).subarray(loc_begin, loc_end)
    };

    // Create an empty buffer object to store Index buffer
    let index_buffer = context.create_buffer().expect("failed to create buffer");

    // Bind appropriate array buffer to it
    context.bind_buffer(
        WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,
        Some(&index_buffer),
    );

    // Pass the vertex data to the buffer
    context.buffer_data_with_array_buffer_view(
        WebGlRenderingContext::ELEMENT_ARRAY_BUFFER,
        &indices_array,
        WebGlRenderingContext::STATIC_DRAW,
    );

    // Unbind the buffer
    context.bind_buffer(WebGlRenderingContext::ELEMENT_ARRAY_BUFFER, None);

    index_buffer
}