// Standard Dependencies
use std::rc::Rc;
use std::any::Any;
use std::boxed::Box;
use std::io::{Cursor};
use std::cmp::Ordering;
use std::cell::RefCell;
use std::collections::HashMap;
// 3rd Party Dependencies
use web_sys::{WebGlProgram, WebGlRenderingContext, WebGlShader,WebGlTexture, HtmlCanvasElement, WebGlUniformLocation, WebGlBuffer, WebGlFramebuffer};

pub fn native_pixels_per_point() -> f32 {
    let pixels_per_point = web_sys::window().unwrap().device_pixel_ratio() as f32;
    if pixels_per_point > 0.0 && pixels_per_point.is_finite() {
        pixels_per_point
    } else {
        1.0
    }
}

pub fn canvas_size_in_points(canvas: &HtmlCanvasElement) -> egui::Vec2 
{
    let pixels_per_point = native_pixels_per_point();
    egui::vec2(
        canvas.width() as f32 / pixels_per_point,
        canvas.height() as f32 / pixels_per_point,
    )
}

pub fn screen_size_in_native_points() -> Option<egui::Vec2> {
    let window = web_sys::window()?;
    Some(egui::Vec2::new(
        window.inner_width().ok()?.as_f64()? as f32,
        window.inner_height().ok()?.as_f64()? as f32,
    ))
}

pub fn resize_canvas_to_screen_size(canvas: &HtmlCanvasElement, max_size_points: egui::Vec2) -> Option<()> 
{
    let screen_size_points = screen_size_in_native_points()?;
    let pixels_per_point = native_pixels_per_point();

    let max_size_pixels = pixels_per_point * max_size_points;

    let canvas_size_pixels = pixels_per_point * screen_size_points;
    let canvas_size_pixels = canvas_size_pixels.min(max_size_pixels);
    let canvas_size_points = canvas_size_pixels / pixels_per_point;

    // Make sure that the height and width are always even numbers.
    // otherwise, the page renders blurry on some platforms.
    // See https://github.com/emilk/egui/issues/103
    fn round_to_even(v: f32) -> f32 {
        (v / 2.0).round() * 2.0
    }

    /*canvas
        .style()
        .set_property(
            "width",
            &format!("{}px", round_to_even(canvas_size_points.x)),
        )
        .ok()?;
    canvas
        .style()
        .set_property(
            "height",
            &format!("{}px", round_to_even(canvas_size_points.y)),
        )
        .ok()?;*/
    canvas.set_width(round_to_even(canvas_size_pixels.x) as u32);
    canvas.set_height(round_to_even(canvas_size_pixels.y) as u32);

    Some(())
}

pub fn reset_egui_raw_input(canvas: &HtmlCanvasElement) -> egui::RawInput
{
    let mut raw_input = egui::RawInput::default();

    raw_input.pixels_per_point = Some(native_pixels_per_point());
    raw_input.predicted_dt = 0.016666;
    raw_input.screen_rect = Some(egui::Rect::from_min_size(Default::default(), canvas_size_in_points(&canvas)));

    return raw_input;
}