// Standard Dependencies
use std::rc::Rc;
use std::any::Any;
use std::boxed::Box;
use std::io::{Cursor};
use std::cmp::Ordering;
use std::cell::RefCell;
use std::collections::HashMap;
// 3rd Party Dependencies
use glam::{Mat4, Vec2, Vec3, Vec4, Quat};
// Engine Dependencies
use crate::engine::event::{Event, EventListener, EventHandler};

#[derive(Clone)]
pub struct SphericalCoord
{
    pub m_radius: f32,
    pub m_azimuth_rad: f32,
    pub m_elevation_rad: f32,
    pub m_target_radius: f32,
    pub m_target_azimuth_rad: f32,
    pub m_target_elevation_rad: f32,
}

impl SphericalCoord
{
    pub fn new() -> SphericalCoord
    {
        let azimuth_deg: f32 = -90.0;

        SphericalCoord{ m_radius: 1.0, 
                        m_azimuth_rad: azimuth_deg.to_radians(), 
                        m_elevation_rad: 0.0,
                        m_target_radius: 1.0, 
                        m_target_azimuth_rad: azimuth_deg.to_radians(),
                        m_target_elevation_rad: 0.0}
    }

    pub fn get_position(&self) -> Vec3
    {
        let y = self.m_radius * self.m_elevation_rad.sin();
        let base = self.m_radius * self.m_elevation_rad.cos();
        let z = -(base * self.m_azimuth_rad.sin());
        let x = base * self.m_azimuth_rad.cos();

        Vec3::new(x,y,z)
    }

    pub fn get_matrix(&self) -> Mat4
    {
        let translate = self.get_position();
        let axis_z = translate.normalize();
        let axis_x = Vec3::new(0.0,1.0,0.0).cross(axis_z);
        let axis_y = axis_z.cross(axis_x);

        Mat4::from_cols(axis_x.extend(0.0),axis_y.extend(0.0),axis_z.extend(0.0), translate.extend(1.0))
    }
    
    pub fn update(&mut self, _dt: f32)
    {
        self.m_radius = 0.5 * (self.m_radius + self.m_target_radius);
        self.m_azimuth_rad = 0.5 * (self.m_azimuth_rad + self.m_target_azimuth_rad );
        self.m_elevation_rad = 0.5 * (self.m_elevation_rad + self.m_target_elevation_rad);
    }
}

#[derive(Clone)]
pub struct CameraEvent
{
    pub m_camera_map: HashMap<String,Camera>,
}

impl Event for CameraEvent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }
}

#[derive(Clone)]
pub struct Camera
{
    pub m_sphericalcoord: SphericalCoord,
    pub m_lookat_pos: Vec3,
    pub m_aspectratio: f32,
    pub m_updated: bool,
    pub m_fov_y: f32,
    pub m_near: f32,
    pub m_far: f32,
}

impl Camera
{
    pub fn new() -> Self
    {
        Camera{
            m_sphericalcoord: SphericalCoord::new(),
            m_lookat_pos: Vec3::ZERO,
            m_aspectratio: 1.7778,
            m_updated: false,
            m_fov_y: 60.0,
            m_near: 0.1, 
            m_far: 10000.0,
        }
    }

    pub fn get_view_matrix(&self) -> Mat4
    {
        Mat4::look_at_rh(self.m_sphericalcoord.get_position(),self.m_lookat_pos,Vec3::new(0.0,1.0,0.0))
    }

    pub fn get_proj_matrix(&self) -> Mat4
    {
        Mat4::perspective_rh(self.m_fov_y.to_radians(),self.m_aspectratio,self.m_near, self.m_far)
    }
}