// Standard Dependencies
use std::rc::Rc;
use std::any::Any;
use std::boxed::Box;
use std::io::{Cursor};
use std::cmp::Ordering;
use std::cell::RefCell;
use std::collections::HashMap;
// 3rd Party Dependencies
use image::io::{Reader};
use web_sys::{WebGlProgram, WebGlRenderingContext, WebGlShader,WebGlTexture, HtmlCanvasElement, WebGlUniformLocation, WebGlBuffer, WebGlFramebuffer};

#[derive(Clone)]
pub struct Texture
{
    pub m_texture: Option<WebGlTexture>,
    pub m_format: u32,
    pub m_width: i32,
    pub m_height: i32,
}

impl Texture
{
    pub fn new_empty_2d(context: &WebGlRenderingContext, format: u32) -> Self
    {
        Texture{
            m_texture: context.create_texture(),
            m_format: format,
            m_width: 0,
            m_height: 0,
        }
    }

    pub fn copy_frame_to_tex_2d(&mut self,context: &WebGlRenderingContext, offset_x: i32, offset_y: i32, width: i32, height: i32)
    {
        self.m_width = width;
        self.m_height = height;

        context.active_texture(WebGlRenderingContext::TEXTURE0);
        context.bind_texture(WebGlRenderingContext::TEXTURE_2D,self.m_texture.as_ref());

        context.tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_S, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
        context.tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_T, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
        context.tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MIN_FILTER, WebGlRenderingContext::LINEAR as i32);
        context.tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MAG_FILTER, WebGlRenderingContext::LINEAR as i32);

        context.copy_tex_image_2d(WebGlRenderingContext::TEXTURE_2D,0,self.m_format, offset_x, offset_y, self.m_width, self.m_height, 0);
    
        context.bind_texture(WebGlRenderingContext::TEXTURE_2D,None);
    }

    pub fn new_empty_cubemap(context: &WebGlRenderingContext, format: u32) -> Self
    {
        Texture{
            m_texture: context.create_texture(),
            m_format: format,
            m_width: 0,
            m_height: 0,
        }
    }

    pub fn copy_frame_to_tex_cubemap(&mut self,context: &WebGlRenderingContext, columns: i32, rows: i32, offset_x: i32, offset_y: i32, width: i32, height: i32)
    {
        self.m_width = width;
        self.m_height = height;

        let half_sample : (i32,i32) = (self.m_width / columns, self.m_height / rows);

        context.active_texture(WebGlRenderingContext::TEXTURE0);
        context.bind_texture(WebGlRenderingContext::TEXTURE_CUBE_MAP,self.m_texture.as_ref());

        context.tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_WRAP_S, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
        context.tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_WRAP_T, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
        context.tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_MAG_FILTER, WebGlRenderingContext::LINEAR as i32);
        context.tex_parameteri(WebGlRenderingContext::TEXTURE_CUBE_MAP, WebGlRenderingContext::TEXTURE_MIN_FILTER, WebGlRenderingContext::LINEAR as i32);

        context.copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_POSITIVE_X,0,self.m_format, offset_x, offset_y, half_sample.0, half_sample.1,0);
        context.copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_NEGATIVE_X,0,self.m_format, offset_x, offset_y + half_sample.1,   half_sample.0, half_sample.1,0);
        context.copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_POSITIVE_Y,0,self.m_format, offset_x + half_sample.0, offset_y,   half_sample.0, half_sample.1,0);
        context.copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_NEGATIVE_Y,0,self.m_format, offset_x + half_sample.0, offset_y + half_sample.1, half_sample.0, half_sample.1,0);
        context.copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_POSITIVE_Z,0,self.m_format, offset_x + 2 * half_sample.0, offset_y,   half_sample.0, half_sample.1,0);
        context.copy_tex_image_2d(WebGlRenderingContext::TEXTURE_CUBE_MAP_NEGATIVE_Z,0,self.m_format, offset_x + 2 * half_sample.0, offset_y + half_sample.1, half_sample.0, half_sample.1,0);
    
        context.bind_texture(WebGlRenderingContext::TEXTURE_CUBE_MAP,None);
    }

    pub fn new_2d(context: &WebGlRenderingContext,width: i32, height: i32,pixels: &Vec<u8>)  -> Self
    {
        let count = (pixels.len() as u32) / (width * height) as u32;

        web_sys::console::log_2(&"Texture::new_2d: ".into(),&count.into());

        let format = match count
        {
            1 => WebGlRenderingContext::LUMINANCE,
            2 => WebGlRenderingContext::LUMINANCE_ALPHA,
            3 => WebGlRenderingContext::RGB,
            4 =>WebGlRenderingContext::RGBA,
            _=> WebGlRenderingContext::ALPHA
        };
        
        Texture::new_with_format_2d(context, width, height, format, pixels)
    }

    pub fn new_with_format_2d(context: &WebGlRenderingContext,width: i32, height: i32, format: u32, pixels: &Vec<u8>) -> Self
    {
        let tex = context.create_texture();

        context.active_texture(WebGlRenderingContext::TEXTURE0);
        context.bind_texture(WebGlRenderingContext::TEXTURE_2D, tex.as_ref());

        let level = 0;
        let internal_format = format;
        let border = 0;
        let src_format = format;
        let src_type = WebGlRenderingContext::UNSIGNED_BYTE;
        let alignment = 1;
        context.pixel_storei(WebGlRenderingContext::UNPACK_ALIGNMENT, alignment);
        context.tex_image_2d_with_i32_and_i32_and_i32_and_format_and_type_and_opt_u8_array(
            WebGlRenderingContext::TEXTURE_2D,
            level,
            internal_format as i32,
            width as i32,
            height as i32,
            border,
            src_format,
            src_type,
            Some(pixels),
        )
        .unwrap();
        context.tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_S, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
        context.tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_WRAP_T, WebGlRenderingContext::CLAMP_TO_EDGE as i32);
        context.tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MIN_FILTER, WebGlRenderingContext::LINEAR as i32);
        context.tex_parameteri(WebGlRenderingContext::TEXTURE_2D, WebGlRenderingContext::TEXTURE_MAG_FILTER, WebGlRenderingContext::LINEAR as i32);

        context.bind_texture(WebGlRenderingContext::TEXTURE_2D, None);

        Texture{
            m_texture: tex,
            m_format: format,
            m_width: width,
            m_height: height,
        }
    }
}


// so far only support jpeg and png, seems that hdr have issue and check Cargo.toml 
pub fn decode_buffer(buffer: &Vec<u8>) -> Option<(u32,u32,u32,Vec<u8>)>
{
    let reader = Reader::new(Cursor::new(buffer.clone())).with_guessed_format().expect("Cursor io never fails");

    match reader.decode().expect("Failed to decode!")
    {
        image::DynamicImage::ImageLuma8(gray_image) =>
        {
            Some((gray_image.width(),gray_image.height(),WebGlRenderingContext::LUMINANCE,gray_image.into_raw()))
        },
        image::DynamicImage::ImageLumaA8(gray_alpha_image) =>
        {
            Some((gray_alpha_image.width(),gray_alpha_image.height(),WebGlRenderingContext::LUMINANCE_ALPHA,gray_alpha_image.into_raw()))
        },
        image::DynamicImage::ImageRgb8(rgb_image) =>
        {
            Some((rgb_image.width(),rgb_image.height(),WebGlRenderingContext::RGB,rgb_image.into_raw()))
        },
        image::DynamicImage::ImageRgba8(rgba_image) =>
        {
            Some((rgba_image.width(),rgba_image.height(),WebGlRenderingContext::RGBA,rgba_image.into_raw()))        },
        image::DynamicImage::ImageBgr8(_) =>
        {
            panic!("Decode Buffer: ImageBgr8 is not supported");
            None
        },
        image::DynamicImage::ImageBgra8(_) =>
        {
            panic!("Decode Buffer: ImageBgra8 is not supported");
            None
        },
        image::DynamicImage::ImageLuma16(_) =>
        {
            panic!("Decode Buffer: ImageLuma16 is not supported");
            None
        },
        image::DynamicImage::ImageLumaA16(_) =>
        {
            panic!("Decode Buffer: ImageLumaA16 is not supported");
            None
        },
        image::DynamicImage::ImageRgb16(_) =>
        {
            panic!("Decode Buffer: ImageRgb16 is not supported");
            None
        },
        image::DynamicImage::ImageRgba16(_) =>
        {
            panic!("Decode Buffer: ImageRgba16 is not supported");
            None
        },
        _ =>
        {
            panic!("Decode Buffer: Format is not supported");
            None
        }
    }
}