// Standard Dependencies
use std::rc::Rc;
use std::any::Any;
use std::boxed::Box;
use std::io::{Cursor};
use std::cmp::Ordering;
use std::cell::RefCell;
use std::collections::HashMap;
// 3rd Party Dependencies
use glam::{Mat4, Vec2, Vec3, Vec4, Quat};
// Engine Dependencies
use crate::engine::event::{Event, EventListener, EventHandler};
// Render Dependencies
use crate::engine::render::*;
// Local Dependencies
use shader::{ShaderProgram,ShaderUniform,ShaderAttribute};

#[derive(Clone)]
pub struct LightEvent
{
    pub m_light_map: HashMap<String,Light>,
}

impl Event for LightEvent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }
}

#[derive(Clone)]
pub struct Light
{
    pub m_position: Vec3,
    pub m_color: Vec3,
    pub m_intensity: f32,
}

impl Light
{
    pub fn new(position: &Vec3) -> Self
    {
        Light{m_position: position.clone(), m_color: Vec3::ONE, m_intensity: 1.0}
    }
}