precision mediump float;

attribute vec3 a_pos;

uniform mat4 model_mat;

uniform mat4 proj;
uniform mat4 view;

void main(void) 
{
    gl_Position = proj * view * model_mat * vec4(a_pos, 1.0);
}