precision mediump float;

varying vec3 f_pos;

uniform sampler2D equirectangular_texture;

// Converts from [-Pi, Pi] on X to [-0.5, 0.5], and [-Pi/2, Pi/2] on Y to [-0.5, 0.5]
const vec2 normalize_spherical_coords = vec2(0.1591, 0.3183);
vec2 SampleSphericalMap(vec3 v)
{
    vec2 uv = vec2(atan(v.x, v.z), asin(-v.y));
    uv *= normalize_spherical_coords;
    uv += 0.5;
    return uv;
}

void main() 
{
    vec3 pos = f_pos;
    vec2 uv = SampleSphericalMap(normalize(pos));
    vec3 col = texture2D(equirectangular_texture, uv).rgb;
    gl_FragColor = vec4(col, 1.0);
}
