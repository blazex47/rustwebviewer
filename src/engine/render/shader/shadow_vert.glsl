precision mediump float;

attribute vec3 a_pos;

uniform mat4 model_mat;
uniform mat4 depth_mat;

varying vec4 f_light_space_pos;

void main(void) 
{
    f_light_space_pos = depth_mat * model_mat * vec4(a_pos, 1.0);

    gl_Position = f_light_space_pos;
}