precision mediump float;

varying vec3 f_pos;

//uniform mat4 proj;
//uniform mat4 view;
//uniform float roughness;

uniform samplerCube cube_map;

void main() {
    vec3 col = textureCube(cube_map, f_pos).rgb;
    gl_FragColor = vec4(col, 1.0);
}
