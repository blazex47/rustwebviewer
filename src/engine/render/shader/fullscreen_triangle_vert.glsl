precision mediump float;

attribute vec2 a_uv;

varying vec2 f_uv;

// This is a trick from Sascha Willems which uses just the gl_VertexIndex
// to calculate the position and uv coordinates for one full-scren "quad"
// which is actually just a triangle with two of the vertices positioned
// correctly off screen.
void main() 
{
	//outUV = vec2((gl_VertexIndex << 1) & 2, gl_VertexIndex & 2);
	f_uv = a_uv;
	gl_Position = vec4(a_uv * 2.0 + -1.0, 0.0, 1.0);
}