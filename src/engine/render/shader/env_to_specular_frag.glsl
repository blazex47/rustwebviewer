precision mediump float;

const int SAMPLE_COUNT = 1024;

varying vec3 f_pos;
//varying flat int face_index;

//layout(set = 0, binding = 1) uniform sampler env_sampler;
uniform samplerCube env_texture;

uniform float roughness;
//uniform float resolution;

const float PI = 3.14159265359;

// https://learnopengl.com/PBR/IBL/Specular-IBL
float VanDerCorput(int n, int base)
{
    float invBase = 1.0 / float(base);
    float denom   = 1.0;
    float result  = 0.0;

    for(int i = 0; i < 32; ++i)
    {
        if(n > 0)
        {
            denom   = mod(float(n), 2.0);
            result += denom * invBase;
            invBase = invBase / 2.0;
            n       = int(float(n) / 2.0);
        }
    }

    return result;
}

// https://learnopengl.com/PBR/IBL/Specular-IBL
vec2 HammersleyNoBitOps(int i, int N)
{
    return vec2(float(i)/float(N), VanDerCorput(i, 2));
}

// https://learnopengl.com/PBR/IBL/Specular-IBL
vec3 ImportanceSampleGGX(vec2 Xi, vec3 N, float roughness)
{
    float a = roughness*roughness*roughness;
	
    float phi = 2.0 * PI * Xi.x;
    float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (a*a - 1.0) * Xi.y));
    float sinTheta = sqrt(1.0 - cosTheta*cosTheta);
	
    // from spherical coordinates to cartesian coordinates
    vec3 H;
    H.x = cos(phi) * sinTheta;
    H.y = sin(phi) * sinTheta;
    H.z = cosTheta;
	
    // from tangent-space vector to world-space sample vector
    vec3 up        = abs(N.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
    vec3 tangent   = normalize(cross(up, N));
    vec3 bitangent = cross(N, tangent);
	
    vec3 sampleVec = tangent * H.x + bitangent * H.y + N * H.z;
    return normalize(sampleVec);
}  

// https://computergraphics.stackexchange.com/questions/7656/importance-sampling-microfacet-ggx
float PdfGGX(float NdotH, float HdotV, float roughness) {
    float a = roughness * roughness * roughness;
    float b = (a - 1.0) * NdotH * NdotH + 1.0;
    float D = a / (PI * b * b);
    return (D * NdotH / (4.0 * HdotV)) + 0.0001;
}

void main() {
    vec3 pos = f_pos;
    vec3 N = normalize(pos);
    vec3 R = N;
    vec3 V = R;

    float total_weight = 0.0;
    vec3 acc = vec3(0.0);

    for (int i = 0; i < SAMPLE_COUNT; i++) {
        vec2 Xi = HammersleyNoBitOps(i, SAMPLE_COUNT);
        vec3 H = ImportanceSampleGGX(Xi, N, roughness);
        vec3 L = normalize(2.0 * dot(V, H) * H - V);

        float NdotL = max(dot(N, L), 0.0);
        //float NdotH = max(dot(H, N), 0.0);
        //float HdotV = max(dot(H, V), 0.0);

        //float pdf = PdfGGX(NdotH, HdotV, roughness);
        //float saTexel = 4.0 * PI / (6.0 * resolution * resolution);
        //float saSample = 1.0 / (float(SAMPLE_COUNT) * pdf + 0.0001);
        //float lod = roughness == 0.0 ? 0.0 : 0.5 * log2(saSample / saTexel);

        acc += textureCube(env_texture, L).rgb * NdotL;
        total_weight += NdotL;
    }

    acc = acc / total_weight;

    gl_FragColor = vec4(acc, 1.0);
}
