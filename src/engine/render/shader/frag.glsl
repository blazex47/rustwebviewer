precision mediump float;

uniform sampler2D albedo_map;

varying vec4 f_world_pos;
varying vec4 f_norm;
varying vec2 f_uv;

void main() 
{
    vec3 albedo = texture2D(albedo_map,f_uv).rgb;

    vec3 viewDir = normalize(f_world_pos.xyz);

    float spec = max(dot(normalize(f_norm.xyz), viewDir), 0.0);

    gl_FragColor = vec4(albedo * spec, 1.0);
}