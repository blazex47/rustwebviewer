precision mediump float;

attribute vec3 a_pos;
attribute vec3 a_col;

varying vec3 f_col;

uniform mat4 model_mat;

uniform mat4 proj;
uniform mat4 view;

void main() 
{
    f_col = a_col;
    gl_Position = proj * view * model_mat * vec4(a_pos,1.0);
}