precision mediump float;

attribute vec3 a_pos;
attribute vec3 a_norm;
attribute vec4 a_tang;
attribute vec2 a_uv;

uniform mat4 model_mat;

uniform mat4 depth_mat;

//uniform mat4 transpose_inv_view_model;
uniform mat4 proj;
uniform mat4 view;

varying vec4 f_light_space_pos;
varying vec4 f_world_pos;
varying vec3 f_norm;
varying vec3 f_tang;
varying float f_tbn_handedness;
varying vec2 f_uv;

void main()
{
    f_uv = a_uv;
    f_norm = normalize((view * model_mat * vec4(a_norm, 0.0)).xyz);
    f_tang = normalize((view * model_mat * vec4(a_tang.xyz, 0.0)).xyz);
    f_tbn_handedness = a_tang.w;
    vec4 world_pos = model_mat * vec4(a_pos, 1.0);
    f_light_space_pos = depth_mat * world_pos;
    f_world_pos = view * world_pos;
    gl_Position = proj * f_world_pos;
}