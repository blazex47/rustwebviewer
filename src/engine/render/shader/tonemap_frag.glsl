precision mediump float;

varying vec2 f_uv;

//ayout(set = 0, binding = 0) uniform sampler tex_sampler;
uniform sampler2D hdr_tex;

uniform float exposure;

void main()
{             
    const float gamma = 1.0;
    vec3 hdrColor = texture2D(hdr_tex, f_uv).rgb;
  
    // exposure tone mapping
    vec3 mapped = vec3(1.0) - exp(-hdrColor * exposure);
    // gamma correction 
    mapped = pow(mapped, vec3(1.0 / gamma));
  
    gl_FragColor = vec4(mapped, 1.0);
}