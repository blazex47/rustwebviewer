attribute vec3 a_pos;

uniform mat4 proj;
uniform mat4 view;

varying vec3 f_pos;

void main() {
    f_pos = a_pos;
    gl_Position = proj * view * vec4(vec3(100.0) * f_pos, 1.0);
}