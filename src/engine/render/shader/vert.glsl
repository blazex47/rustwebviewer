attribute vec3 a_pos;
attribute vec3 a_norm;
attribute vec2 a_uv;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model_mat;

varying vec4 f_world_pos;
varying vec4 f_norm;
varying vec2 f_uv;

void main() 
{
    f_uv = a_uv;
    f_norm = view * model_mat * vec4(a_norm,0.0);
    f_world_pos = proj * view * model_mat * vec4(a_pos, 1.0);
    gl_Position = f_world_pos;
}