precision mediump float;

varying vec4 f_light_space_pos;
varying vec4 f_world_pos;
varying vec3 f_norm;
varying vec3 f_tang;
varying float f_tbn_handedness;
varying vec2 f_uv;

uniform float opacity;
uniform float alpha_val;

uniform vec3 light_pos;
uniform float light_intensity;
uniform vec3 light_color;

uniform vec2 texture_size;

uniform sampler2D shadow_depth_map;

uniform samplerCube spec_cube_map;
uniform samplerCube irradiance_cube_map;
uniform sampler2D spec_brdf_map;

uniform mat4 proj;
uniform mat4 view;

uniform sampler2D albedo_map;
uniform sampler2D normal_map;
uniform sampler2D metallic_roughness_map;
uniform sampler2D ao_map;
uniform sampler2D emissive_map;

uniform vec4 albedo_factor;
uniform float metallic_factor;
uniform float roughness_factor;
uniform vec3 emissive_factor;

const float MAX_SPEC_LOD = 4.0;

vec3 f_schlick(const vec3 f0, const float vh) {
	return f0 + (1.0 - f0) * exp2((-5.55473 * vh - 6.98316) * vh);
}

float v_smithschlick(const float nl, const float nv, const float a) {
	return 1.0 / ((nl * (1.0 - a) + a) * (nv * (1.0 - a) + a));
}

float d_ggx(const float nh, const float a) {
	float a2 = a * a;
	float denom = pow(nh * nh * (a2 - 1.0) + 1.0, 2.0);
	return a2 * (1.0 / 3.1415926535) / denom;
}

vec3 specularBRDF(const vec3 f0, const float roughness, const float nl, const float nh, const float nv, const float vh) {
	float a = roughness * roughness;
	return d_ggx(nh, a) * clamp(v_smithschlick(nl, nv, a), 0.0, 1.0) * f_schlick(f0, vh) / 4.0;
}

vec3 lambertDiffuseBRDF(const vec3 albedo, const float nl) {
	return albedo * max(0.0, nl);
}

vec3 saturate(vec3 v) {
    return clamp(v, vec3(0.0), vec3(1.0));
}

float saturate(float v) {
    return clamp(v, 0.0, 1.0);
}

float shadowCalculation(vec4 fragPosLightSpace, vec3 normal, vec3 lightDir, vec2 textureSize)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    projCoords = (projCoords * 0.5) + 0.5;
    float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005); 
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize;
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture2D(shadow_depth_map, projCoords.xy + vec2(x, y) * texelSize).r; 
            if(projCoords.z - bias > pcfDepth)
            {
                shadow += 1.0;
            }        
        }    
    }
    shadow /= 9.0;
    return shadow;
}

void main() 
{
    vec4 albedo_alpha = albedo_factor * texture2D(albedo_map,f_uv);
    vec3 albedo = albedo_alpha.rgb;
    vec3 normal = texture2D(normal_map,f_uv).rgb;
    vec2 metallic_roughness = texture2D(metallic_roughness_map, f_uv).bg;
    float metallic = metallic_factor * metallic_roughness.x;
    float roughness = roughness_factor * metallic_roughness.y;
    float ao = texture2D(ao_map, f_uv).r;
    vec3 emissive = texture2D(emissive_map, f_uv).rgb;

    vec3 V = normalize(- f_world_pos.xyz);

    vec3 N = normalize(f_norm);
    vec3 T = normalize(f_tang - N * dot(N, f_tang));
    vec3 B = normalize(cross(N, T)) * f_tbn_handedness;
    mat3 TBN = mat3(T, B, N);

    if (normal.x > 0.0 || normal.y > 0.0 || normal.z > 0.0)
    {
        normal = normal * 2.0 - 1.0;
    }
    else
    {
        normal = N;
    }

    N = normalize(TBN * normal);
    vec3 R = reflect(-V, N);

    float NdotV = abs(dot(N, V)) + 0.00001;

    vec3 f0 = mix(vec3(0.04), albedo, metallic);

    vec3 ambient_irradiance = textureCube(irradiance_cube_map, N).rgb;
    vec3 ambient_spec = textureCube(spec_cube_map, R).rgb;
    vec2 env_brdf = texture2D(spec_brdf_map, vec2(NdotV, roughness)).rg;

    vec3 ambient_spec_fres = f_schlick(f0, NdotV);

    vec3 ambient_diffuse_fac = vec3(1.0) - ambient_spec_fres;
    ambient_diffuse_fac *= 1.0 - metallic;

    vec3 ambient = (ambient_irradiance * albedo * ambient_diffuse_fac) + (ambient_spec * (ambient_spec_fres * env_brdf.x + env_brdf.y));

    float a = roughness * roughness;
    vec3 acc = vec3(0.0);

    vec3 L = vec4(view * vec4(light_pos,1.0)).xyz - f_world_pos.xyz;
    float d2 = dot(L, L);
    L = normalize(L);
    vec3 H = normalize(V + L);
    vec3 l_contrib = light_color * light_intensity / d2;

    float NdotL = saturate(dot(N, L));
    float NdotH = saturate(dot(N, H));
    float VdotH = saturate(dot(H, V));
    vec3 fresnel = f_schlick(f0, VdotH);
    vec3 k_D = vec3(1.0) - fresnel;
    k_D *= 1.0 - metallic;
    
    vec3 specular = d_ggx(NdotH, a) * clamp(v_smithschlick(NdotL, NdotV, a), 0.0, 1.0) * fresnel;
    specular /= max(4.0 * NdotV * NdotL, 0.001);

    vec3 diffuse = albedo / 3.1415926535 * k_D;

    acc += (diffuse + specular) * NdotL * l_contrib;

    float shadow = shadowCalculation(f_light_space_pos,N,L,texture_size);

    vec3 final = ambient * ao + (1.0 - shadow) * acc + emissive * emissive_factor;

    float alpha = opacity * max(dot(R, L), albedo_alpha.a);

    gl_FragColor = vec4(final, alpha);
}