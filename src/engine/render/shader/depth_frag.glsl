precision mediump float;

varying vec2 f_uv;

uniform sampler2D shadow_depth_map;

void main()
{             
    vec4 depthValue = texture2D(shadow_depth_map, f_uv);

    gl_FragColor = depthValue;
}