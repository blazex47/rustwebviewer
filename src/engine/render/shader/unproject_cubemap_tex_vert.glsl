attribute vec3 cube_vert;
attribute vec2 vert;

varying vec3 f_pos;

void main() 
{
    f_pos = cube_vert;

    gl_Position = vec4(vert, 0.0, 1.0);
}