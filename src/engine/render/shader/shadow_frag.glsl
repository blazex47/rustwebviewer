precision mediump float;

varying vec4 f_light_space_pos;

void main(void) 
{
    vec3 projCoords = f_light_space_pos.xyz / f_light_space_pos.w;
    
    projCoords = (projCoords * 0.5) + 0.5;

    gl_FragColor = vec4(vec3(projCoords.z),1.0);
}