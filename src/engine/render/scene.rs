// Standard Dependencies
use std::rc::Rc;
use std::any::Any;
use std::boxed::Box;
use std::io::{Cursor};
use std::cmp::Ordering;
use std::cell::RefCell;
use std::collections::HashMap;
// 3rd Party Dependencies
use glam::{Mat4, Vec2, Vec3, Vec4, Quat};
// Engine Dependencies
use crate::engine::event::{Event, EventListener, EventHandler};
// Render Dependencies
use crate::engine::render::*;
// Local Dependencies
use shader::{ShaderProgram,ShaderUniform,ShaderAttribute};
use light::{Light};

pub struct SceneEvent
{
    pub m_scene_list: Vec<(String,Scene)>,
}

impl Event for SceneEvent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }
}

#[derive(Clone)]
pub struct Scene
{
    pub m_bg_color: Vec4,

    pub m_light: Light,

    pub m_view_matrix: Mat4,
    pub m_proj_matrix: Mat4,

    pub m_offset_ratio: Vec2,
    pub m_dimension_ratio: Vec2,

    pub m_render_queue: Vec<RenderBlock>,
    pub m_pass_order: Vec<RenderFlag>,
    pub m_pass_map: HashMap<RenderFlag,RenderPass>,
}

impl Scene
{
    pub fn new(bg_color: &Vec4, offset_ratio: &Vec2, dimension_ratio: &Vec2) -> Self
    {
        Scene
        { 
            m_bg_color: bg_color.clone(),

            m_light: Light::new(&Vec3::ONE),

            m_view_matrix: Mat4::IDENTITY,
            m_proj_matrix: Mat4::IDENTITY,

            m_offset_ratio: offset_ratio.clone(),
            m_dimension_ratio: dimension_ratio.clone(),

            m_render_queue: Vec::new(),
            m_pass_order: Vec::new(),
            m_pass_map: HashMap::new(),
        }
    }

    pub fn get(&self, render_flag: &RenderFlag, uniform_name :&String) -> Option<ShaderUniform>
    {
        let light_fov_y: f32 = 60.0;

        let light_proj_mat = Mat4::perspective_rh(light_fov_y.to_radians(),1.0,0.5, 2.0);

        let light_view_mat = Mat4::look_at_rh(self.m_light.m_position,Vec3::new(0.0,0.0,0.0),Vec3::new(0.0,1.0,0.0));

        let depth_mat = light_proj_mat * light_view_mat;

        match render_flag
        {
            RenderFlag::Equirectangular =>
            {
                return None;
            },
            RenderFlag::Specular =>
            {
                return None;
            },
            RenderFlag::Irradiance =>
            {
                return None;
            },
            RenderFlag::BRDF =>
            {
                return None;
            },
            RenderFlag::PreStencil =>
            {
                return match uniform_name.as_str()
                {
                    "proj" => Some(ShaderUniform::Mat4(self.m_proj_matrix.clone())),
                    "view" => Some(ShaderUniform::Mat4(self.m_view_matrix.clone())),
                    _ => None,
                }
            },
            RenderFlag::Environment =>
            {
                return match uniform_name.as_str()
                {
                    "proj" => Some(ShaderUniform::Mat4(self.m_proj_matrix.clone())),
                    "view" => Some(ShaderUniform::Mat4(self.m_view_matrix.clone())),
                    _ => None,
                }
            },
            RenderFlag::Shadow =>
            {
                return match uniform_name.as_str()
                {
                    "depth_mat" => Some(ShaderUniform::Mat4(depth_mat.clone())),
                    _ => None,
                }
            },
            RenderFlag::PBR =>
            {
                return match uniform_name.as_str()
                {
                    "depth_mat" => Some(ShaderUniform::Mat4(depth_mat.clone())),
                    "proj" => Some(ShaderUniform::Mat4(self.m_proj_matrix.clone())),
                    "view" => Some(ShaderUniform::Mat4(self.m_view_matrix.clone())),
                    "light_pos" => Some(ShaderUniform::Vec3(self.m_light.m_position.clone())),
                    "light_color" => Some(ShaderUniform::Vec3(self.m_light.m_color.clone())),
                    "light_intensity" => Some(ShaderUniform::Float(self.m_light.m_intensity.clone())),
                    _ => None,
                }
            },
            RenderFlag::PBRAlpha => 
            {
                return match uniform_name.as_str()
                {
                    "depth_mat" => Some(ShaderUniform::Mat4(depth_mat.clone())),
                    "proj" => Some(ShaderUniform::Mat4(self.m_proj_matrix.clone())),
                    "view" => Some(ShaderUniform::Mat4(self.m_view_matrix.clone())),
                    "light_pos" => Some(ShaderUniform::Vec3(self.m_light.m_position.clone())),
                    "light_color" => Some(ShaderUniform::Vec3(self.m_light.m_color.clone())),
                    "light_intensity" => Some(ShaderUniform::Float(self.m_light.m_intensity.clone())),
                    _ => None,
                }
            },
            RenderFlag::World =>
            {
                return match uniform_name.as_str()
                {
                    "proj" => Some(ShaderUniform::Mat4(self.m_proj_matrix.clone())),
                    "view" => Some(ShaderUniform::Mat4(self.m_view_matrix.clone())),
                    _ => None,
                }
            },
            RenderFlag::WorldAlpha =>
            {
                return match uniform_name.as_str()
                {
                    "proj" => Some(ShaderUniform::Mat4(self.m_proj_matrix.clone())),
                    "view" => Some(ShaderUniform::Mat4(self.m_view_matrix.clone())),
                    _ => None,
                }
            },
            RenderFlag::Flat =>
            {
                return match uniform_name.as_str()
                {
                    "proj" => Some(ShaderUniform::Mat4(self.m_proj_matrix.clone())),
                    "view" => Some(ShaderUniform::Mat4(self.m_view_matrix.clone())),
                    _ => None,
                }
            },
            RenderFlag::FlatAlpha =>
            {
                return match uniform_name.as_str()
                {
                    "proj" => Some(ShaderUniform::Mat4(self.m_proj_matrix.clone())),
                    "view" => Some(ShaderUniform::Mat4(self.m_view_matrix.clone())),
                    _ => None,
                }
            },
            RenderFlag::PostStencil =>
            {
                return match uniform_name.as_str()
                {
                    "proj" => Some(ShaderUniform::Mat4(self.m_proj_matrix.clone())),
                    "view" => Some(ShaderUniform::Mat4(self.m_view_matrix.clone())),
                    _ => None,
                }
            },
            RenderFlag::UI =>
            {
                return None;
            }
        }
    }
}