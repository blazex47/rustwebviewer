// Standard Dependencies
use std::rc::Rc;
use std::any::Any;
use std::boxed::Box;
use std::io::{Cursor};
use std::cmp::Ordering;
use std::cell::RefCell;
use std::collections::HashMap;
// 3rd Party Dependencies
use glam::{Mat4, Vec2, Vec3, Vec4, Quat};
use web_sys::{WebGlProgram, WebGlRenderingContext, WebGlShader,WebGlTexture, HtmlCanvasElement, WebGlUniformLocation, WebGlBuffer, WebGlFramebuffer};
// Engine Dependencies
use crate::engine::{Core, Chrono, UniqueIDRegistry};

#[derive(Clone)]
pub enum ShaderAttribute
{
    Position,
    Normal,
    Tangent,
    TexCoord,
    Color,
}

#[derive(Clone)]
pub enum ShaderUniform
{
    Int(i32),
    Float(f32),
    Vec2(Vec2),
    Vec3(Vec3),
    Vec4(Vec4),
    Mat4(Mat4),
    Texture2D(Option<WebGlTexture>),
    TextureCubeMap(Option<WebGlTexture>),
}

#[derive(Clone)]
pub enum ShaderInput
{
    Texture2D(String),
    TextureCubeMap(String),
    TexDimension(String),
}

#[derive(Clone)]
pub enum ShaderOutput
{
    Screen,
    Texture2D(Vec2),
    TextureCubeMap,
}

#[derive(Clone)]
pub struct ShaderProgram
{
    m_context: Rc<RefCell<web_sys::WebGlRenderingContext>>,
    pub m_attributes: Vec<(String,u32)>,
    pub m_uniforms: Vec<(String,Option<WebGlUniformLocation>)>,
    pub m_program: web_sys::WebGlProgram,
}

impl ShaderProgram
{
    pub fn new(core: &Option<Core>,vertex_shader_source: &str, fragment_shader_source: &str) -> Self
    {
        let context = &core.as_ref().unwrap().m_context;
    
        let vert_shader = compile_shader(
            &context.borrow_mut(),
            WebGlRenderingContext::VERTEX_SHADER,
            vertex_shader_source,
        ).expect("Failed to compile vertex shader!");
        let frag_shader = compile_shader(
            &context.borrow_mut(),
            WebGlRenderingContext::FRAGMENT_SHADER,
            fragment_shader_source,
        ).expect("Failed to compile fragment shader!");
    
        let program = link_program(&context.borrow_mut(), &vert_shader, &frag_shader).expect("Failed to link shaders!");

        ShaderProgram{m_context: context.clone(), m_attributes: Vec::new(), m_uniforms: Vec::new(), m_program: program }
    }

    pub fn use_program(&mut self, use_prog: bool)
    {
        self.m_context.borrow_mut().use_program(match use_prog
        {
            true => Some(&self.m_program),
            false => None,
        });
    }

    pub fn extract_attributes(&mut self, name_list: &Vec<String>)
    {
        for name in name_list
        {
            let attribute = self.m_context.borrow_mut().get_attrib_location(&self.m_program, name.clone().as_str()) as u32;

            self.m_attributes.push((name.clone(),attribute));
        }
    }

    pub fn extract_uniforms(&mut self, name_list: &Vec<String>)
    {
        for name in name_list
        {
            let uniform = self.m_context.borrow_mut().get_uniform_location(&self.m_program, name.clone().as_str()).expect(&format!("cannot get {}",  name.clone().as_str())[..]).into();

            self.m_uniforms.push((name.clone(),uniform));
        }       
    }
}

pub fn setup_shaderprogram(core: &Option<Core>,program_map: &mut HashMap<String,ShaderProgram>)
{
    /*=========================Equirectangular Shaders========================*/

    let vertex_shader_source = include_str!("./shader/unproject_cubemap_tex_vert.glsl");
    let fragment_shader_source = include_str!("./shader/equirectangular_to_cube_faces_frag.glsl");

    let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);
    
    program.use_program(true);
    
    program.extract_attributes( &vec![   String::from("cube_vert"),
                                    String::from("vert")]);

    program.extract_uniforms(&vec![String::from("equirectangular_texture")]);

    program.use_program(false);

    program_map.insert(String::from("equirectangular"),program);

    /*=========================Specular Shaders========================*/

    let vertex_shader_source = include_str!("./shader/unproject_cubemap_tex_vert.glsl");
    let fragment_shader_source = include_str!("./shader/env_to_specular_frag.glsl");

    let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);
    
    program.use_program(true);
    
    program.extract_attributes(&vec![ String::from("cube_vert"),
                                 String::from("vert")]);
    
    program.extract_uniforms(&vec![  String::from("roughness"),
                                String::from("env_texture")]);

    program.use_program(false);

    program_map.insert(String::from("specular"),program);

    /*=========================Irradiance Shaders========================*/

    let vertex_shader_source = include_str!("./shader/unproject_cubemap_tex_vert.glsl");
    let fragment_shader_source = include_str!("./shader/env_to_irradiance_frag.glsl");

    let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);
    
    program.use_program(true);

    program.extract_attributes(&vec![ String::from("cube_vert"),
                                String::from("vert")]);
    
    program.extract_uniforms(&vec![String::from("env_texture")]);

    program.use_program(false);

    program_map.insert(String::from("irradiance"),program);
    
    /*=========================BRDF Shaders========================*/

    let vertex_shader_source = include_str!("./shader/fullscreen_triangle_vert.glsl");
    let fragment_shader_source = include_str!("./shader/integrate_spec_brdf_frag.glsl");
    
    let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);
    
    program.use_program(true);

    program.extract_attributes(&vec![String::from("a_uv")]);

    program.use_program(false);

    program_map.insert(String::from("brdf"),program);
    
    /*=========================Environment Shaders========================*/

    let vertex_shader_source = include_str!("./shader/environment_map_vert.glsl");
    let fragment_shader_source = include_str!("./shader/environment_map_frag.glsl");
    
    let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);
    
    program.use_program(true);
    
    program.extract_attributes(&vec![String::from("a_pos")]);
    
    program.extract_uniforms(  &vec![String::from("proj"),
                                String::from("view"),
                                String::from("cube_map")]);

    program.use_program(false);

    program_map.insert(String::from("environment"),program);

    /*=========================PreStencil Shaders========================*/

    let vertex_shader_source = include_str!("./shader/pre_stencil_vert.glsl");
    let fragment_shader_source = include_str!("./shader/stencil_frag.glsl");

    let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);
    
    program.use_program(true);
    
    program.extract_attributes(&vec![String::from("a_pos")]);
    
    program.extract_uniforms(&vec![ String::from("model_mat"),
                                    String::from("proj"),
                                    String::from("view"),
                                    String::from("color")]);

    program.use_program(false);

    program_map.insert(String::from("prestencil"),program);

    /*=========================PostStencil Shaders========================*/

    let vertex_shader_source = include_str!("./shader/post_stencil_vert.glsl");
    let fragment_shader_source = include_str!("./shader/stencil_frag.glsl");

    let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);
    
    program.use_program(true);
    
    program.extract_attributes(&vec![   String::from("a_pos"),
                                        String::from("a_norm")]);
    
    program.extract_uniforms(&vec![ String::from("model_mat"),
                                    String::from("proj"),
                                    String::from("view"),
                                    String::from("color")]);

    program.use_program(false);

    program_map.insert(String::from("poststencil"),program);

    /*=========================Shadow Shaders========================*/

    let vertex_shader_source = include_str!("./shader/shadow_vert.glsl");
    let fragment_shader_source = include_str!("./shader/shadow_frag.glsl");

    let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);
    
    program.use_program(true);
    
    program.extract_attributes(&vec![String::from("a_pos")]);
    
    program.extract_uniforms(&vec![  String::from("model_mat"),
                                String::from("depth_mat")]);

    program.use_program(false);

    program_map.insert(String::from("shadow"),program);

    /*=========================PBR Shaders========================*/

    let vertex_shader_source = include_str!("./shader/pbr_vert.glsl");
    let fragment_shader_source = include_str!("./shader/pbr_frag.glsl");
    
    let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);
    
    program.use_program(true);
    
    program.extract_attributes(&vec![   String::from("a_pos"),
                                        String::from("a_norm"),
                                        String::from("a_tang"),
                                        String::from("a_uv")]);
    
    program.extract_uniforms(&vec![ String::from("model_mat"),
                                    String::from("depth_mat"),
                                    String::from("proj"),
                                    String::from("view"),
                                    String::from("texture_size"),
                                    String::from("opacity"),
                                    //String::from("alpha_val"),
                                    String::from("light_pos"),
                                    String::from("light_intensity"),
                                    String::from("light_color"),
                                    String::from("shadow_depth_map"),
                                    String::from("spec_cube_map"),
                                    String::from("irradiance_cube_map"),
                                    String::from("spec_brdf_map"),
                                    String::from("albedo_map"),
                                    String::from("normal_map"),
                                    String::from("metallic_roughness_map"),
                                    String::from("ao_map"),
                                    String::from("emissive_map"),
                                    String::from("albedo_factor"),
                                    String::from("metallic_factor"),
                                    String::from("roughness_factor"),
                                    String::from("emissive_factor"),
                                    ]);

    program.use_program(false);

    program_map.insert(String::from("pbr"),program);
    
    /*=========================World Shaders========================*/

    let vertex_shader_source = include_str!("./shader/vert.glsl");
    let fragment_shader_source = include_str!("./shader/frag.glsl");
    
    let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);

    program.use_program(true);

    program.extract_attributes(&vec![   String::from("a_pos"),
                                        String::from("a_norm"),
                                        String::from("a_uv")]);

    program.extract_uniforms(&vec![ String::from("proj"),
                                    String::from("view"),
                                    String::from("model_mat"),
                                    String::from("albedo_map")]);

    program.use_program(false);

    program_map.insert(String::from("world"),program);

    /*=========================Flat Shaders========================*/

    let vertex_shader_source = include_str!("./shader/flat_vert.glsl");
    let fragment_shader_source = include_str!("./shader/flat_frag.glsl");

    let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);
    
    program.use_program(true);

    program.extract_attributes(&vec![String::from("a_pos"),
                                String::from("a_col")]);

    program.extract_uniforms(&vec![ String::from("model_mat"),
                                    String::from("proj"),
                                    String::from("view")]);

    program.use_program(false);

    program_map.insert(String::from("flat"),program);
    
    /*=========================UI Shaders========================*/

    let vertex_shader_source = include_str!("./shader/vertex_100es.glsl");
    let fragment_shader_source = include_str!("./shader/fragment_100es.glsl");

    let mut program = ShaderProgram::new(core,vertex_shader_source,fragment_shader_source);
    
    program.use_program(true);

    program.extract_attributes(&vec![   String::from("a_pos"),
                                        String::from("a_tc"),
                                        String::from("a_srgba")]);

    program.use_program(false);

    program_map.insert(String::from("ui"),program);
}

pub fn compile_link_shader(core: &Option<Core>,vertex_shader_source: &str, fragment_shader_source: &str) -> web_sys::WebGlProgram
{
    let context = &core.as_ref().unwrap().m_context;

    let vert_shader = compile_shader(
        &context.borrow_mut(),
        WebGlRenderingContext::VERTEX_SHADER,
        vertex_shader_source,
    ).expect("Failed to compile vertex shader!");
    let frag_shader = compile_shader(
        &context.borrow_mut(),
        WebGlRenderingContext::FRAGMENT_SHADER,
        fragment_shader_source,
    ).expect("Failed to compile fragment shader!");

    link_program(&context.borrow_mut(), &vert_shader, &frag_shader).expect("Failed to link shaders!")
}

pub fn compile_shader(context: &WebGlRenderingContext,shader_type: u32,source: &str,) -> Result<WebGlShader, String> 
{
    let shader = context.create_shader(shader_type).expect("Unable to create shader object");

    context.shader_source(&shader, source);

    context.compile_shader(&shader);

    if context.get_shader_parameter(&shader, WebGlRenderingContext::COMPILE_STATUS).as_bool().unwrap_or(false)
    {
        Ok(shader)
    } 
    else 
    {
        Err(context.get_shader_info_log(&shader) .expect("Unknown error creating shader"))
    }
}

pub fn link_program(context: &WebGlRenderingContext,vert_shader: &WebGlShader,frag_shader: &WebGlShader,) -> Result<WebGlProgram, String> 
{
    let program = context.create_program().expect("Unable to create shader object");

    context.attach_shader(&program, vert_shader);

    context.attach_shader(&program, frag_shader);

    context.link_program(&program);

    if context.get_program_parameter(&program, WebGlRenderingContext::LINK_STATUS).as_bool().unwrap_or(false)
    {
        Ok(program)
    }
    else 
    {
        Err(context.get_program_info_log(&program).expect("Unknown error creating program object"))
    }
}