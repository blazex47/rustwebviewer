use crate::engine::event::{EventListener, EventHandler, Event};
use std::cell::RefCell;
use std::rc::Rc;
use cgmath::*;
use kdtree_ray::{AABB, BoundingBox, KDtree};
use std::collections::HashMap;
use glam::{Mat4, Vec4, Vec3, Vec2, Quat};
use crate::engine::{Core,Chrono};
use crate::engine::world::{EntityComponenetSystem};
use serde::ser::{SerializeStruct};
use serde::{Serialize, Deserialize,Serializer,Deserializer};
use std::fmt::{self, Formatter};
use serde::de::{self, Visitor, MapAccess};
use crate::engine::input::{MouseEvent,MouseButton};
use crate::engine::resource::{ResourceLoader,Model,AlphaFlag};
use crate::engine::statemachine::{State,WorldEvent};
use crate::engine::render::{RenderEvent, RenderInfo ,RenderFlag,RenderBlock};
use crate::engine::component::{TransformComponent, MeshComponent, CollisionComponent, Collider, CollisionInfo};
use crate::engine::component::*;
use crate::engine::render::camera::*;
use crate::engine::render::scene::*;
use crate::engine::render::light::*;

pub struct TestState1
{
    m_core : Option<Core>,
    m_listener: EventListener,
    m_angle: Rc<RefCell<f32>>,
    m_camera: Rc<RefCell<Camera>>,
    m_prev_mouse_pos: Rc<RefCell<Vec2>>,
    //m_byte_buffer: Vec<u8>,
    //m_model_entity_list: Vec<String>,
    //m_model: Option<Model>,
    m_debug_info: Vec<RenderInfo>,
}

impl Serialize for TestState1
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut s = serializer.serialize_struct("TestState1", 1)?;
        s.serialize_field("m_angle",&*self.m_angle.borrow())?;
        s.end()
    }
}

impl<'de> Deserialize<'de> for TestState1 {
    fn deserialize<D>(deserializer: D) -> Result<TestState1, D::Error>
    where
        D: Deserializer<'de>,
    {
        web_sys::console::log_1(&"Deserialize TestState1 enter!".into());

        struct TestStateVisitor;

        impl<'de> Visitor<'de> for TestStateVisitor {
            type Value = TestState1;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result 
            {
                formatter.write_str("struct TestState1")
            }

            fn visit_map<V>(self, mut map: V) -> Result<Self::Value, V::Error>
            where
                V: MapAccess<'de>,
            {
                web_sys::console::log_1(&"visit_map enter!".into());
                let mut angle = None;
                while let Some(key) = map.next_key()? {
                    match key {
                        "m_angle" => {
                            web_sys::console::log_1(&"state added begin!".into());
                            if angle.is_some() {
                                return Err(de::Error::duplicate_field("m_angle"));
                            }
                            web_sys::console::log_1(&"state added end!".into());
                            angle = Some(map.next_value::<f32>()?);
                            web_sys::console::log_1(&"state added done!".into());
                        }
                        _=>
                        {
                            panic!("Component key do not work!")
                        }
                    }
                }
                let angle = angle.ok_or_else(|| de::Error::missing_field("m_angle"))?;
                let mut state = TestState1::new();
                state.m_angle = Rc::new(RefCell::new(angle));
                Ok(state)
            }
        }
        const FIELDS: &'static [&'static str] = &["m_angle"];
        deserializer.deserialize_struct("TestState1", FIELDS, TestStateVisitor)
    }
}

impl State for TestState1
{
    fn new() -> Self
    {
        let listener = EventListener::new();

        TestState1{ m_core : None, 
                    m_listener : listener, 
                    m_angle: Rc::new(RefCell::new(0.0)), 
                    m_camera: Rc::new(RefCell::new(Camera::new())),
                    m_prev_mouse_pos: Rc::new(RefCell::new(Vec2::new(0.0,0.0))),
                    m_debug_info: Vec::new()}
    }

    fn on_init(&mut self, ecs: &mut EntityComponenetSystem)
    {
        let canvas = &ecs.m_core.as_ref().unwrap().m_canvas;

        self.m_camera.borrow_mut().m_aspectratio = (canvas.width() as f32)/ (canvas.height() as f32);

        self.m_listener.init(&ecs.m_core);

        web_sys::console::log_1(&"TestState1 on_init".into());
        
        /*{
            /// DEBUG
            let mut debug_event = EventHandler::new(Vec::new(),BufferObjEvent::new(&BufferObjInfo::Flat(String::from("Debug"))));
            ecs.m_core.as_ref().unwrap().m_messager.borrow_mut().invoke(&mut debug_event);

            let debug_msg = debug_event.get_event::<BufferObjEvent>();

            if let Some(debug_info) = debug_msg.m_renderinfo_map.get(&String::from("Debug"))
            {
                for info in &debug_info.1
                {
                    self.m_debug_info.push(info.clone());
                }
            }
        }*/
    }

    fn on_enter(&mut self, ecs: &mut EntityComponenetSystem)
    {
        let prev_mouse_pos = self.m_prev_mouse_pos.clone();
        
        let camera = self.m_camera.clone();

        self.m_listener.register::<MouseEvent>(&String::from("Input"),Box::new(move |handle:&mut EventHandler|->Vec<EventHandler>
        {            
            let mouse = handle.get_event::<MouseEvent>();

            match &*mouse
            {
                MouseEvent::MouseDown(btn,pos) =>
                {
                    match btn
                    {
                        MouseButton::Primary => 
                        {
                            *prev_mouse_pos.borrow_mut() = pos.clone();

                            (*camera.borrow_mut()).m_updated = true;
                        },
                        MouseButton::Auxillary => {},
                        MouseButton::Secondary => {}
                    }
                },
                MouseEvent::MouseMove(pos) =>
                {
                    if (*camera.borrow_mut()).m_updated == true
                    {
                        let delta = (*pos - *prev_mouse_pos.borrow_mut()) * 10.0;

                        (*camera.borrow_mut()).m_sphericalcoord.m_target_azimuth_rad -= delta.x;
                        (*camera.borrow_mut()).m_sphericalcoord.m_target_elevation_rad += delta.y;
                        (*camera.borrow_mut()).m_updated = true;
                    }

                    *prev_mouse_pos.borrow_mut() = *pos;
                },
                MouseEvent::MouseScroll(scroll) =>
                {
                    (*camera.borrow_mut()).m_sphericalcoord.m_target_radius += scroll.y * 10.0;// * 0.01f32;   
                },
                MouseEvent::MouseUp(btn,_)=>
                {
                    match btn
                    {
                        MouseButton::Primary => 
                        {
                            (*camera.borrow_mut()).m_updated = false;
                        },
                        MouseButton::Auxillary => {},
                        MouseButton::Secondary => {}
                    }
                }
                _ => { println!("Not supported MouseEvent!");}
            }

            Vec::new()
        }));

        let mut sceneevent_map = Vec::new(); 

        sceneevent_map.push((String::from("Stage"),Scene::new(&Vec4::new(0.2,0.2,0.2,1.0), &Vec2::new(0.0,0.0),&Vec2::new(1.0,1.0))));

        ecs.m_core.as_ref().unwrap().m_messager.borrow_mut().invoke(&mut EventHandler::new(Vec::new(), SceneEvent
        {
            m_scene_list: sceneevent_map
        }));

        //web_sys::console::log_2(&"RenderFlag::PBRAlpha as u32: ".into(),&(RenderFlag::PBRAlpha as u32).into());
    }

    fn on_update(&mut self, ecs: &mut EntityComponenetSystem,timer : &mut Chrono)
    {
        let canvas = &ecs.m_core.as_ref().unwrap().m_canvas;

        let prev_mouse_pos = (*self.m_prev_mouse_pos.borrow_mut()).clone();

        let mut cam = self.m_camera.borrow_mut();

        cam.m_sphericalcoord.update(timer.get_dt());

        let half_fov_y_radian = cam.m_fov_y.to_radians() * 0.5;

        let half_screen_h = half_fov_y_radian.tan() * cam.m_near.abs();
        let half_screen_w = half_screen_h * cam.m_aspectratio;

        let ndc_ray_x = (2.0 * prev_mouse_pos.x) - 1.0;
        let ndc_ray_y = 1.0 - (2.0 * prev_mouse_pos.y);

        let mut entity_colliders = Vec::new();

        let mut whole_min = Vec3::ZERO;
        let mut whole_max = Vec3::ZERO;

        for entity in ecs.get_all_entities()
        {
            let mut aabb_min = Vec3::ZERO;
            let mut aabb_max = Vec3::ZERO;

            if let Some(collision_comp) = entity.get_entity().get_component::<CollisionComponent>()
            {
                aabb_min = collision_comp.m_min;
                aabb_max = collision_comp.m_max;

                whole_min = Vec3::new(
                    whole_min.x.min(aabb_min.x),
                    whole_min.y.min(aabb_min.y),
                    whole_min.z.min(aabb_min.z),
                );
                whole_max = Vec3::new(
                    whole_max.x.max(aabb_max.x),
                    whole_max.y.max(aabb_max.y),
                    whole_max.z.max(aabb_max.z),
                );
            }

            entity_colliders.push(Collider::new(aabb_min,aabb_max,CollisionInfo::Entity(entity.get_name().clone())));
        }

        cam.m_lookat_pos = 0.5 * (whole_min + whole_max);
        
        //let half_y_height = 0.5 * (whole_max.y - whole_min.y);  
        //let distance = 1.5 * half_y_height / half_fov_y_radian.tan();

        //cam.m_sphericalcoord.m_target_radius = distance;

        if entity_colliders.len() > 0
        {
            let ray_org = cam.m_sphericalcoord.get_position();
            let ray_dir = cam.get_view_matrix().inverse() * Vec4::new(ndc_ray_x * half_screen_w, ndc_ray_y * half_screen_h, -cam.m_near,0.0).normalize();

            let kdtree = KDtree::new(entity_colliders.clone());

            let ray_origin = Vector3::new(ray_org.x,ray_org.y,ray_org.z);

            let ray_direction = Vector3::new(ray_dir.x,ray_dir.y,ray_dir.z);
    
            let temp_colliders = kdtree.intersect(&ray_origin, &ray_direction);

            let mut overall_name = String::new();
            let mut overall_t = f32::MAX;

            for temp_collider in temp_colliders
            {
                if let CollisionInfo::Entity(hit_name) = &temp_collider.m_info
                {
                    if let Some(entity) = ecs.get_entity_by_name(&hit_name)
                    {
                        if let Some(mut collision_comp) = entity.get_entity().get_component::<CollisionComponent>()
                        {
                            if let Some(collider) = collision_comp.intersect_ray(&ray_org,&ray_dir.truncate())
                            {
                                let tmin = collide(&collider,&ray_org,&ray_dir.truncate());

                                if tmin < overall_t
                                {
                                    overall_t = tmin;
                                    overall_name = hit_name.clone();
                                }
                            }
                        }
                    }
                }
            }
        
            /*if let RenderInfo::Flat(flat_info) = &mut self.m_debug_info[0]
            {
                flat_info.m_vertices.clear();
                flat_info.m_colors.clear();
                flat_info.m_index.clear();

                flat_info.m_vertices.push(Vec3::new(overall_max.x,overall_max.y,overall_max.z));
                flat_info.m_vertices.push(Vec3::new(overall_max.x,overall_max.y,overall_min.z));
                flat_info.m_vertices.push(Vec3::new(overall_max.x,overall_min.y,overall_max.z));
                flat_info.m_vertices.push(Vec3::new(overall_max.x,overall_min.y,overall_min.z));
                flat_info.m_vertices.push(Vec3::new(overall_min.x,overall_max.y,overall_max.z));
                flat_info.m_vertices.push(Vec3::new(overall_min.x,overall_max.y,overall_min.z));
                flat_info.m_vertices.push(Vec3::new(overall_min.x,overall_min.y,overall_max.z));
                flat_info.m_vertices.push(Vec3::new(overall_min.x,overall_min.y,overall_min.z));
                flat_info.m_vertices.push(Vec3::new(overall_max.x,overall_max.y,overall_max.z));
                flat_info.m_vertices.push(Vec3::new(overall_max.x,overall_min.y,overall_max.z));
                flat_info.m_vertices.push(Vec3::new(overall_min.x,overall_max.y,overall_max.z));
                flat_info.m_vertices.push(Vec3::new(overall_min.x,overall_min.y,overall_max.z));
                flat_info.m_vertices.push(Vec3::new(overall_max.x,overall_max.y,overall_min.z));
                flat_info.m_vertices.push(Vec3::new(overall_max.x,overall_min.y,overall_min.z));
                flat_info.m_vertices.push(Vec3::new(overall_min.x,overall_max.y,overall_min.z));
                flat_info.m_vertices.push(Vec3::new(overall_min.x,overall_min.y,overall_min.z));
                flat_info.m_vertices.push(Vec3::new(overall_max.x,overall_max.y,overall_max.z));
                flat_info.m_vertices.push(Vec3::new(overall_min.x,overall_max.y,overall_max.z));
                flat_info.m_vertices.push(Vec3::new(overall_max.x,overall_max.y,overall_min.z));
                flat_info.m_vertices.push(Vec3::new(overall_min.x,overall_max.y,overall_min.z));
                flat_info.m_vertices.push(Vec3::new(overall_max.x,overall_min.y,overall_max.z));
                flat_info.m_vertices.push(Vec3::new(overall_min.x,overall_min.y,overall_max.z));
                flat_info.m_vertices.push(Vec3::new(overall_max.x,overall_min.y,overall_min.z));
                flat_info.m_vertices.push(Vec3::new(overall_min.x,overall_min.y,overall_min.z));

                for i in 0..24
                {
                    flat_info.m_colors.push(Vec3::new(1.0,0.0,0.0));
                    flat_info.m_index.push(i);
                }
                
                ecs.m_core.as_ref().unwrap().m_messager.borrow_mut().invoke(&mut EventHandler::new(RenderEvent::new(&Mat4::IDENTITY,&RenderInfo::Flat(flat_info.clone()),1.0)));
            }*/

            let mut mtw_mtx = Mat4::IDENTITY;
    
            for entity in ecs.get_all_entities()
            {
                let mut bo_list = Vec::new();
    
                if let Some(mut transform_comp) = entity.get_entity().get_component::<TransformComponent>()
                {            
                    mtw_mtx = transform_comp.get_global_transform();
                }
                
                if let Some(mesh_comp) = entity.get_entity().get_component::<MeshComponent>()
                {
                    bo_list = mesh_comp.m_bo_list.clone();
                }

                let mut renderevent_map = HashMap::new(); 

                renderevent_map.insert(String::from("Stage"),Vec::new());
        
                if let Some(list) = renderevent_map.get_mut(&String::from("Stage"))
                {
                    for bo in bo_list
                    { 
                        let mut renderflag = RenderFlag::Shadow as u64;

                        renderflag |= match bo.m_material.m_alpha_flag
                        {
                            AlphaFlag::Opaque =>
                            {
                                if overall_name == *entity.get_name()
                                {
                                    RenderFlag::PBR as u64 | RenderFlag::PreStencil as u64 | RenderFlag::PostStencil as u64
                                }
                                else
                                {
                                    RenderFlag::PBR as u64
                                }
                            },
                            AlphaFlag::Blend =>
                            {
                                if overall_name == *entity.get_name()
                                {
                                    RenderFlag::PBRAlpha as u64 | RenderFlag::PreStencil as u64 | RenderFlag::PostStencil as u64
                                }
                                else
                                {
                                    RenderFlag::PBRAlpha as u64
                                }
                            },
                            AlphaFlag::Mask =>
                            {
                                if overall_name == *entity.get_name()
                                {
                                    RenderFlag::PBRAlpha as u64 | RenderFlag::PreStencil as u64 | RenderFlag::PostStencil as u64
                                }
                                else
                                {
                                    RenderFlag::PBRAlpha as u64
                                }
                            },
                        };

                        list.push(RenderBlock
                        {
                            m_material: bo.m_material.clone(),
                            m_buffer_obj: bo.m_buffer_obj.clone(),
                            m_model_mtx: mtw_mtx.clone(),
                            m_opacity: 1.0,
                            m_render_flag: renderflag,
                        });
                    }
                }
        
                ecs.m_core.as_ref().unwrap().m_messager.borrow_mut().invoke(&mut EventHandler::new(Vec::new(),RenderEvent
                {
                    m_block_map: renderevent_map,
                }));
            }

        }

        let light_angle: f32 = 30.0;

        let light_pos = Quat::from_axis_angle(Vec3::new(0.0,1.0,0.0), light_angle.to_radians()) * cam.m_sphericalcoord.get_position().normalize();

        let mut lightevent_map = HashMap::new(); 

        let mut light = Light::new(&light_pos);
        light.m_intensity = 1.5;

        lightevent_map.insert(String::from("Stage"),light);

        ecs.m_core.as_ref().unwrap().m_messager.borrow_mut().invoke(&mut EventHandler::new(Vec::new(), LightEvent
        {
            m_light_map: lightevent_map
        }));

        let mut cameraevent_map = HashMap::new(); 

        cameraevent_map.insert(String::from("Stage"),(*cam).clone());

        ecs.m_core.as_ref().unwrap().m_messager.borrow_mut().invoke(&mut EventHandler::new(Vec::new(), CameraEvent
        {
            m_camera_map: cameraevent_map
        }));
    }

    fn on_exit(&mut self, ecs: &mut EntityComponenetSystem)
    {
        self.m_listener.deregister::<MouseEvent>(&String::from("Input"));
    }

    fn on_free(&mut self, ecs: &mut EntityComponenetSystem)
    {
        ecs.remove_all_entities();
    }
}