use crate::engine::event::{EventListener, EventHandler, Event};
use std::cell::RefCell;
use std::rc::Rc;
use std::array;
use std::collections::HashMap;
use glam::{Mat4, Vec3, Vec4, Vec2, Quat};
use crate::engine::{Core, Chrono};
use crate::engine::world::{EntityComponenetSystem};
use serde::{Serialize, Deserialize,Serializer,Deserializer};
use std::fmt::{self, Formatter};
use serde::ser::{SerializeStruct};
use serde::de::{self, Visitor, MapAccess};
use crate::engine::input::{MouseEvent};
use crate::engine::statemachine::{State, WorldEvent};
use crate::engine::render::{BufferObjEvent, RenderEvent,RenderFlag,RenderBlock};
use crate::engine::component::{TransformComponent, MeshComponent};
use crate::engine::resource::{PosNormTangTexCol};

static VERTICES: [[f32;3]; 3] = [
    [0.0, 0.5, 0.0], //
    [-0.5, -0.5, 0.0], //
    [0.5, -0.5, 0.0], //
];

static INDICES: [u32; 3] = [0, 1, 2];

pub struct TestState2
{
    m_listener: EventListener,
    m_angle: Rc<RefCell<f32>>,
}

impl Serialize for TestState2
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut s = serializer.serialize_struct("TestState2", 1)?;
        s.serialize_field("m_angle",&*self.m_angle.borrow())?;
        s.end()
    }
}

impl<'de> Deserialize<'de> for TestState2 {
    fn deserialize<D>(deserializer: D) -> Result<TestState2, D::Error>
    where
        D: Deserializer<'de>,
    {
        web_sys::console::log_1(&"Deserialize TestState2 enter!".into());

        struct TestState2Visitor;

        impl<'de> Visitor<'de> for TestState2Visitor {
            type Value = TestState2;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result 
            {
                formatter.write_str("struct TestState2")
            }

            fn visit_map<V>(self, mut map: V) -> Result<Self::Value, V::Error>
            where
                V: MapAccess<'de>,
            {
                web_sys::console::log_1(&"visit_map enter!".into());
                let mut angle = None;
                while let Some(key) = map.next_key()? {
                    match key {
                        "m_angle" => {
                            web_sys::console::log_1(&"state added begin!".into());
                            if angle.is_some() {
                                return Err(de::Error::duplicate_field("m_angle"));
                            }
                            web_sys::console::log_1(&"state added end!".into());
                            angle = Some(map.next_value::<f32>()?);
                            web_sys::console::log_1(&"state added done!".into());
                        }
                        _=>
                        {
                            panic!("Component key do not work!")
                        }
                    }
                }
                let angle = angle.ok_or_else(|| de::Error::missing_field("m_angle"))?;
                let mut state = TestState2::new();
                state.m_angle = Rc::new(RefCell::new(angle));
                Ok(state)
            }
        }
        const FIELDS: &'static [&'static str] = &["m_angle"];
        deserializer.deserialize_struct("TestState2", FIELDS, TestState2Visitor)
    }
}

impl State for TestState2
{
    fn new() -> Self
    {
        let listener = EventListener::new();

        TestState2{m_listener : listener, m_angle: Rc::new(RefCell::new(0.0))}
    }

    fn on_init(&mut self, ecs: &mut EntityComponenetSystem)
    {
        self.m_listener.init(&ecs.m_core);

        web_sys::console::log_1(&"TestState2 on_init".into());

        /*if let Some(entity) = ecs.add_entity_by_name(&String::from("Triangle2"))
        {
            entity.get_entity().add_component::<TransformComponent>(TransformComponent::new());
        }*/
        
        /*let vertices = array::IntoIter::new(VERTICES).map(|pos|
            PosNormTangTex {
                m_position: pos.into(),
                m_normal: Vec3::new(0.0,0.0,0.0),
                m_tangent: Vec4::new(0.0,0.0,0.0,0.0),
                m_tex_coord: Vec2::new(0.0,0.0),
            }).collect::<Vec<_>>();

        let mut buf_obj_event = EventHandler::new(BufferObjEvent::new(&vertices,&INDICES.to_vec()));
        ecs.m_core.as_ref().unwrap().m_messager.borrow_mut().invoke(&mut buf_obj_event);
        
        {
            let entity = ecs.get_entity_by_name(&String::from("Triangle2"));
            
            let buf_obj_msg = buf_obj_event.get_event::<BufferObjEvent>();

            entity.add_component::<MeshComponent>(MeshComponent::new(&buf_obj_msg.m_vbo_ibo_list.clone()));
            
            /*if let Some(mut mesh_comp) = entity.get_component::<MeshComponent>()
            {
                mesh_comp.m_vbo = buf_obj_msg.m_vbo.clone();
                mesh_comp.m_ibo = buf_obj_msg.m_ibo.clone();
            }*/
        };*/
    }

    fn on_enter(&mut self, ecs: &mut EntityComponenetSystem)
    {
        let angle = self.m_angle.clone();

        self.m_listener.register::<MouseEvent>(&String::from("Input"),Box::new(move |handle:&mut EventHandler|->Vec<EventHandler>
        {
            let mouse = handle.get_event::<MouseEvent>();

            match *mouse
            {
                MouseEvent::MouseScroll(scroll) =>
                {
                    *angle.borrow_mut() += scroll.y * 0.01f32;
                },
                MouseEvent::MouseDown(_,_) =>
                {
                    return vec![EventHandler::new(Vec::new(),WorldEvent::ChangeState{m_state: String::from("STATE1")})]
                },
                _ => { println!("Not supported MouseEvent!");}
            }

            Vec::new()
        }));
    }

    fn on_update(&mut self, ecs: &mut EntityComponenetSystem,timer : &mut Chrono)
    {
        let mut mtw_mtx = Mat4::IDENTITY;
        //let mut vbo = None;
        //let mut ibo = None;

        let mut bo_list = Vec::new();

        {
            if let Some(entity) = ecs.get_entity_by_name(&String::from("Triangle2"))
            {
                if let Some(mut transform_comp) = entity.get_entity().get_component::<TransformComponent>()
                {
                    transform_comp.m_rotation =  Quat::from_rotation_z(*self.m_angle.borrow_mut());
    
                    mtw_mtx = transform_comp.get_global_transform();
                }
    
                if let Some(mesh_comp) = entity.get_entity().get_component::<MeshComponent>()
                {
                    //vbo = mesh_comp.m_vbo.clone();
                    //ibo = mesh_comp.m_ibo.clone();
    
                    bo_list = mesh_comp.m_bo_list.clone();
                }
            }
        }
    
        let mut renderevent_map = HashMap::new(); 

        renderevent_map.insert(String::from("Stage"),Vec::new());

        if let Some(list) = renderevent_map.get_mut(&String::from("Stage"))
        {
            for bo in bo_list
            {
                list.push(RenderBlock
                    {
                        m_material: bo.m_material,
                        m_buffer_obj: bo.m_buffer_obj,
                        m_model_mtx: mtw_mtx.clone(),
                        m_opacity: 1.0,
                        m_render_flag: RenderFlag::PBR as u64 | RenderFlag::Shadow as u64,
                    });
            }
        }

        ecs.m_core.as_ref().unwrap().m_messager.borrow_mut().invoke(&mut EventHandler::new(Vec::new(),RenderEvent
        {
            m_block_map: renderevent_map,
        }));
    }

    fn on_exit(&mut self, ecs: &mut EntityComponenetSystem)
    {
        self.m_listener.deregister::<MouseEvent>(&String::from("Input"));
    }

    fn on_free(&mut self, ecs: &mut EntityComponenetSystem)
    {
        ecs.remove_all_entities();
    }
}