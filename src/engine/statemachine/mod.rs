use std::cell::RefCell;
use std::rc::Rc;
use uuid::Uuid;
use std::any::Any;
use std::hash::Hash;
use crate::engine::{Core,Chrono};
use std::collections::HashMap;
pub use teststate1::TestState1;
pub use teststate2::TestState2;
use crate::engine::event::{Event};
use serde::{Serialize, Serializer,Deserialize,Deserializer};
use serde::ser::{SerializeMap, SerializeSeq};
use serde::de::{self, Visitor, MapAccess, SeqAccess, DeserializeSeed};
use erased_serde::{Serialize as ErasedSerialize, Deserializer as ErasedDeserializer};
use std::fmt::{self, Formatter};

pub mod teststate2;
pub mod teststate1;
use crate::engine::world::{EntityComponenetSystem};

pub enum WorldEvent
{
    ChangeState{m_state: String},
    ChangeWorld{m_world: String},
    ReloadWorld,
}

impl Event for WorldEvent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }
}

pub trait State : ErasedSerialize
{
    fn new() -> Self where Self: Sized;
    fn on_init(&mut self, _ecs: &mut EntityComponenetSystem){}
    fn on_enter(&mut self, _ecs: &mut EntityComponenetSystem){}
    fn on_update(&mut self, _ecs: &mut EntityComponenetSystem,_timer : &mut Chrono){}
    fn on_exit(&mut self, _ecs: &mut EntityComponenetSystem){}
    fn on_free(&mut self, _ecs: &mut EntityComponenetSystem){}
}

erased_serde::serialize_trait_object!(State);

#[derive(Clone)]
pub struct StateHandler
{
    pub m_name: String,
    pub m_state: Rc<RefCell<dyn State>>,
}

impl Serialize for StateHandler
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut s = serializer.serialize_map(Some(1))?;
        s.serialize_entry(&self.m_name, &*self.m_state.borrow())?;
        s.end()
    }
}

pub struct StateHandlerSeed;

impl<'de> DeserializeSeed<'de> for StateHandlerSeed
{
    type Value = StateHandler ;

    fn deserialize<D>(self,deserializer: D) -> Result<Self::Value, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct StateHandlerVisitor;

        impl<'de> Visitor<'de> for StateHandlerVisitor {
            type Value = StateHandler;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result 
            {
                formatter.write_str("struct StateHandler")
            }

            fn visit_map<V>(self, mut map: V) -> Result<Self::Value, V::Error>
            where
                V: MapAccess<'de>,
            {
                web_sys::console::log_1(&"visit_map enter!".into());
                let mut name = None;
                let mut state = None;
                while let Some(key) = map.next_key::<String>()? 
                {
                    if name.is_some() {
                        return Err(de::Error::duplicate_field("m_name"));
                    }
                    name = Some(key.clone());
                    web_sys::console::log_1(&"state added begin!".into());
                    if state.is_some() {
                        return Err(de::Error::duplicate_field("m_state"));
                    }
                    web_sys::console::log_1(&"state added end!".into());
                    state = Some(map.next_value_seed(ErasedState{m_state_name:key.clone()})?);
                    web_sys::console::log_1(&"state added done!".into());
                }
                let name = name.ok_or_else(|| de::Error::missing_field("m_name"))?;
                let state = state.ok_or_else(|| de::Error::missing_field("m_state"))?;
                Ok(StateHandler::new_2(&name,state))
            }
        }

        const FIELDS: &'static [&'static str] = &["m_name", "m_uuid","m_state"];
        deserializer.deserialize_struct("StateHandler", FIELDS, StateHandlerVisitor)
    }
}

struct ErasedState
{
    m_state_name: String,
}

impl<'de> DeserializeSeed<'de> for ErasedState
{
    type Value = Rc<RefCell<dyn State>> ;

    fn deserialize<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
    where
        D: Deserializer<'de>,
    {
        web_sys::console::log_1(&"DeserializeSeed enter!".into());

        let mut result: Box<dyn ErasedDeserializer> = Box::new(ErasedDeserializer::erase(deserializer));

        web_sys::console::log_1(&"DeserializeSeed enter2 !".into());

        match self.m_state_name.as_str()
        {
            "STATE1"=>
            {
                if let Ok(state) = erased_serde::deserialize::<TestState1>(result.as_mut())
                {
                    return Ok(Rc::new(RefCell::new(state)))
                }
            },
            "STATE2"=>
            {
                if let Ok(state) = erased_serde::deserialize::<TestState2>(result.as_mut())
                {
                    return Ok(Rc::new(RefCell::new(state)))
                }
        
            },
            _=>
            {
                panic!("Component key not supported for deserialize!")
            }
        }

        web_sys::console::log_1(&"DeserializeSeed end!".into());

        Err(de::Error::custom("No deserialization for state type!"))
    }
}

impl StateHandler
{
    pub fn new<T: State + 'static>(name : &String, state : T) -> StateHandler
    {
        let state_wrapper = Rc::new(RefCell::new(state));

        StateHandler{ m_name: name.clone(),m_state: state_wrapper as Rc<RefCell<dyn State>>}
    }

    pub fn new_2(name : &String,state : Rc<RefCell<dyn State>>) -> StateHandler
    {
        StateHandler{ m_name: name.clone(),m_state: state as Rc<RefCell<dyn State>>}
    }
}

#[derive(Serialize,Deserialize)]
pub struct StateMachine
{
    m_curr_id: Option<String>,
    m_next_id: Option<String>,
    #[serde(serialize_with = "serialize_state_map")]
    #[serde(deserialize_with = "deserialize_state_map")]
    m_state_map: HashMap<String,StateHandler>,
}

fn serialize_state_map<S>(state_map:&HashMap<String,StateHandler>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let mut seq = serializer.serialize_seq(Some(state_map.len()))?;
    for (_, v) in state_map {
        seq.serialize_element(&v)?;
    }
    seq.end()
}

fn deserialize_state_map<'de, D>(deserializer: D) -> Result<HashMap<String,StateHandler>, D::Error> 
where D: Deserializer<'de>
{
    struct HashMapStateVisitor;

    impl<'de> Visitor<'de> for HashMapStateVisitor {
        type Value = HashMap<String,StateHandler>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result 
        {
            formatter.write_str("struct HashMap<String,StateHandler>")
        }

        fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
        where
            A: SeqAccess<'de>, 
        {
            web_sys::console::log_1(&"visit_seq enter!".into());
            let mut map = HashMap::new();
            while let Some(handle) = seq.next_element_seed(StateHandlerSeed)? 
            {
                map.insert(handle.m_name.clone(),handle.clone());
            }
            Ok(map)
        }
    }

    deserializer.deserialize_seq(HashMapStateVisitor)
}

impl Default for StateMachine
{
    fn default() -> Self
    {
        StateMachine::new()
    }
}

impl StateMachine
{
    pub fn new() -> StateMachine
    {
        let state_map: HashMap<String,StateHandler> = HashMap::new();

        StateMachine{m_curr_id: None, m_next_id: None,m_state_map: state_map}
    }

    pub fn add_state<T: State + 'static>(&mut self, state_id: &String, state: T)
    {
        self.m_state_map.insert(state_id.clone(),StateHandler::new(state_id,state));
    }

    pub fn default_state(&mut self, state_id: &String)
    {    
        self.m_curr_id = Some(state_id.clone());

        self.m_next_id = Some(state_id.clone());
    }

    pub fn change_state(&mut self, state_id: &String)
    {
        if !self.m_state_map.contains_key(&state_id.clone())
        {
            panic!("Invalid next state!");
        }
    
        self.m_next_id = Some(state_id.clone());
    }
    
    pub fn init(&mut self,ecs: &mut EntityComponenetSystem)
    {
        if self.m_curr_id.is_none()
        {
            panic!("Invalid starting state!");
        }

        for (key, value) in &mut self.m_state_map
        {
            value.m_state.borrow_mut().on_init(ecs);
        }
        
        if let Some(index) = &self.m_curr_id
        {
            self.m_state_map[index].m_state.borrow_mut().on_enter(ecs);
        }
    }

    pub fn update(&mut self,ecs: &mut EntityComponenetSystem, timer: &mut Chrono)
    {
        if self.m_curr_id != self.m_next_id
        {
            if let Some(index) = &self.m_curr_id
            {
                self.m_state_map[index].m_state.borrow_mut().on_exit(ecs);
            }

            self.m_curr_id = self.m_next_id.clone();

            if let Some(index) = &self.m_curr_id
            {
                self.m_state_map[index].m_state.borrow_mut().on_enter(ecs);
            }
        }
        else
        {
            if let Some(index) = &self.m_curr_id
            {
                self.m_state_map[index].m_state.borrow_mut().on_update(ecs,timer);
            }
        }
    }

    pub fn free(&mut self)
    {
        self.m_state_map.clear();
    }
}
