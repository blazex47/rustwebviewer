use crate::engine::{Core, UniqueIDRegistry};
use web_sys::{HtmlCanvasElement};
use std::rc::Rc;
use std::any::Any;
use glam::{Vec3, Vec2};
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen::JsValue;
use web_sys::*;
use crate::engine::event::{Event, EventHandler, EventMessager};

#[derive(Clone)]
pub enum MouseButton
{
    Primary,
    Secondary,
    Auxillary
}

#[derive(Clone)]
pub enum MouseEvent 
{
    MouseDown(MouseButton,Vec2),
    MouseUp(MouseButton,Vec2),
    MouseMove(Vec2),
    MouseScroll(Vec3),
}

impl Event for MouseEvent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }
}

pub struct InputManager
{
    m_core: Option<Core>,
}

impl InputManager
{
    pub fn new()->InputManager
    {
        InputManager{m_core: None}
    }

    pub fn init(&mut self,core: &Option<Core>)
    {
        self.m_core = core.clone();

        attach_mouse_down_handler(&self.m_core);
        attach_mouse_up_handler(&self.m_core);
        attach_mouse_move_handler(&self.m_core);
        attach_mouse_wheel_handler(&self.m_core);
    
        attach_touch_start_handler(&self.m_core);
        attach_touch_move_handler(&self.m_core);
        attach_touch_end_handler(&self.m_core);
    }
}
    fn attach_mouse_down_handler(core: &Option<Core>) -> Result<(), JsValue> 
    {
        let messager = core.as_ref().unwrap().m_messager.clone();

        let canvas = core.as_ref().unwrap().m_canvas.clone();

        let handler = move |event: web_sys::MouseEvent| 
        {
            let left = canvas.offset_left();
            let top = canvas.offset_top();

            let x = (event.client_x() - left) as f32 / (canvas.offset_width() as f32);
            let y = (event.client_y() - top) as f32 / (canvas.offset_height() as f32);

            let mouse_event: MouseEvent;
            let mouse_point = Vec2::new(x,y);

            match event.button()
            {
                0 => { mouse_event = MouseEvent::MouseDown(MouseButton::Primary,mouse_point);},
                1 => { mouse_event = MouseEvent::MouseDown(MouseButton::Auxillary,mouse_point);},
                2 => { mouse_event = MouseEvent::MouseDown(MouseButton::Secondary,mouse_point);},
                _ => { panic!("Mouse event not handled!"); }
            }

            messager.borrow_mut().invoke(&mut EventHandler::new(Vec::new(),mouse_event));
        };

        let handler = Closure::wrap(Box::new(handler) as Box<dyn FnMut(_)>);
        core.as_ref().unwrap().m_canvas.add_event_listener_with_callback("mousedown", handler.as_ref().unchecked_ref())?;
        handler.forget();

        Ok(())
    }

    fn attach_mouse_up_handler(core: &Option<Core>) -> Result<(), JsValue> 
    {
        let messager = core.as_ref().unwrap().m_messager.clone();

        let canvas = core.as_ref().unwrap().m_canvas.clone();

        let handler = move |event: web_sys::MouseEvent| 
        {
            let left = canvas.offset_left();
            let top = canvas.offset_top();

            let x = (event.client_x() - left) as f32 / (canvas.offset_width() as f32);
            let y = (event.client_y() - top) as f32 / (canvas.offset_height() as f32);

            let mouse_event: MouseEvent;
            let mouse_point = Vec2::new(x,y);

            match event.button()
            {
                0 => { mouse_event = MouseEvent::MouseUp(MouseButton::Primary,mouse_point);},
                1 => { mouse_event = MouseEvent::MouseUp(MouseButton::Auxillary,mouse_point);},
                2 => { mouse_event = MouseEvent::MouseUp(MouseButton::Secondary,mouse_point);},
                _ => { panic!("Mouse event not handled!"); }
            }

            messager.borrow_mut().invoke(&mut EventHandler::new(Vec::new(),mouse_event));
        };

        let handler = Closure::wrap(Box::new(handler) as Box<dyn FnMut(_)>);
        core.as_ref().unwrap().m_canvas.add_event_listener_with_callback("mouseup", handler.as_ref().unchecked_ref())?;
        handler.forget();

        Ok(())
    }

    fn attach_mouse_move_handler(core: &Option<Core>) -> Result<(), JsValue> 
    {
        let messager = core.as_ref().unwrap().m_messager.clone();
        
        let canvas = core.as_ref().unwrap().m_canvas.clone();

        let handler = move |event: web_sys::MouseEvent| 
        {
            let left = canvas.offset_left();
            let top = canvas.offset_top();

            event.prevent_default();
            let x = (event.client_x() - left) as f32 / (canvas.offset_width() as f32);
            let y = (event.client_y() - top) as f32 / (canvas.offset_height() as f32);

            messager.borrow_mut().invoke(&mut EventHandler::new(Vec::new(),MouseEvent::MouseMove(Vec2::new(x,y))));
        };

        let handler = Closure::wrap(Box::new(handler) as Box<dyn FnMut(_)>);
        core.as_ref().unwrap().m_canvas.add_event_listener_with_callback("mousemove", handler.as_ref().unchecked_ref())?;
        handler.forget();

        Ok(())
    }

    fn attach_mouse_wheel_handler(core: &Option<Core>) -> Result<(), JsValue> 
    {
        let messager = core.as_ref().unwrap().m_messager.clone();

        let handler = move |event: web_sys::WheelEvent| 
        {
            event.prevent_default();

            let x = (event.delta_x() / 50.) as f32;
            let y = (event.delta_y() / 50.) as f32;
            let z = (event.delta_z() / 50.) as f32;

            messager.borrow_mut().invoke(&mut EventHandler::new(Vec::new(),MouseEvent::MouseScroll(Vec3::new(x,y,z))));
        };

        let handler = Closure::wrap(Box::new(handler) as Box<dyn FnMut(_)>);
        core.as_ref().unwrap().m_canvas.add_event_listener_with_callback("wheel", handler.as_ref().unchecked_ref())?;
        handler.forget();

        Ok(())
    }

    fn attach_touch_start_handler(core: &Option<Core>) -> Result<(), JsValue> 
    {
        let messager = core.as_ref().unwrap().m_messager.clone();

        let canvas = core.as_ref().unwrap().m_canvas.clone();

        let handler = move |event: web_sys::TouchEvent| 
        {
            let left = canvas.offset_left();
            let top = canvas.offset_top();

            let touch = event.touches().item(0).expect("First Touch");
            let x = (touch.client_x() - left) as f32 / (canvas.offset_width() as f32);
            let y = (touch.client_y() - top) as f32 / (canvas.offset_height() as f32);

            messager.borrow_mut().invoke(&mut EventHandler::new(Vec::new(),MouseEvent::MouseDown(MouseButton::Primary,Vec2::new(x,y))));
        };

        let handler = Closure::wrap(Box::new(handler) as Box<dyn FnMut(_)>);
        core.as_ref().unwrap().m_canvas.add_event_listener_with_callback("touchstart", handler.as_ref().unchecked_ref())?;
        handler.forget();

        Ok(())
    }

    fn attach_touch_move_handler(core: &Option<Core>) -> Result<(), JsValue> 
    {
        let messager = core.as_ref().unwrap().m_messager.clone();
        
        let canvas = core.as_ref().unwrap().m_canvas.clone();

        let handler = move |event: web_sys::TouchEvent| 
        {
            let left = canvas.offset_left();
            let top = canvas.offset_top();

            event.prevent_default();
            let touch = event.touches().item(0).expect("First Touch");
            let x = (touch.client_x() - left) as f32 / (canvas.offset_width() as f32);
            let y = (touch.client_y() - top) as f32 / (canvas.offset_height() as f32);

            messager.borrow_mut().invoke(&mut EventHandler::new(Vec::new(),MouseEvent::MouseMove(Vec2::new(x,y))));
        };

        let handler = Closure::wrap(Box::new(handler) as Box<dyn FnMut(_)>);
        core.as_ref().unwrap().m_canvas.add_event_listener_with_callback("touchmove", handler.as_ref().unchecked_ref())?;
        handler.forget();

        Ok(())
    }

    fn attach_touch_end_handler(core: &Option<Core>) -> Result<(), JsValue> 
    {
        let messager = core.as_ref().unwrap().m_messager.clone();

        let canvas = core.as_ref().unwrap().m_canvas.clone();

        let handler = move |event: web_sys::TouchEvent| 
        {
            let left = canvas.offset_left();
            let top = canvas.offset_top();

            let touch = event.touches().item(0).expect("First Touch");
            let x = (touch.client_x() - left) as f32 / (canvas.offset_width() as f32);
            let y = (touch.client_y() - top) as f32 / (canvas.offset_height() as f32);

            messager.borrow_mut().invoke(&mut EventHandler::new(Vec::new(),MouseEvent::MouseUp(MouseButton::Primary,Vec2::new(x,y))));
        };

        let handler = Closure::wrap(Box::new(handler) as Box<dyn FnMut(_)>);

        core.as_ref().unwrap().m_canvas.add_event_listener_with_callback("touchend", handler.as_ref().unchecked_ref())?;

        handler.forget();

        Ok(())
    }
