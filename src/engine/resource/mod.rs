use std::fs::File;
use std::io::prelude::*;
use std::io::{Cursor};
use uuid::Uuid;
use std::any::Any;
use std::cell::RefCell;
use std::rc::Rc;
use std::collections::HashMap;
use crate::engine::event::{EventListener, EventHandler, Event};
use crate::engine::render::{BufferObjEvent, BufferObjInfo, RenderEvent};
use crate::engine::{Core, Chrono, UniqueIDRegistry};
use image::{ImageFormat};
use glam::{Mat4, Vec2, Vec3, Vec4, Quat};

//extern crate glam;
extern crate gltf;
extern crate image;


pub struct AssetRetrieveEvent
{
    pub m_request_id: String,
    pub m_data: Vec<u8>,
    pub m_status_code: u32,
}

impl Event for AssetRetrieveEvent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }
}

impl AssetRetrieveEvent
{
    pub fn new(request_id: &String,data: &Vec<u8>,status_code: u32) -> Self
    {
        AssetRetrieveEvent{m_request_id:request_id.clone(), m_data: data.clone(), m_status_code: status_code}
    }
}

pub struct ResourceRequestEvent
{
    m_name: String,
    m_uri: String,
}

impl Event for ResourceRequestEvent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }
}

impl ResourceRequestEvent
{
    pub fn new(name: String, uri: String) -> ResourceRequestEvent
    {
        ResourceRequestEvent{ m_name: name, m_uri: uri}
    }
}

pub struct AssetRequestEvent
{
    pub m_uri: String,
    pub m_request_id: String,
}

impl AssetRequestEvent
{
    fn new(request_id: &String, uri: &String)-> Self
    {
        AssetRequestEvent{m_request_id: request_id.clone(), m_uri: uri.clone()}
    }
}

impl Event for AssetRequestEvent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }
}

pub enum ResourceLoader
{
    Gltf2(Model),
}

impl ResourceLoader
{
    pub fn load_from_directory<T : Resource>(buffer:&Vec<u8>) -> Option<T>
    {
        //if let Ok(mut file) = File::open(file_path)
        //{
            let mut result = T::new();

            //let mut buffer = Vec::new();

            //file.read_to_end(&mut buffer);

            result.parse(&buffer);

            return Some(result);
        //}

        //None
    }
}

pub struct ResourceRequest
{
    pub m_request: HashMap<String,ResourceLoader>,
}

impl ResourceRequest
{
    pub fn new(request: HashMap<String,ResourceLoader>) -> Self
    {
        ResourceRequest{m_request: request }
    }

    pub fn load(&mut self, id: &String, data: &Vec<u8>)
    {
        if let Some(loader) = self.m_request.get_mut(id)
        {
            match loader
            {
                ResourceLoader::Gltf2(model) =>
                {
                    model.parse(data);
                },
            }
        }
    }

    pub fn check_ready(&mut self) -> Vec<EventHandler>
    {
        let mut model = Model::new();

        for (_, loader) in &self.m_request
        {
            match loader
            {
                ResourceLoader::Gltf2(check_model) =>
                {
                    if check_model.ready() == false
                    {
                        return Vec::new();
                    }
                    model = check_model.clone();
                },
                // more kind of loader
            }
        }

        // base on all loader trigger the right event
        vec![EventHandler::new(vec![String::from("BufferObj0")],BufferObjEvent::new(&BufferObjInfo::PBR(model)))]
    }
}

#[derive(Clone)]
pub struct ResourceInfo
{
    m_uri : String,
    m_uuid: Uuid,
    m_delay: Chrono,
    m_retries: u32,
}

impl ResourceInfo
{
    fn new(uri: &String, uuid: &Uuid) -> Self
    {
        ResourceInfo{ m_uri: uri.clone(), m_uuid: uuid.clone(), m_delay: Chrono::new(0.0), m_retries: 0}
    }
}

pub struct ResourceManager
{
    m_core: Option<Core>,
    m_listener: EventListener,
    m_look_up: Rc<RefCell<HashMap<String,ResourceInfo>>>,
    m_poll_map: Rc<RefCell<HashMap<Uuid,ResourceRequest>>>,
}

impl ResourceManager
{
    pub fn new() -> Self  
    {
        let look_up = Rc::new(RefCell::new(HashMap::new()));

        let poll_map = Rc::new(RefCell::new(HashMap::new()));

        ResourceManager{m_core: None, m_listener: EventListener::new(), m_look_up: look_up, m_poll_map: poll_map }
    }

    pub fn init(&mut self,core: &Option<Core>)
    {
        self.m_core = core.clone();

        self.m_listener.init(&self.m_core);

        let look_up = self.m_look_up.clone();

        let poll_map = self.m_poll_map.clone();

        self.m_listener.register::<AssetRetrieveEvent>(&String::from("AssetRetrieve"),Box::new(move |handle:&mut EventHandler|->Vec<EventHandler>
        {
            let asset_retrieve = handle.get_event::<AssetRetrieveEvent>();
            
            let mut result_info = None;

            if let Some(info) = look_up.borrow_mut().get_mut(&asset_retrieve.m_request_id)
            {
                result_info = Some(info.clone());
            }

            if let Some(info) = result_info
            {
                match asset_retrieve.m_status_code
                {
                    200 => //OK
                    {
                        let mut result = Vec::new();

                        if let Some(request) = poll_map.borrow_mut().get_mut(&info.m_uuid)
                        {
                            request.load(&asset_retrieve.m_request_id,&asset_retrieve.m_data);

                            result = request.check_ready();
                        }

                        if result.is_empty() == false
                        {
                            poll_map.borrow_mut().remove(&info.m_uuid);

                            look_up.borrow_mut().remove(&asset_retrieve.m_request_id);

                            return result;
                        }
                    },
                    201 => //Created
                    {
                        //TODO: Retries and Delays
                    },
                    202 => //Accepted
                    {
                        //TODO: Retries and Delays                            
                    },
                    204 => //No Content
                    {
                        //TODO: Retries and Delays                            
                    },
                    code =>
                    {
                        panic!("Status Code {} is not handled",code)
                    }
                }
            }


            Vec::new()
        }));

        let look_up = self.m_look_up.clone();

        let poll_map = self.m_poll_map.clone();

        self.m_listener.register::<ResourceRequestEvent>(&String::from("ResourceRequest"),Box::new(move |handle:&mut EventHandler|->Vec<EventHandler>
        {
            let resource_request = handle.get_event::<ResourceRequestEvent>();
                
            let uuid = Uuid::new_v4();

            let request_id = resource_request.m_name.clone() + uuid.to_string().as_str();

            look_up.borrow_mut().insert(request_id.clone(),ResourceInfo::new(&resource_request.m_uri,&uuid));

            let mut request = HashMap::new();

            request.insert(request_id.clone(),ResourceLoader::Gltf2(Model::new()));

            poll_map.borrow_mut().insert(uuid,ResourceRequest::new(request));

            vec![EventHandler::new(Vec::new(),AssetRequestEvent::new(&request_id,&resource_request.m_uri))]
        }));
    }

    pub fn update(&mut self, timer: &mut Chrono)
    {

    }

    pub fn free(&mut self)
    {
        self.m_listener.deregister::<AssetRetrieveEvent>(&String::from("AssetRetrieve"));

        self.m_listener.deregister::<ResourceRequestEvent>(&String::from("ResourceRequest"));
    }
}

pub trait Resource
{
    fn new()-> Self;
    fn ready(&self) -> bool;
    fn parse(&mut self, data : &Vec<u8>);
}

#[derive(Clone)]
pub struct PosNormTangTexCol {
    pub m_position: Vec3,
    pub m_normal: Vec3,
    pub m_tangent: Vec4,
    pub m_tex_coord: Vec2,
    pub m_color: Vec4,
}

#[derive(Clone)]
pub struct Model
{
    m_ready: bool,
    pub m_images: Vec<Image>,
    pub m_meshes: Vec<Mesh>,
    pub m_materials: Vec<Material>,
}

impl Resource for Model
{
    fn new()-> Self
    {
        Model{m_ready: false, m_images: Vec::new(), m_meshes : Vec::new(), m_materials: Vec::new()}
    }

    fn ready(&self) -> bool
    {
        self.m_ready
    }

    fn parse(&mut self, data : &Vec<u8>)
    {
        if let Ok((document, buffers, images)) = gltf::import_slice(data.as_slice())
        {
            let mut lookup: HashMap<usize,Mat4> = HashMap::new();

            for node in document.nodes()
            {
                let trans = match node.transform()
                {
                    gltf::scene::Transform::Matrix{matrix} =>
                    {
                        Mat4::from_cols_array_2d(&matrix)
                    },
                    gltf::scene::Transform::Decomposed{translation,rotation,scale} =>
                    {
                        Mat4::from_scale_rotation_translation(Vec3::from(scale),Quat::from(rotation),Vec3::from(translation))
                    },
                };

                if let Some(mtx) = lookup.get_mut(&node.index())
                {
                    (*mtx) = trans * (*mtx);
                }
                else
                {
                    lookup.insert(node.index(),trans);
                }

                for child_node in node.children()
                {
                    if let Some(mtx) = lookup.get_mut(&child_node.index())
                    {
                        (*mtx) = trans * (*mtx);
                    }
                    else
                    {
                        lookup.insert(child_node.index(),trans);
                    }
                }
            }

            for node in document.nodes()
            {
                if let Some(mesh) = node.mesh()
                {
                    let trans = match lookup.get_mut(&node.index())
                    {
                        Some(mtx) => (*mtx),
                        None => Mat4::IDENTITY,
                    };

                    if let Ok(mut result_mesh) = load_gltf_mesh(&mesh,&buffers)
                    {
                        result_mesh.m_name = String::from(node.name().expect("Node does not have name!"));

                        result_mesh.m_transform = trans;

                        self.m_meshes.push(result_mesh);
                    }
                }
            }

            /*for mesh in document.meshes()
            {
                if let Ok(result_mesh) = load_gltf_mesh(&mesh,&buffers,)
                {
                    self.m_meshes.push(result_mesh);
                }
            }*/

            for material in document.materials()
            {
                if let Ok(result_material) = load_gltf_material(&material,&buffers,)
                {
                    self.m_materials.push(result_material);
                }
            }

            for texture in document.textures()
            {
                if let Ok(result_image) = load_gltf_image(&texture,&images,)
                {
                    self.m_images.push(result_image);
                }
            }

            self.m_ready = true;
        }
    }
}

fn load_gltf_image(
    texture: &gltf::Texture<'_>,
    /*srgb: bool,
    generate_mips: bool,*/
    images: &Vec<gltf::image::Data>,
) -> Result<Image,&'static str>
{
    let mut ret_image = Image::new();

    let img = images[texture.source().index()].clone();
    ret_image.m_pixels = img.pixels;
    ret_image.m_width = img.width;
    ret_image.m_height = img.height;
    /*match img.format
    {
        R8 =>           {web_sys::console::log_1(&"Format::R8 ".into());},
        R8G8=>          {web_sys::console::log_1(&"Format::R8G8".into());},
        R8G8B8=>        {web_sys::console::log_1(&"Format::R8G8B8".into());},
        R8G8B8A8=>      {web_sys::console::log_1(&"Format::R8G8B8A8".into());},
        B8G8R8=>        {web_sys::console::log_1(&"Format::B8G8R8".into());},
        B8G8R8A8=>      {web_sys::console::log_1(&"Format::B8G8R8A8".into());},
        R16=>           {web_sys::console::log_1(&"Format::R16".into());},
        R16G16=>        {web_sys::console::log_1(&"Format::R16G16".into());},
        R16G16B16=>     {web_sys::console::log_1(&"Format::R16G16B16".into());},
        R16G16B16A16=>  {web_sys::console::log_1(&"Format::R16G16B16A16".into());},
    };*/  

    Ok(ret_image)
}

#[derive(Clone)]
pub struct GfxIndex(pub Option<u32>);

#[derive(Clone)]
pub struct Mesh
{
    pub m_name: String,
    pub m_transform: Mat4,
    pub m_primitives: Vec<Primitive>,
}

impl Mesh
{
    pub fn new() -> Self
    {
        Mesh{ m_name: String::new(), m_transform: Mat4::IDENTITY, m_primitives: Vec::new() }
    }   
}

#[derive(Clone)]
pub struct Image
{
    pub m_pixels: Vec<u8>,
    pub m_width: u32,
    pub m_height: u32,
}

impl Image
{
    pub fn new() -> Self
    {
        Image{ m_pixels: Vec::new(), m_width: 0, m_height: 0 }
    }
}

#[derive(Clone)]
pub struct Primitive
{
    pub m_indices: Vec<u32>,
    pub m_vertices : Vec<PosNormTangTexCol>,  
    pub m_material_id : GfxIndex,
    pub m_mode: PrimitiveMode,
}

impl mikktspace::Geometry for Primitive
{
    fn num_faces(&self) -> usize
    {
        self.m_indices.len() / 3
    }
    
    fn num_vertices_of_face(&self, _face: usize) -> usize
    {
        3
    }
    
    fn position(&self, face: usize, vert: usize) -> [f32; 3]
    {
        let index = face * 3 + vert;

        let vert_id = self.m_indices[index] as usize;
    
        let pos = self.m_vertices[vert_id].m_position;

        [pos.x,pos.y,pos.z]
    }
    
    fn normal(&self, face: usize, vert: usize) -> [f32; 3]
    {
        let index = face * 3 + vert;

        let vert_id = self.m_indices[index]as usize;
    
        let norm = self.m_vertices[vert_id].m_normal;

        [norm.x,norm.y,norm.z]
    }
    
    fn tex_coord(&self, face: usize, vert: usize) -> [f32; 2]
    {
        let index = face * 3 + vert;

        let vert_id = self.m_indices[index]as usize;
    
        let uv = self.m_vertices[vert_id].m_tex_coord;

        [uv.x,uv.y]
    }

    fn set_tangent_encoded(&mut self,tangent: [f32; 4],face: usize,vert: usize)
    {
        let index = face * 3 + vert;

        let vert_id = self.m_indices[index]as usize;
    
        self.m_vertices[vert_id].m_tangent =  Vec4::from(tangent);
    }
}

#[derive(Clone,PartialEq)]
pub enum AlphaFlag
{
    Opaque,
    Mask,
    Blend
}

#[derive(Clone)]
pub enum PrimitiveMode {
    Points,
    Lines,
    LineLoop,
    LineStrip,
    Triangles,
    TriangleStrip,
    TriangleFan,
}

#[derive(Clone)]
pub struct Material
{
    pub m_albedo_factor: Vec4,
    pub m_metallic_factor: f32,
    pub m_roughness_factor: f32,
    pub m_emissive_factor: Vec3,
    pub m_albedo_image_id: GfxIndex,
    pub m_metallic_roughness_image_id: GfxIndex,
    pub m_normal_image_id: GfxIndex,
    pub m_ao_image_id: GfxIndex,
    pub m_emissive_image_id: GfxIndex,
    pub m_double_sided: bool,
    pub m_alpha_factor: f32,
    pub m_alpha_flag: AlphaFlag,
}

pub fn load_gltf_mesh(
    mesh: &gltf::Mesh<'_>,
    buffers: &Vec<gltf::buffer::Data>,
) -> Result<Mesh, &'static str> 
{
    let mut ret_mesh = Mesh::new();
    web_sys::console::log_2(&"mesh.index(): ".into(),&(mesh.index()as u32).into());
    for primitive in mesh.primitives()
    {
        web_sys::console::log_2(&"primitive.index(): ".into(),&(primitive.index()as u32).into());
        let reader = primitive.reader(|buf_id| 
        {
            web_sys::console::log_2(&"buf_id.index(): ".into(),&(buf_id.index()as u32).into());
            if let Some(elem) = buffers.get(buf_id.index())
            {
                return Some(elem.0.as_slice());
            }
            None
        });

        let indices = reader
            .read_indices()
            .expect("Mesh primitive does not contain indices")
            .into_u32()
            .collect::<Vec<u32>>();

        let mut positions: Vec<Vec3> = Vec::new();
        if let Some(pos_list) = reader.read_positions()
        {
            for pos in pos_list
            {
                positions.push(Vec3::from(pos));
            }
        }
        let mut normals: Vec<Vec3> = Vec::new(); 
        if let Some(norm_list) = reader.read_normals()
        {
            for norm in norm_list
            {
                normals.push(Vec3::from(norm));
            }
        }
        let mut tangents: Vec<Vec4> = Vec::new(); 
        if let Some(tang_list) = reader.read_tangents()
        {
            for tang in tang_list
            {
                tangents.push(Vec4::from(tang));
            }
        }
        let mut colors: Vec<Vec4> = Vec::new();
        if let Some(col_enum) = reader.read_colors(0)
        {
            match col_enum
            {
                gltf::mesh::util::ReadColors::RgbU8(col_list) =>
                {
                    for col in col_list
                    {
                        colors.push((Vec3::new( (col[0] as f32) / (u8::MAX as f32),
                                                (col[1] as f32) / (u8::MAX as f32),
                                                (col[2] as f32) / (u8::MAX as f32))).extend(1.0));
                    }
                },
                gltf::mesh::util::ReadColors::RgbU16(col_list) =>
                {
                    for col in col_list
                    {
                        colors.push((Vec3::new( (col[0] as f32) / (u16::MAX as f32),
                                                (col[1] as f32) / (u16::MAX as f32),
                                                (col[2] as f32) / (u16::MAX as f32))).extend(1.0));
                    }
                },
                gltf::mesh::util::ReadColors::RgbF32(col_list) =>
                {
                    for col in col_list
                    {
                        colors.push(Vec3::from(col).extend(1.0));
                    }
                },
                gltf::mesh::util::ReadColors::RgbaU8(col_list) =>
                {
                    for col in col_list
                    {
                        colors.push(Vec4::new(  (col[0] as f32) / (u8::MAX as f32),
                                                (col[1] as f32) / (u8::MAX as f32),
                                                (col[2] as f32) / (u8::MAX as f32),
                                                (col[3] as f32) / (u8::MAX as f32)));
                    }
                },
                gltf::mesh::util::ReadColors::RgbaU16(col_list) =>
                {
                    for col in col_list
                    {
                        colors.push(Vec4::new(  (col[0] as f32) / (u16::MAX as f32),
                                                (col[1] as f32) / (u16::MAX as f32),
                                                (col[2] as f32) / (u16::MAX as f32),
                                                (col[3] as f32) / (u16::MAX as f32)));
                    }
                },
                gltf::mesh::util::ReadColors::RgbaF32(col_list) =>
                {
                    for col in col_list
                    {
                        colors.push(Vec4::from(col));
                    }
                },
            }
            
        }
        let mut uvs: Vec<Vec2> = Vec::new();
        if let Some(uv_enum) = reader.read_tex_coords(0)
        {
            match uv_enum
            {
                gltf::mesh::util::ReadTexCoords::U8(uv_list) =>
                {
                    for uv in uv_list
                    {
                        uvs.push(Vec2::new( (uv[0] as f32) / (u8::MAX as f32),
                                            (uv[1] as f32) / (u8::MAX as f32)));
                    }
                },
                gltf::mesh::util::ReadTexCoords::U16(uv_list) =>
                {
                    for uv in uv_list
                    {
                        uvs.push(Vec2::new(  (uv[0] as f32) / (u16::MAX as f32),
                                             (uv[1] as f32) / (u16::MAX as f32)));
                    }
                },
                gltf::mesh::util::ReadTexCoords::F32(uv_list) =>
                {
                    for uv in uv_list
                    {
                        uvs.push(Vec2::from(uv));
                    }
                },
            }
        }
        
        let mut vertices: Vec<PosNormTangTexCol> = Vec::new();
        
        for i in 0..positions.len()
        {
            vertices.push(PosNormTangTexCol{
                m_position: if positions.is_empty(){ Vec3::ZERO } else { positions[i] },
                m_normal: if normals.is_empty(){ Vec3::ONE } else { normals[i] },
                m_tangent: if tangents.is_empty(){ Vec4::ONE } else { tangents[i] },
                m_tex_coord: if uvs.is_empty(){ Vec2::ZERO } else { uvs[i] },
                m_color: if colors.is_empty(){ Vec4::ZERO } else { colors[i] },
            });
        }

        let mode = match primitive.mode()
        {
            gltf::mesh::Mode::Points => PrimitiveMode::Points,
            gltf::mesh::Mode::Lines => PrimitiveMode::Lines,
            gltf::mesh::Mode::LineLoop => PrimitiveMode::LineLoop,
            gltf::mesh::Mode::LineStrip => PrimitiveMode::LineStrip,
            gltf::mesh::Mode::Triangles => PrimitiveMode::Triangles,
            gltf::mesh::Mode::TriangleStrip => PrimitiveMode::TriangleStrip,
            gltf::mesh::Mode::TriangleFan => PrimitiveMode::TriangleFan,
        };

        let material = primitive.material();

        let mut mat_id = GfxIndex(None);

        if let Some(index) = material.index()
        {
            mat_id = GfxIndex(Some(index as u32));
        }

        let mut prim = Primitive
        {   m_indices: indices,
            m_vertices: vertices,
            m_material_id: mat_id,
            m_mode: mode,
        };

        if tangents.is_empty()
        {
            mikktspace::generate_tangents(&mut prim);
        }

        ret_mesh.m_primitives.push(prim);
        
        web_sys::console::log_1(&"ret_mesh.m_primitives created".into());
    }

    Ok(ret_mesh)
}

pub fn load_gltf_material(
    material: &gltf::Material<'_>,
    buffers: &Vec<gltf::buffer::Data>,
) -> Result<Material,&'static str> 
{
    let pbr_metal_rough = material.pbr_metallic_roughness();

    /*let state = ImageState {
        queue,
        stage: PipelineStage::FRAGMENT_SHADER,
        access: Access::SHADER_READ,
        layout: Layout::ShaderReadOnlyOptimal,
    };*/

    let mut albedo: Option<u32> = None;
    
    if let Some(alb) = pbr_metal_rough.base_color_texture()
    {
        albedo = Some(alb.texture().source().index() as u32);
    }

    let mut metallic_roughness: Option<u32> = None;
    if let Some(met_rou) = pbr_metal_rough.metallic_roughness_texture()
    {
        metallic_roughness = Some(met_rou.texture().source().index() as u32);
    }
    
    let mut normal: Option<u32> = None;
    if let Some(norm) = material.normal_texture()
    {
        normal = Some(norm.texture().source().index() as u32);
    }

    let mut ao: Option<u32> = None;
    if let Some(occ) = material.occlusion_texture()
    {
        ao = Some(occ.texture().source().index() as u32);
    }

    let mut emissive: Option<u32> = None; 
    
    if let Some(emissive_info) = material.emissive_texture() 
    {
        emissive = Some(emissive_info.texture().source().index() as u32);
    }

    let alpha_factor = material.alpha_cutoff();

    let alpha = match material.alpha_mode()
    {
        gltf::material::AlphaMode::Opaque => AlphaFlag::Opaque,
        gltf::material::AlphaMode::Mask => AlphaFlag::Mask,
        gltf::material::AlphaMode::Blend => AlphaFlag::Blend,
    };

    let albedo_ftr = pbr_metal_rough.base_color_factor();
        
    let emissive_ftr = material.emissive_factor();
    
    Ok(Material
    {
        m_albedo_factor: Vec4::new(albedo_ftr[0],albedo_ftr[1],albedo_ftr[2],albedo_ftr[3]),
        m_metallic_factor: pbr_metal_rough.metallic_factor(),
        m_roughness_factor: pbr_metal_rough.roughness_factor(),
        m_emissive_factor: Vec3::new(emissive_ftr[0],emissive_ftr[1],emissive_ftr[2]),
        m_albedo_image_id: GfxIndex(albedo),
        m_metallic_roughness_image_id: GfxIndex(metallic_roughness),
        m_normal_image_id: GfxIndex(normal),
        m_ao_image_id: GfxIndex(ao),
        m_emissive_image_id: GfxIndex(emissive),
        m_double_sided: material.double_sided(),
        m_alpha_factor: alpha_factor,
        m_alpha_flag: alpha,
    })
}
