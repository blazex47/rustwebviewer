use std::cell::{RefCell,RefMut};
use std::rc::Rc;
use std::sync::Arc;
use uuid::Uuid;
use std::any::Any;
use cgmath::*;
use kdtree_ray::{AABB, BoundingBox, KDtree};
use crate::engine::{Core, Chrono};
use glam::{Mat4, Vec3, Quat};
use std::collections::HashMap;
use crate::engine::{UniqueIDRegistry};
use web_sys::{WebGlBuffer,WebGlTexture};
use crate::engine::render::{RenderInfo};
use serde::{Serialize, Serializer,Deserialize,Deserializer};
use serde::ser::{SerializeMap, SerializeSeq};
use serde::de::{self, Visitor, MapAccess, SeqAccess, DeserializeSeed};
use erased_serde::{Serialize as ErasedSerialize, Deserializer as ErasedDeserializer};
use std::fmt::{self, Formatter};

pub trait Component : ErasedSerialize
{
    fn as_any(&mut self)-> &mut dyn Any;
    fn init(&mut self,core : &Option<Core>, uuid: &Uuid){}
    fn update(&mut self, _timer: &mut Chrono){}
    fn free(&mut self){}
}

erased_serde::serialize_trait_object!(Component);

#[derive(Clone)]
pub struct ComponentHandler 
{
    pub m_name: String,
    pub m_uuid : Uuid,
    //#[serde(serialize_with = "serialize_dyn_component")]
    pub m_component : Rc<RefCell<dyn Component>>,
}


fn serialize_dyn_component<S>(dyn_component:&Rc<RefCell<dyn Component>>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    erased_serde::serialize(&*dyn_component.borrow(), serializer)
}

impl Serialize for ComponentHandler
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut s = serializer.serialize_map(Some(2))?;
        s.serialize_entry("uuid", &self.m_uuid)?;
        s.serialize_entry(&self.m_name, &*self.m_component.borrow())?;
        s.end()
    }
}

struct ErasedComponent
{
    m_type_name: String,
}

impl<'de> DeserializeSeed<'de> for ErasedComponent
{
    type Value = Rc<RefCell<dyn Component>> ;

    fn deserialize<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
    where
        D: Deserializer<'de>,
    {
        web_sys::console::log_1(&"DeserializeSeed enter!".into());

        let mut result: Box<dyn ErasedDeserializer> = Box::new(ErasedDeserializer::erase(deserializer));

        web_sys::console::log_1(&"DeserializeSeed enter 2!".into());

        match self.m_type_name.as_str()
        {
            "rust_web_viewer::engine::component::TransformComponent"=>
            {
                if let Ok(component) = erased_serde::deserialize::<TransformComponent>(result.as_mut())
                {
                    web_sys::console::log_1(&"DeserializeSeed enter TransformComponent!".into());
        
                    return Ok(Rc::new(RefCell::new(component)))
                }
            },
            "rust_web_viewer::engine::component::MeshComponent"=>
            {
                if let Ok(component) = erased_serde::deserialize::<MeshComponent>(result.as_mut())
                {
                    web_sys::console::log_1(&"DeserializeSeed enter MeshComponent!".into());
        
                    return Ok(Rc::new(RefCell::new(component)))
                }
            },
            _=>
            {
                panic!("Component key not supported for deserialize!")
            }
        }

        web_sys::console::log_1(&"DeserializeSeed end!".into());

        Err(de::Error::custom("No deserialization for component type!"))
    }
}

pub struct ComponentHandlerSeed;

impl<'de> DeserializeSeed<'de> for ComponentHandlerSeed
{
    type Value = ComponentHandler ;

    fn deserialize<D>(self,deserializer: D) -> Result<Self::Value, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct ComponentHandlerVisitor;

        impl<'de> Visitor<'de> for ComponentHandlerVisitor {
            type Value = ComponentHandler;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result 
            {
                formatter.write_str("struct ComponentHandler")
            }

            fn visit_map<V>(self, mut map: V) -> Result<Self::Value, V::Error>
            where
                V: MapAccess<'de>,
            {
                web_sys::console::log_1(&"visit_map enter!".into());
                let mut name = None;
                let mut uuid = None;
                let mut component = None;
                while let Some(key) = map.next_key::<String>()? 
                {
                    if key.as_str() == "m_uuid"
                    {
                        if uuid.is_some() {
                            return Err(de::Error::duplicate_field("m_uuid"));
                        }
                        uuid = Some(map.next_value()?);
                    }
                    else
                    {
                        if name.is_some() {
                            return Err(de::Error::duplicate_field("m_name"));
                        }
                        name = Some(key.clone());
                        web_sys::console::log_1(&"component added begin!".into());
                        if component.is_some() {
                            return Err(de::Error::duplicate_field("m_component"));
                        }
                        web_sys::console::log_1(&"component added end!".into());
                        component = Some(map.next_value_seed(ErasedComponent{m_type_name:key.clone()})?);
                        web_sys::console::log_1(&"component added done!".into());
                    }
                }
                let name = name.ok_or_else(|| de::Error::missing_field("m_name"))?;
                let uuid = uuid.ok_or_else(|| de::Error::missing_field("m_uuid"))?;
                let component = component.ok_or_else(|| de::Error::missing_field("m_component"))?;
                Ok(ComponentHandler::new_2(&name,&uuid,component))
            }
        }

        const FIELDS: &'static [&'static str] = &["m_name", "m_uuid","m_component"];
        deserializer.deserialize_struct("ComponentHandler", FIELDS, ComponentHandlerVisitor)
    }
}

impl ComponentHandler
{
    pub fn new<T: Component + 'static>(name : &String,uuid: &Uuid, component : T) -> ComponentHandler
    {
        let component_wrapper = Rc::new(RefCell::new(component));

        ComponentHandler{ m_name: name.clone(), m_uuid : uuid.clone(),m_component: component_wrapper as Rc<RefCell<dyn Component>>}
    }

    pub fn new_2(name : &String,uuid: &Uuid, component : Rc<RefCell<dyn Component>>) -> ComponentHandler
    {
        ComponentHandler{ m_name: name.clone(), m_uuid : uuid.clone(),m_component: component as Rc<RefCell<dyn Component>>}
    }

    pub fn get_component<T: Component + 'static>(&mut self) -> RefMut<T>
    {
        let borrow = self.m_component.borrow_mut();

        RefMut::map(borrow, |inner| 
        {
            inner.as_any().downcast_mut::<T>().expect("Type cannot be found!")
        })
    }

    pub fn get_base(&mut self) -> Rc<RefCell<dyn Component>>
    {
        self.m_component.clone()
    }
}

pub struct ComponentFactory
{
    m_core : Option<Core>,
    m_look_up : HashMap<String,Vec<ComponentHandler>>
}

impl Default for ComponentFactory
{
    fn default() -> Self
    {
        ComponentFactory::new()
    }
}

impl ComponentFactory
{
    pub fn new() -> ComponentFactory
    {
        let look_up : HashMap<String,Vec<ComponentHandler>> = HashMap::new();

        ComponentFactory{m_core : None,m_look_up: look_up}
    }

    pub fn init(&mut self,core : &Option<Core>)
    {
        web_sys::console::log_1(&"ComponentFactory init".into());

        self.m_core = core.clone();
    }

    pub fn generate<T : Component + 'static>(&mut self,component: T) -> ComponentHandler
    {
        let name = std::any::type_name::<T>();

        web_sys::console::log_2(&"generate: ".into(), &name.into());
    
        let result_uuid = self.m_core.as_ref().unwrap().m_registry.borrow_mut().generate(&name.to_string());

        web_sys::console::log_1(&"after uuid generate".into());

        let mut result = ComponentHandler::new::<T>(&name.to_string(),&result_uuid,component);

        result.get_base().borrow_mut().init(&self.m_core,&result_uuid);

        match self.m_look_up.get_mut(name)
        {
            Some(component_list) =>
            {
                component_list.push(result.clone());
            },
            None => 
            {
                self.m_look_up.insert(name.to_string(),vec![result.clone()]); 
            }
        }

        result.clone()
    }

    pub fn delete(&mut self,name: &String, uuid: &Uuid)
    {
        match self.m_look_up.get_mut(name)
        {
            Some(component_list) =>
            {
                for i in 0..component_list.len()
                {
                    if component_list[i].m_uuid == *uuid
                    {
                        let mut comp = component_list.remove(i);

                        comp.get_base().borrow_mut().free();

                        self.m_core.as_ref().unwrap().m_registry.borrow_mut().delete(uuid);
                    }
                    break;
                }
            },
            None => 
            {
                panic!("No component found!");
            }
        }
    }

    pub fn update(&mut self, timer: &mut Chrono)
    {
        for (_key, value) in &self.m_look_up
        {
            //web_sys::console::log_2(&"update: ".into(), &key.into());

            for elem in value
            {
                elem.m_component.borrow_mut().update(timer);
            }
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct TransformComponent 
{
    m_uuid: Uuid,
    #[serde(serialize_with = "serialize_parent_transform")]
    #[serde(deserialize_with = "deserialize_parent_transform")]
    m_parent: (Uuid,Option<ComponentHandler>),
    #[serde(serialize_with = "serialize_children_transform")]
    #[serde(deserialize_with = "deserialize_children_transform")]
    m_children: Vec<(Uuid,Option<ComponentHandler>)>,
    m_mtw_matrix: Mat4,
    pub m_rotation: Quat,
    pub m_position: Vec3,
    pub m_scale: Vec3,
}

fn serialize_parent_transform<S>(parent:&(Uuid,Option<ComponentHandler>), serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    serializer.serialize_newtype_struct("Uuid", &parent.0)
}

fn deserialize_parent_transform<'de, D>(deserializer: D) -> Result<(Uuid,Option<ComponentHandler>), D::Error> 
where D: Deserializer<'de>
{
    struct ParentTransformVisitor;

    impl<'de> Visitor<'de> for ParentTransformVisitor {
        type Value = (Uuid,Option<ComponentHandler>);

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result 
        {
            formatter.write_str("tuple (Uuid,Option<ComponentHandler>)")
        }

        fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
        where
        E: de::Error,
        { 
           
            Ok((Uuid::parse_str(v).unwrap(),None))
        }
    }

    deserializer.deserialize_str(ParentTransformVisitor)
}

fn serialize_children_transform<S>(children:&Vec<(Uuid,Option<ComponentHandler>)>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let mut seq = serializer.serialize_seq(Some(children.len()))?;
    for (k, _) in children {
        seq.serialize_element(k)?;
    }
    seq.end()
}

fn deserialize_children_transform<'de, D>(deserializer: D) -> Result<Vec<(Uuid,Option<ComponentHandler>)>, D::Error> 
where D: Deserializer<'de>
{
    struct ChildrenTranformVisitor;

    impl<'de> Visitor<'de> for ChildrenTranformVisitor {
        type Value = Vec<(Uuid,Option<ComponentHandler>)>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result 
        {
            formatter.write_str("vec Vec<(Uuid,Option<ComponentHandler>)>")
        }

        fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
        where
            A: SeqAccess<'de>, 
        {
            web_sys::console::log_1(&"visit_seq enter!".into());
            let mut arr = Vec::new();
            while let Some(uuid_str) = seq.next_element::<&str>()? 
            {
                arr.push((Uuid::parse_str(uuid_str).unwrap(),None));
            }
            Ok(arr)
        }
    }

    deserializer.deserialize_seq(ChildrenTranformVisitor)
}


impl TransformComponent
{
    pub fn new() -> TransformComponent
    {
        let children = Vec::new();

        TransformComponent  { m_uuid: Uuid::nil()
                            , m_parent : (Uuid::nil(),None)
                            , m_children : children
                            , m_mtw_matrix : Mat4::IDENTITY
                            , m_rotation : Quat::IDENTITY
                            , m_position : Vec3::new(0.0,0.0,0.0)
                            , m_scale :  Vec3::new(1.0,1.0,1.0)}
    }

    pub fn get_local_transform(&mut self) -> Mat4
    {
        self.m_mtw_matrix = Mat4::from_scale_rotation_translation(self.m_scale,self.m_rotation,self.m_position);

        self.m_mtw_matrix
    }

    pub fn get_global_transform(&mut self) -> Mat4
    {
        if let (_,Some(mut trans_comp)) = self.m_parent.clone()
        {
            return trans_comp.get_component::<TransformComponent>().get_global_transform() * self.get_local_transform();
        }

        self.get_local_transform()
    }

    pub fn set_local_transform(&mut self, local_mtx: &Mat4)
    {
        let (scale,rotate,position) = local_mtx.to_scale_rotation_translation();
        self.m_mtw_matrix = local_mtx.clone();
        self.m_rotation = rotate;
        self.m_position = position;
        self.m_scale = scale;

    }

    pub fn set_parent_transform(&mut self, handle: Option<ComponentHandler>)
    {
        let parent = handle.clone();

        if let Some(temp) = &parent
        {
            self.m_parent = (temp.m_uuid,parent);
        }
    }

    pub fn get_parent_transform(&mut self) -> Option<ComponentHandler>
    {
        self.m_parent.1.clone()
    }

    pub fn remove_parent_transform(&mut self) -> Option<ComponentHandler>
    {
        let result = self.get_parent_transform();

        self.set_parent_transform(None);

        result
    }

    pub fn add_child_transform(&mut self, handle: Option<ComponentHandler>)
    {
        if let Some(comp_handle) = handle.clone()
        {
            for elem in &self.m_children
            {
                if elem.0 == comp_handle.m_uuid
                {
                    return;
                }
            }

            self.m_children.push((comp_handle.m_uuid.clone(),handle.clone()));
        }
    }

    pub fn get_child_transform(&mut self, uuid: &Uuid)->Option<ComponentHandler>
    {

        for i in 0..self.m_children.len()
        {
            if self.m_children[i].0 == *uuid
            {
                return self.m_children[i].1.clone()
            }
        }

        None
    }

    pub fn remove_child_transform(&mut self, uuid: &Uuid)->Option<ComponentHandler>
    {

        for i in 0..self.m_children.len()
        {
            if self.m_children[i].0 == *uuid
            {
                return self.m_children.remove(i).1
            }
        }

        None
    }
}

impl Component for TransformComponent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }

    fn init(&mut self,core : &Option<Core>, uuid: &Uuid)
    {
        self.m_uuid = uuid.clone();
    }

    fn update(&mut self, _timer: &mut Chrono)
    {

    }

    fn free(&mut self)
    {
        if let Some(mut parent) = self.remove_parent_transform()
        {
            parent.get_component::<TransformComponent>().remove_child_transform(&self.m_uuid);
        }

        for child in self.m_children.clone()
        {
            if let Some(mut temp) = child.1
            {
                temp.get_component::<TransformComponent>().remove_parent_transform();
            }
        }

        self.m_children.clear();
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct MeshComponent
{
    /*#[serde(skip)]
    pub m_vbo: Option<WebGlBuffer>,
    #[serde(skip)]
    pub m_ibo: Option<WebGlBuffer>*/
    pub m_opacity: f32,
    #[serde(skip)]
    pub m_bo_list: Vec<RenderInfo>,
    #[serde(skip)]
    m_core: Option<Core>,
}

impl MeshComponent
{
    /*pub fn new(vbo: &Option<WebGlBuffer>,ibo: &Option<WebGlBuffer>) -> MeshComponent
    {
       MeshComponent{m_vbo : vbo.clone(), m_ibo : ibo.clone()}
    }*/
    pub fn new(bo_list: &Vec<RenderInfo>) -> MeshComponent
    {
       MeshComponent{m_opacity: 1.0,m_core : None, m_bo_list : bo_list.clone()}
    }
}

impl Component for MeshComponent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }

    fn init(&mut self,core : &Option<Core>, uuid: &Uuid)
    {
        self.m_core = core.clone();
    }

    fn update(&mut self, _timer: &mut Chrono)
    {

    }
}

#[derive(Debug,Clone, Serialize, Deserialize)]
pub enum CollisionInfo
{
    Triangle{v0:Vec3,v1:Vec3,v2:Vec3},
    Entity(String),
}

#[derive(Debug,Clone, Serialize, Deserialize)]
pub struct Collider
{
    pub m_min: Vec3,
    pub m_max: Vec3,
    pub m_node_index: usize,
    pub m_info: CollisionInfo,
}

impl Collider
{
    pub fn new(min: Vec3, max: Vec3, info: CollisionInfo) -> Self
    {
        Collider{m_min: min, m_max: max,m_node_index: 0, m_info: info}
    }
}

impl BoundingBox for Collider 
{
    fn bounding_box(&self) -> AABB 
    {
        let min = Vector3::new(self.m_min.x,self.m_min.y,self.m_min.z);

        let max = Vector3::new(self.m_max.x,self.m_max.y,self.m_max.z);

        [min, max]
    }
}

#[derive(Serialize, Deserialize)]
pub struct CollisionComponent
{
    pub m_min: Vec3,
    pub m_max: Vec3,
    #[serde(serialize_with = "serialize_kdtree")]
    #[serde(deserialize_with = "deserialize_kdtree")]
    m_kdtree: KDtree<Collider>,
}

fn serialize_kdtree<S>(parent:&KDtree<Collider>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let mut seq = serializer.serialize_seq(None)?;
    seq.end()
}

fn deserialize_kdtree<'de, D>(deserializer: D) -> Result<KDtree<Collider>, D::Error> 
where D: Deserializer<'de>
{
    struct KDtreeVisitor;

    impl<'de> Visitor<'de> for KDtreeVisitor {
        type Value = KDtree<Collider>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result 
        {
            formatter.write_str("tuple KDtree")
        }

        fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
        where
        E: de::Error,
        { 
           
            Ok(KDtree::new(Vec::new()))
        }
    }

    deserializer.deserialize_str(KDtreeVisitor)
}

pub fn collide<'a>(collider :&'a Collider, ray_org: &Vec3, ray_dir: &Vec3) -> f32
{
    let mut tmin = f32::MAX;

    match collider.m_info
    {
        CollisionInfo::Triangle{v0,v1,v2} =>
        {
            let vec01 = v1 - v0;
            let vec02 = v2 - v0;
            let normal = vec01.cross(vec02).normalize();
            let denom = ray_dir.dot(normal);

            if denom < 0.0
            {
                let t = normal.dot(v0 - *ray_org) / denom;
                let ray_int = (*ray_org) + t * (*ray_dir);

                let vec12 = v2 - v1;
                let vec20 = v0 - v2;

                let int01 = vec01.cross(ray_int - v0).dot(normal);
                let int12 = vec12.cross(ray_int - v1).dot(normal);
                let int20 = vec20.cross(ray_int - v2).dot(normal);

                if int01 * int12 * int20 > 0.0
                {
                    tmin = t;
                }
            }
        },
        CollisionInfo::Entity(_) =>
        {
            if ray_dir.x.abs() > f32::EPSILON
            {
                let tx1 = (collider.m_min.x - ray_org.x) / ray_dir.x;
                let tx2 = (collider.m_max.x - ray_org.x) / ray_dir.x;
                tmin = tmin.min(tx1).min(tx2);
            }
        
            if ray_dir.y.abs() > f32::EPSILON
            {
                let tx1 = (collider.m_min.y - ray_org.y) / ray_dir.y;
                let tx2 = (collider.m_max.y - ray_org.y) / ray_dir.y;
                tmin = tmin.min(tx1).min(tx2);
            }
        
            if ray_dir.z.abs() > f32::EPSILON
            {
                let tx1 = (collider.m_min.z - ray_org.z) / ray_dir.z;
                let tx2 = (collider.m_max.z - ray_org.z) / ray_dir.z;
                tmin = tmin.min(tx1).min(tx2);
            }
        },
    }

    tmin
}

pub fn collide_list(colliders :&Vec<Arc<Collider>>, ray_org: &Vec3, ray_dir: &Vec3) -> Option<Arc<Collider>>
{
    let mut result: Option<Arc<Collider>> = None;
    let mut overall_min = f32::MAX;

    for collider in colliders
    {
        let tmin = collide(collider,ray_org,ray_dir);

        if tmin < overall_min
        {
            overall_min = tmin;
            result = Some(collider.clone());
        }
    }

    result
}

impl CollisionComponent
{
    pub fn new(min: Vec3, max: Vec3, colliders: &Vec<Collider>) -> CollisionComponent
    {
        let kdtree = KDtree::new(colliders.clone());

        CollisionComponent{ m_min: min, m_max: max,m_kdtree: kdtree}
    }

    pub fn rebuild(&mut self,min: Vec3, max: Vec3, colliders: &Vec<Collider>)
    {
        self.m_min = min;

        self.m_max = max;

        self.m_kdtree = KDtree::new(colliders.clone());
    }

    pub fn intersect_ray(&mut self, ray_org: &Vec3, ray_dir: &Vec3) -> Option<Arc<Collider>>
    {
        let ray_origin = Vector3::new(ray_org.x,ray_org.y,ray_org.z);

        let ray_direction = Vector3::new(ray_dir.x,ray_dir.y,ray_dir.z);

        let colliders = self.m_kdtree.intersect(&ray_origin, &ray_direction);

        collide_list(&colliders,ray_org,ray_dir)
    }
}

impl Component for CollisionComponent
{
    fn as_any(&mut self)-> &mut dyn Any
    {
        self
    }
}