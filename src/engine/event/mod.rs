use std::collections::HashMap;
use std::cell::{RefCell,RefMut};
use std::any::Any;
use std::rc::Rc;
use uuid::Uuid;
use crate::engine::{Core, UniqueIDRegistry};

pub trait Event
{
    fn as_any(&mut self)-> &mut dyn Any;
}

#[derive(Clone)]
pub struct EventHandler
{
    pub m_name: String,
    pub m_network: Vec<String>,
    pub m_event: Rc<RefCell<dyn Event>>,
}

impl EventHandler
{
    pub fn new<T: Event + 'static>(network: Vec<String>, event : T) -> EventHandler
    {
        let name = std::any::type_name::<T>();
        let event_wrapper = Rc::new(RefCell::new(event));

        EventHandler{ m_name: name.to_string(),m_network: network, m_event: event_wrapper as Rc<RefCell<dyn Event>>}
    }

    pub fn get_event<T: Event + 'static>(&mut self) -> RefMut<T>
    {
        let borrow = self.m_event.borrow_mut();

        RefMut::map(borrow, |inner| 
        {
            inner.as_any().downcast_mut::<T>().expect("Type cannot be found!")
        })
    }
}

type EventCallBack = Box<dyn FnMut(&mut EventHandler) -> Vec<EventHandler>>;

pub struct EventMessager 
{
    m_network_map: HashMap<String, HashMap<String, Vec<Uuid>>>,
    m_func_map : HashMap<Uuid,EventCallBack>,
}

impl EventMessager 
{
    pub fn new() -> EventMessager
    {
        EventMessager{m_network_map: HashMap::new(), m_func_map : HashMap::new()}
    }

    fn subscribe(&mut self, network: &String, name: &String, uuid : &Uuid, callback: EventCallBack)
    {
        match self.m_network_map.get_mut(name)
        {
            Some(subnet_map) =>
            {
                match subnet_map.get_mut(network)
                {
                    Some(func_list) =>
                    {
                        func_list.push(uuid.clone());
                    },
                    None =>
                    {
                        subnet_map.insert(network.clone(),vec![uuid.clone()]);
                    }
                }
            },
            None => 
            {
                let mut subnet_map = HashMap::new();
                subnet_map.insert(network.clone(),vec![uuid.clone()]);
                self.m_network_map.insert(name.clone(),subnet_map);
            }
        }
        self.m_func_map.insert(uuid.clone(),callback);
    }

    fn unsubscribe(&mut self, network: &String, name: &String, uuid : &Uuid)
    {
        match self.m_network_map.get_mut(name)
        {
            Some(subnet_map) =>
            {
                match subnet_map.get_mut(network)
                {
                    Some(func_list) =>
                    {
                        for i in 0..func_list.len()
                        {
                            if func_list[i] == *uuid
                            {
                                func_list.remove(i);
                                self.m_func_map.remove(uuid);
                                break;
                            }
                        }
                    },
                    None =>
                    {
                        panic!("No event callback to unsubscribe!")
                    }
                }

            },
            None => 
            {
                panic!("No event callback to unsubscribe!")
            }
        }
    }

    pub fn invoke(&mut self, event: &mut EventHandler)
    {
        let mut event_list = vec![event.clone()];

        while let Some(mut result) = event_list.pop()
        {
            //web_sys::console::log_2(&"trigger: ".into(), &result.m_name.clone().into());

            match self.m_network_map.get_mut(&result.m_name)
            {
                Some(subnet_map) =>
                {
                    if result.m_network.is_empty()
                    {
                        for (_, func_list) in subnet_map
                        {
                            for func_id in func_list
                            {
                                if let Some(func) = self.m_func_map.get_mut(func_id)
                                {
                                    for event_handle in (*func)(&mut result)
                                    {
                                        event_list.push(event_handle);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        let networks = result.m_network.clone();
                        for network in &networks
                        {
                            if let Some(func_list) = subnet_map.get_mut(network)
                            {
                                for func_id in func_list
                                {
                                    if let Some(func) = self.m_func_map.get_mut(func_id)
                                    {
                                        for event_handle in (*func)(&mut result)
                                        {
                                            event_list.push(event_handle);
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                None => 
                {
                    panic!("Error: Event: {} not supported!", &result.m_name);
                }
            }
        }
    }
}

pub struct EventListener
{
    m_core : Option<Core>,
    m_look_up : HashMap<String,Uuid>,
}

impl Default for EventListener
{
    fn default()-> Self
    {
        EventListener::new()
    }
}

impl EventListener
{
    pub fn new() -> EventListener
    {
        let look_up  = HashMap::new();

        EventListener{m_core: None, m_look_up: look_up}
    }

    pub fn init(&mut self,core : &Option<Core>)
    {
        self.m_core = core.clone();
    }

    pub fn register<T: Event>(&mut self, network: &String, callback: EventCallBack)
    {
        let name = std::any::type_name::<T>();

        web_sys::console::log_2(&"register: ".into(), &name.into());

        if !self.m_look_up.contains_key(name)
        {
            let uuid = Uuid::new_v4();

            self.m_look_up.insert(name.to_string(),uuid.clone());
    
            self.m_core.as_ref().unwrap().m_messager.borrow_mut().subscribe(&network,&name.to_string(),&uuid,callback);
        }
        else
        {
            panic!("Event had been registered already!");
        }
    }

    pub fn deregister<T: Event>(&mut self,network: &String)
    {
        let name = std::any::type_name::<T>();

        web_sys::console::log_2(&"deregister: ".into(), &name.into());

        match self.m_look_up.get_mut(name)
        {
            Some(uuid) =>
            {
                self.m_core.as_ref().unwrap().m_messager.borrow_mut().unsubscribe(&network,&name.to_string(),&uuid);

                self.m_look_up.remove(name);
            },
            None => 
            {
                panic!("Event had not even been registered!");
            }
        }        
    }
}