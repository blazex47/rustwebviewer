use std::boxed::Box;
use std::cell::RefCell;
use std::rc::Rc;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{WebGlRenderingContext,WebGlBuffer};
use render::{RenderManager};
use world::{WorldManager};
use input::{InputManager};
use event::{EventMessager, EventListener, EventHandler};
use component::{TransformComponent};
use resource::{Resource,ResourceRequestEvent,AssetRequestEvent,AssetRetrieveEvent};
use crate::system::*;
use std::collections::HashMap;
use uuid::Uuid;
use serde::{Serialize, Deserialize};
use crate::*;

pub mod resource;
pub mod input;
pub mod event;
pub mod statemachine;
pub mod render;
pub mod world;
pub mod component;
pub mod entity;

#[derive(Debug)]
pub struct UniqueIDRegistry
{
    m_registry: HashMap<Uuid,String>,
}

impl UniqueIDRegistry
{
    fn new() -> UniqueIDRegistry
    {
        let registry: HashMap<Uuid,String> = HashMap::new();

        UniqueIDRegistry{ m_registry: registry}
    }

    fn generate(&mut self, type_name: &String) -> Uuid
    {
        let mut uuid = Uuid::new_v4();
        
        while self.m_registry.contains_key(&uuid)
        {
            uuid = Uuid::new_v4();
        }

        self.m_registry.insert(uuid.clone(),type_name.clone());

        uuid.clone()
    }

    fn delete(&mut self, uuid: &Uuid)
    {
        self.m_registry.remove(uuid);
    }
}


#[derive(Clone)]
pub struct Core
{
    pub m_canvas: Rc<web_sys::HtmlCanvasElement>,
    pub m_context: Rc<RefCell<web_sys::WebGlRenderingContext>>,
    pub m_registry: Rc<RefCell<UniqueIDRegistry>>,
    pub m_messager: Rc<RefCell<EventMessager>>,
}

impl Core
{
    pub fn new()-> Core
    {

        let canvas = Rc::new(web_api::canvas());

        let ctx_options = js_sys::Object::new();
        js_sys::Reflect::set(&ctx_options, &"stencil".into(), &true.into()).unwrap();

        let webgl_context = canvas.get_context_with_context_options("webgl",&ctx_options).expect("Failed to get context webgl!");

        let js_context = webgl_context.expect("Failed to load js contect webgl!");

        let context = Rc::new(RefCell::new(js_context.dyn_into::<WebGlRenderingContext>().expect("Failed ot dynamic cast conext to WebGlRenderingContext!")));

        let registry = Rc::new(RefCell::new(UniqueIDRegistry::new()));

        let messager = Rc::new(RefCell::new(EventMessager::new()));

        Core{m_canvas: canvas, m_context : context, m_registry: registry, m_messager : messager}
    }
}


