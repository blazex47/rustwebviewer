use std::cell::{RefCell};
use std::rc::Rc;
use glam::{Vec3, Mat4, Quat};
use std::hash::Hash;
use std::collections::HashMap;
use crate::engine::component::{TransformComponent, MeshComponent, CollisionComponent, Collider, CollisionInfo};
use crate::engine::component::*;
use crate::engine::entity::{Entity,EntityHandler,EntityAllocator};
use crate::engine::statemachine::{WorldEvent,StateMachine,State, TestState1, TestState2};
use crate::engine::render::{BufferObjEvent, BufferObjInfo, RenderEvent};
use crate::engine::{Core, UniqueIDRegistry};
use crate::engine::input::{MouseEvent};
use crate::engine::event::{Event, EventHandler, EventListener};
use crate::engine::{Chrono};
use serde::{Serialize,Serializer,Deserialize,Deserializer};
use serde::ser::{SerializeStruct};
use serde::de::{self, Visitor,MapAccess};
use std::fmt::{self, Formatter};
use erased_serde::{Serialize as ErasedSerialize};

#[derive(Serialize,Deserialize)]
pub struct EntityComponenetSystem
{
    #[serde(skip)]
    pub m_core : Option<Core>,
    #[serde(skip)]
    m_listener: EventListener,
    #[serde(skip)]
    m_component_factory: Rc<RefCell<ComponentFactory>>,
    #[serde(skip)]
    m_entity_allocator: Rc<RefCell<EntityAllocator>>,
}

impl EntityComponenetSystem
{
    pub fn new() -> EntityComponenetSystem
    {
        let component_factory = Rc::new(RefCell::new(ComponentFactory::new()));

        let entity_allocator = Rc::new(RefCell::new(EntityAllocator::new()));

        let listener = EventListener::new();

        EntityComponenetSystem{ m_core: None, m_listener : listener,m_component_factory: component_factory,m_entity_allocator: entity_allocator }
    }

    pub fn add_entity(&mut self) -> Option<EntityHandler>
    {
        let uuid = self.m_core.as_ref().unwrap().m_registry.borrow_mut().generate(&std::any::type_name::<Entity>().to_string());

        self.m_entity_allocator.borrow_mut().generate(&uuid.to_string(), &uuid)
    }

    pub fn add_entity_by_name(&mut self, name: &String) -> Option<EntityHandler>
    {
        let uuid = self.m_core.as_ref().unwrap().m_registry.borrow_mut().generate(&std::any::type_name::<Entity>().to_string());

        self.m_entity_allocator.borrow_mut().generate(name,&uuid)
    }

    pub fn get_entity_by_name(&mut self, name : &String) -> Option<EntityHandler>
    {
        self.m_entity_allocator.borrow_mut().find(name)
    }

    pub fn get_all_entities(&mut self) -> Vec<EntityHandler>
    {
        self.m_entity_allocator.borrow_mut().get_all_entities()
    }

    pub fn remove_entity_by_name(&mut self, name : &String)
    {
        let uuid = self.m_entity_allocator.borrow_mut().delete_by_name(name);

        self.m_core.as_ref().unwrap().m_registry.borrow_mut().delete(&uuid);
    }

    pub fn remove_all_entities(&mut self)
    {
        let uuid_list = self.m_entity_allocator.borrow_mut().delete_all_entities();

        for uuid in uuid_list
        {
            self.m_core.as_ref().unwrap().m_registry.borrow_mut().delete(&uuid);
        }
    }

    pub fn init(&mut self,core: &Option<Core>)
    {
        self.m_core = core.clone();

        self.m_listener.init(&self.m_core);

        let registry = self.m_core.as_ref().unwrap().m_registry.clone();

        self.m_entity_allocator.borrow_mut().init(&self.m_component_factory);

        let entity_allocator = self.m_entity_allocator.clone();

        self.m_listener.register::<BufferObjEvent>
        (&String::from("BufferObj1"),Box::new(move |handle:&mut EventHandler| ->Vec<EventHandler>
        {
            let buf_obj_msg = handle.get_event::<BufferObjEvent>();

            let mut lookup_map: HashMap<String,String> = HashMap::new();

            for (name, (trans,bo_list)) in &buf_obj_msg.m_renderinfo_map
            {
                let uuid = registry.borrow_mut().generate(&std::any::type_name::<Entity>().to_string());

                lookup_map.insert(name.clone(),uuid.to_string());

                if let Some(entity) = entity_allocator.borrow_mut().generate(&uuid.to_string(),&uuid)
                {
                    if let Some(mut trans_comp) = entity.get_entity().add_component::<TransformComponent>(TransformComponent::new())
                    {
                        trans_comp.set_local_transform(&trans);
                    }
                    
                    entity.get_entity().add_component::<MeshComponent>(MeshComponent::new(&bo_list));
                }
            }

            match &buf_obj_msg.m_bufferinfo
            {
                BufferObjInfo::PBR(model) =>
                {
                    let meshes = &model.m_meshes;

                    for i in 0..meshes.len()
                    {
                        let mesh = &meshes[i];

                        if let Some(name) = &lookup_map.get_mut(&i.to_string())
                        {
                            if let Some(entity) = entity_allocator.borrow_mut().find(&name) 
                            {
                                let mut colliders = Vec::new();
            
                                let mut aabb_min = Vec3::new(f32::MAX,f32::MAX,f32::MAX);
                                let mut aabb_max = Vec3::new(f32::MIN,f32::MIN,f32::MIN);
                
                                if let Some(mut transform_comp) = entity.get_entity().get_component::<TransformComponent>()
                                {    
                                    let mtw_mtx = transform_comp.get_global_transform();
                
                                    for primitive in &mesh.m_primitives
                                    {
                                        let i_len = primitive.m_indices.len();
                    
                                        for i in (0..i_len).step_by(3)
                                        {
                                            let index0 = primitive.m_indices[i] as usize;
                                            let index1 = primitive.m_indices[i + 1] as usize;
                                            let index2 = primitive.m_indices[i + 2] as usize;
                                        
                                            let vertex0 = mtw_mtx * primitive.m_vertices[index0].m_position.extend(1.0);
                                            let vertex1 = mtw_mtx * primitive.m_vertices[index1].m_position.extend(1.0);
                                            let vertex2 = mtw_mtx * primitive.m_vertices[index2].m_position.extend(1.0);
                    
                                            let min = Vec3::new(
                                                vertex0.x.min(vertex1.x).min(vertex2.x),
                                                vertex0.y.min(vertex1.y).min(vertex2.y),
                                                vertex0.z.min(vertex1.z).min(vertex2.z),
                                            );
                                            let max = Vec3::new(
                                                vertex0.x.max(vertex1.x).max(vertex2.x),
                                                vertex0.y.max(vertex1.y).max(vertex2.y),
                                                vertex0.z.max(vertex1.z).max(vertex2.z),
                                            );
                    
                                            aabb_min = Vec3::new(
                                                aabb_min.x.min(min.x),
                                                aabb_min.y.min(min.y),
                                                aabb_min.z.min(min.z),
                                            );
                                            aabb_max = Vec3::new(
                                                aabb_max.x.max(max.x),
                                                aabb_max.y.max(max.y),
                                                aabb_max.z.max(max.z),
                                            );
                    
                                            colliders.push(Collider::new(min,max,CollisionInfo::Triangle{v0: vertex0.truncate(),v1: vertex1.truncate(),v2: vertex2.truncate()}));
                                        }
                                    }
                                }
                
                                entity.get_entity().add_component::<CollisionComponent>(CollisionComponent::new(aabb_min,aabb_max,&colliders));
                            } 
                        }       
                    }
                },
                BufferObjInfo::Flat(_) =>
                {

                }
            }

            Vec::new()
        }));

        self.m_component_factory.borrow_mut().init(&self.m_core);

        self.m_entity_allocator.borrow_mut().init(&self.m_component_factory);
    }

    pub fn update(&mut self, timer: &mut Chrono)
    {
        self.m_component_factory.borrow_mut().update(timer);
    }

    pub fn free(&mut self)
    {

    }
}

#[derive(Serialize, Deserialize)]
pub struct World
{
    pub m_name: String,
    m_ecs: EntityComponenetSystem,
    //#[serde(serialize_with = "serialize_statemachine")]
    m_statemachine: StateMachine,
}

/*fn serialize_statemachine<S>(statemachine: &Rc<RefCell<StateMachine>>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    (*statemachine.borrow()).serialize(serializer)
}

impl<'de> Deserialize<'de> for World
{
    fn deserialize<D>(deserializer: D) -> Result<World, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct WorldVisitor;

        impl<'de> Visitor<'de> for WorldVisitor {
            type Value = World;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result 
            {
                web_sys::console::log_1(&"expecting struct World!".into());
                formatter.write_str("struct World")
            }

            fn visit_map<V>(self, mut map: V) -> Result<Self::Value, V::Error>
            where
                V: MapAccess<'de>,
            {
                web_sys::console::log_1(&"visit_map enter!".into());
                let mut ecs = None;
                let mut statemachine = None;
                while let Some(key) = map.next_key::<String>()? {
                    match key.as_str() {
                        "m_ecs" => {
                            web_sys::console::log_1(&"ecs added begin!".into());
                            if ecs.is_some() {
                                return Err(de::Error::duplicate_field("m_ecs"));
                            }
                            web_sys::console::log_1(&"ecs added end!".into());
                            ecs = Some(map.next_value::<EntityComponenetSystem>()?);
                            web_sys::console::log_1(&"ecs added successful!".into());
                        }
                        "m_statemachine" => {
                            web_sys::console::log_1(&"statemachine added begin!".into());
                            if statemachine.is_some() {
                                return Err(de::Error::duplicate_field("m_statemachine"));
                            }
                            statemachine = Some(map.next_value::<StateMachine>()?);
                            web_sys::console::log_1(&"statemachine added successful!".into());
                        }
                        _=>
                        {
                            panic!("World key do not work!")
                        }
                    }
                }
                web_sys::console::log_1(&"I am done!".into());
                let ecs = ecs.ok_or_else(|| de::Error::missing_field("m_ecs"))?;
                let statemachine = statemachine.ok_or_else(|| de::Error::missing_field("m_statemachine"))?;
                Ok(World::new_2(ecs,statemachine))
            }
        }

        web_sys::console::log_1(&"Deserialize deserialize World!".into());
        const FIELDS: &'static [&'static str] = &["m_ecs", "m_statemachine"];
        deserializer.deserialize_struct("World", FIELDS, WorldVisitor)
    }
}*/

impl World
{
    fn new(name: &String) -> World
    {
        let ecs = EntityComponenetSystem::new();
        let mut statemachine = StateMachine::new();

        statemachine.add_state(&String::from("STATE1"),TestState1::new());
        statemachine.add_state(&String::from("STATE2"),TestState2::new());
        statemachine.default_state(&String::from("STATE1"));
        
        World{ m_name: name.clone(),m_ecs: ecs, m_statemachine : statemachine}
    }

    fn new_2(name: &String, ecs: EntityComponenetSystem,statemachine: StateMachine) -> World
    {
        let listener = EventListener::new();

        World{m_name: name.clone(),m_ecs: ecs, m_statemachine : statemachine}
    }

    pub fn init(&mut self,core: &Option<Core>)
    {
        self.m_ecs.init(&core);
        self.m_statemachine.init(&mut self.m_ecs);
    }

    pub fn update(&mut self, timer: &mut Chrono)
    {
        self.m_ecs.update(timer);
        self.m_statemachine.update(&mut self.m_ecs,timer);
    }

    pub fn free(&mut self)
    {
        self.m_ecs.free();
        self.m_statemachine.free();
    }

    pub fn change_state(&mut self, state_id: &String)
    {
        self.m_statemachine.change_state(state_id);
    }
}

pub struct WorldManager 
{
    m_listener: EventListener,
    m_world_blueprint: Vec<String>,
    m_world: Rc<RefCell<World>>,
}

impl WorldManager 
{
    pub fn new() -> WorldManager  
    {
        let world = Rc::new(RefCell::new(World::new(&String::from("MAIN"))));

        let listener = EventListener::new();

        WorldManager{m_listener : listener, m_world_blueprint: Vec::new(), m_world: world}
    }

    pub fn init(&mut self,core: &Option<Core>)
    {
        self.m_listener.init(&core);

        //let sample_json = include_str!("./sample.json");
        //web_sys::console::log_2(&"sample_json: ".into(), &sample_json.into());

        //if let Ok(mut world) = serde_json::from_str::<World>(sample_json)
        {
            self.m_world.borrow_mut().init(&core);

            let json = serde_json::to_string_pretty(&*self.m_world.borrow_mut()).unwrap();

            web_sys::console::log_2(&"json: ".into(), &json.into());

            web_sys::console::log_1(&"WorldManager added successful!".into());
        }

        let world = self.m_world.clone();

        self.m_listener.register::<WorldEvent>
        (&String::from("World"),Box::new(move |handle:&mut EventHandler| -> Vec<EventHandler>
        {
            let state_event = handle.get_event::<WorldEvent>();

            match &*state_event
            {
                WorldEvent::ChangeState{m_state: state_id}=>
                {
                    world.borrow_mut().change_state(&state_id.clone());
                },
                WorldEvent::ChangeWorld{m_world: world}=>
                {

                },
                WorldEvent::ReloadWorld=>
                {

                },
                _ => 
                {
                    println!("Not supported World!");
                }
            }

            Vec::new()
        }));
    }

    pub fn update(&mut self, timer: &mut Chrono)
    {
        self.m_world.borrow_mut().update(timer);
    }

    pub fn free(&mut self)
    {
        self.m_listener.deregister::<WorldEvent>(&String::from("World"));

        self.m_world.borrow_mut().free();
    }
}