
use std::cell::{RefCell,RefMut};
use std::rc::Rc;
use uuid::Uuid;
use std::collections::HashMap;
use crate::engine::component::{Component,ComponentHandlerSeed,ComponentHandler,ComponentFactory};
use crate::engine::{UniqueIDRegistry};
use serde::{Serialize, Deserialize,Serializer,Deserializer};
use std::fmt::{self, Formatter};
use serde::de::{self, Visitor, MapAccess,SeqAccess};
use serde::ser::{SerializeMap,SerializeSeq,SerializeStruct};

#[derive(Clone,Serialize,Deserialize)]
pub struct Entity
{
    #[serde(skip)]
    m_component_factory: Rc<RefCell<ComponentFactory>>,
    #[serde(serialize_with = "serialize_hash_map_component")]
    #[serde(deserialize_with = "deserialize_hash_map_component")]
    m_component_list: HashMap<String,ComponentHandler>
}

/*impl Serialize for Entity
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut s = serializer.serialize_struct("Entity", 3)?;
        s.serialize_field("name",&self.m_name)?;
        s.serialize_field("uuid", &self.m_uuid)?;
        s.serialize_field("components", &self.m_component_list)?;
        s.end()
    }
}*/

fn serialize_hash_map_component<S>(comp_map:&HashMap<String,ComponentHandler>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let mut seq = serializer.serialize_seq(Some(comp_map.len()))?;
    for (_, v) in comp_map {
        seq.serialize_element(&v)?;
    }
    seq.end()
}

fn deserialize_hash_map_component<'de, D>(deserializer: D) -> Result<HashMap<String,ComponentHandler>, D::Error> 
where D: Deserializer<'de>
{
    struct HashMapComponentVisitor;

    impl<'de> Visitor<'de> for HashMapComponentVisitor {
        type Value = HashMap<String,ComponentHandler>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result 
        {
            formatter.write_str("struct HashMap<String,ComponentHandler>")
        }

        fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
        where
            A: SeqAccess<'de>, 
        {
            web_sys::console::log_1(&"visit_seq enter!".into());
            let mut map = HashMap::new();
            while let Some(handle) = seq.next_element_seed(ComponentHandlerSeed)? 
            {
                map.insert(handle.m_name.clone(),handle.clone());
            }
            Ok(map)
        }
    }

    deserializer.deserialize_seq(HashMapComponentVisitor)
}

impl Default for Entity
{
    fn default() -> Self
    {
        Entity::new(&Rc::new(RefCell::new(ComponentFactory::default())))
    }
}

impl Entity
{   
    pub fn new(component_factory: &Rc<RefCell<ComponentFactory>>) -> Entity
    {
        let component_list: HashMap<String,ComponentHandler> = HashMap::new();

        Entity{ m_component_factory: component_factory.clone(),m_component_list: component_list}
    }

    pub fn init(&mut self,component_factory: &Rc<RefCell<ComponentFactory>>)
    {
        self.m_component_factory = component_factory.clone();
    }

    pub fn add_component<T : Component + 'static>(&mut self, component: T) -> Option<RefMut<T>>
    {
        let name = std::any::type_name::<T>();

        web_sys::console::log_2(&"add_component: ".into(), &name.into());

        let result = self.m_component_factory.borrow_mut().generate(component);
    
        self.m_component_list.insert(result.m_name.clone(),result);

        //web_sys::console::log_2(&"self.m_component_list.len(): ".into(), &(self.m_component_list.len() as u32).into());

        self.get_component::<T>()
    }

    pub fn get_component<T : Component + 'static>(&mut self) -> Option<RefMut<T>>
    {
        let name = std::any::type_name::<T>();

        //web_sys::console::log_2(&"get_component: ".into(), &name.into());

        if let Some(result) = self.m_component_list.get_mut(&name.to_string())
        {
            return Some(result.get_component::<T>())
        }

        None
    }

    pub fn get_handle<T : Component + 'static>(&mut self) -> Option<ComponentHandler>
    {
        let name = std::any::type_name::<T>();

        //web_sys::console::log_2(&"get_handle: ".into(), &name.into());

        if let Some(result) = self.m_component_list.get(&name.to_string())
        {
            return Some(result.clone())
        }

        None
    }

    pub fn remove_component<T : Component>(&mut self)
    {
        let name = std::any::type_name::<T>();

        let uuid = self.m_component_list[&name.to_string()].m_uuid;

        self.m_component_factory.borrow_mut().delete(&name.to_string(),&uuid);
    }

    pub fn clear_all_components(&mut self)
    {
        for (key, value) in &mut self.m_component_list
        {
            value.get_base().borrow_mut().free();
            
            self.m_component_factory.borrow_mut().delete(&key.to_string(),&value.m_uuid);
        }

        self.m_component_list.clear();
    }
}

#[derive(Clone,Serialize,Deserialize)]
pub struct EntityHandler
{
    pub m_name: String,
    pub m_uuid : Uuid,
    #[serde(skip)]
    pub m_entity : Rc<RefCell<Entity>>,
}

impl EntityHandler
{
    pub fn new(name : &String,uuid: &Uuid,component_factory: &Rc<RefCell<ComponentFactory>>) -> EntityHandler
    {
        let entity_wrapper = Rc::new(RefCell::new(Entity::new(component_factory)));

        EntityHandler{ m_name: name.clone(), m_uuid : uuid.clone(),m_entity: entity_wrapper}
    }

    pub fn new_2(name : &String,uuid: &Uuid, component : Rc<RefCell<dyn Component>>) -> ComponentHandler
    {
        ComponentHandler{ m_name: name.clone(), m_uuid : uuid.clone(),m_component: component as Rc<RefCell<dyn Component>>}
    }

    pub fn get_name(&self) -> &String
    {
        &self.m_name
    }

    pub fn get_entity(&self) -> RefMut<Entity>
    {
        self.m_entity.borrow_mut()
    }

    pub fn get_base(&mut self) -> Rc<RefCell<Entity>>
    {
        self.m_entity.clone()
    }
}

pub struct EntityAllocator
{
    m_component_factory: Option<Rc<RefCell<ComponentFactory>>>,

    pub m_look_up: HashMap<String,Uuid>,
    pub m_entity_map: HashMap<Uuid,EntityHandler>,
}

impl Serialize for EntityAllocator
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(self.m_look_up.len()))?;
        for (_, v) in &self.m_look_up 
        {
            if let Some(entity) = self.m_entity_map.get(&v)
            {
                seq.serialize_element(&*entity.get_entity())?;
            }
        }
        seq.end()
    }
}

impl<'de> Deserialize<'de> for EntityAllocator {
    fn deserialize<D>(deserializer: D) -> Result<EntityAllocator, D::Error>
    where
        D: Deserializer<'de>,
    {
        web_sys::console::log_1(&"Deserialize EntityAllocator enter!".into());

        struct EntityAllocatorVisitor;

        impl<'de> Visitor<'de> for EntityAllocatorVisitor {
            type Value = EntityAllocator;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result 
            {
                formatter.write_str("struct EntityAllocator")
            }

            fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
            where
                A: SeqAccess<'de>, 
            {
                web_sys::console::log_1(&"visit_seq enter!".into());
                let mut alloc = EntityAllocator::new();
                alloc.init(&Rc::new(RefCell::new(ComponentFactory::default())));
                while let Some(elem) = seq.next_element::<EntityHandler>()? 
                {
                    alloc.insert(&elem.m_name,&elem.m_uuid,&elem);
                }
                Ok(alloc)
            }
        }
        deserializer.deserialize_seq(EntityAllocatorVisitor)
    }
}

impl Default for EntityAllocator
{
    fn default() -> Self
    {
        EntityAllocator::new()
    }
}

impl EntityAllocator
{
    pub fn new() -> EntityAllocator
    {
        EntityAllocator{ m_component_factory: None,m_look_up: HashMap::new(), m_entity_map: HashMap::new()}
    }

    pub fn generate(&mut self, name: &String, uuid: &Uuid) -> Option<EntityHandler>
    {
        if let Some(component_factory) = &self.m_component_factory
        {
            let entity_handler = EntityHandler::new(name, uuid, &component_factory);

            self.m_look_up.insert(name.clone(),uuid.clone());

            self.m_entity_map.insert(uuid.clone(),entity_handler);
        }

        self.find(&name.clone())
    }

    pub fn insert(&mut self, name: &String, uuid: &Uuid, entity_handler: &EntityHandler) -> Option<EntityHandler>
    {
        self.m_look_up.insert(name.clone(),uuid.clone());

        self.m_entity_map.insert(uuid.clone(),entity_handler.clone());

        self.find(name)
    }

    pub fn find(&mut self, name: &String) -> Option<EntityHandler>
    {
        let uuid = self.m_look_up.get(&name.clone()).expect("No uuid found!");

        if let Some(entity) = self.m_entity_map.get_mut(uuid)
        {
            return Some(entity.clone());
        }

        None
    }

    pub fn get_all_entities(&mut self) -> Vec<EntityHandler>
    {
        let mut result = Vec::new();

        for (_,entity_handler) in &self.m_entity_map
        {
            result.push(entity_handler.clone());
        }
        
        result
    }

    pub fn delete_by_name(&mut self, name: &String) -> Uuid
    {
        let uuid = self.m_look_up.get(&name.clone()).expect("No uuid found!");

        let entity_handler = self.m_entity_map.get_mut(uuid).expect("No entity found!");

        entity_handler.get_entity().clear_all_components();

        let result = self.m_look_up.remove(&name.clone()).expect("No name found to be removed!");

        self.m_entity_map.remove(&result).expect("No uuid found to be removed!");

        result
    }

    pub fn delete_all_entities(&mut self) -> Vec<Uuid>
    {
        let mut result : Vec<Uuid> = Vec::new();

        for (key,value) in &mut self.m_entity_map
        {
            result.push(key.clone());

            value.get_entity().clear_all_components();
        }

        self.m_look_up.clear();
        
        self.m_entity_map.clear();

        result
    }

    pub fn init(&mut self,component_factory: &Rc<RefCell<ComponentFactory>>)
    {
        web_sys::console::log_1(&"EntityAllocator init".into());

        self.m_component_factory = Some(component_factory.clone());
    
        for (_,value) in &mut self.m_entity_map
        {
            if let Some(component_factory) = &self.m_component_factory
            {
                value.get_entity().init(&component_factory);
            }
        }
    }
}